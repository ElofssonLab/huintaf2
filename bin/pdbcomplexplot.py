#!/usr/bin/env python3
import pandas as pd
import sys
import numpy as np
import argparse
import re
import matplotlib.pyplot as plt
import seaborn as sns

from pathlib import Path
home = str(Path.home())


df_contacts=pd.read_csv(home+"/git/huintaf2/data/pdbcomplex-contacts.csv")
df_pdockq=pd.read_csv(home+"/git/huintaf2/data/pdbcomplex-pdockq.csv")

df_contacts["Name"]=df_contacts["Chain1"]+"-"+df_contacts["Chain2"]+"_1"
df_merge=pd.merge(df_contacts,df_pdockq,on="Name")
df_merge["Interact"]=np.where(df_merge["Contacts"]>20,"Direct","Indirect")
f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.kdeplot(data=df_merge, x="pDockQ",hue="Interact",common_norm=False)
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/pdbcomplex.png",dpi=300)
plt.savefig(home+"/git/huintaf2/plots/pdbcomplex.svg",dpi=300)


f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.violinplot(data=df_merge,x="Interact",y="pDockQ")
plt.savefig(home+"/git/huintaf2/plots/pdbcomplex-violin.png",dpi=300)
plt.savefig(home+"/git/huintaf2/plots/pdbcomplex-violin.svg",dpi=300)
