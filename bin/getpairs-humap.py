#!/usr/bin/env python3

import pandas as pd

df=pd.read_csv("data/humap2_complexes_20200809.txt",header=0,sep=",")
k=0
#print (df["Uniprot_ACCs"])
for c in df.HuMAP2_ID:
    #print ("Complex: ",c)
    id=df[df.HuMAP2_ID==c]["Uniprot_ACCs"].to_list()[0]
    foo=id.split(' ')
    #print (c,id,foo)
    for j in foo:
        if j=="":continue
        for i in foo:
            if i=="":continue
            if (i>j):
                print (str(c)+","+str(i)+","+str(j))
    k+=1
