#!/bin/bash -x
#SBATCH -A SNIC2021-5-297
#SBATCH --output=/proj/snic2019-35-62/users/x_gabpo/job_out/AF%j.out
#SBATCH --error=/proj/snic2019-35-62/users/x_gabpo/job_err/AF%j.err
#SBATCH --array=1-500
#SBATCH --gpus-per-task=1
#SBATCH -N 1
#SBATCH -t 06:00:00

##### AF2 CONFIGURATION #####
COMMON='/proj/snic2019-35-62/' # Path prefix shared with all adopted paths
AFHOME=$COMMON'/AF2-multimer-mod/' # Path of AF2-multimer-mod directory
SINGULARITY=$AFHOME'/AF_data/alphafold-multimer.sif' # Path of singularity image
PARAM=$AFHOME'/AF_data/' # path of param folder containing AF2 Neural Net parameters.

###### DATABASES ######
UNICL30=$COMMON'databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08'
UNIREF=$COMMON'databases/uniref90/uniref90.fasta'
UNIPROT=$COMMON'databases/uniprot/uniprot.fasta'
MGNIFY=$COMMON'databases/mgnify/mgy_clusters.fa'
SEQRES=$COMMON'databases/pdb_seqres/pdb_seqres.txt'
PDB70=$COMMON'databases/pdb70/pdb70'
MMCIF=$COMMON'databases/pdb_mmcif/mmcif_files/'
OBS=$COMMON'databases/pdb_mmcif/obsolete.dat'
BFD=$COMMON'databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt'
SMALLBFD=$COMMON'databases/small_bfd/bfd-first_non_consensus_sequences.fasta'

### ADVANCED CONFIGURATION ###
PRESET='model_1_multimer' #Choose preset model configuration - no ensembling (monomer), casp14 setup (monomer_casp14), pTM head (monomer_ptm) or fold and dock (multimer).
MAX_RECYCLES= #max_recycles

### RUNTIME SPECIFICATIONS ###
LIST=$1
OFFSET=$2
POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
LINE=`tail -n+$POS $LIST | head -n 1` 
FASTA=`echo $LINE | awk '{print $1}'`  # Path of input fasta files
FOLDER=./ # Path where AF2 picks fasta files and generates its output folder structure
PRO=`echo $LINE | awk '{print $2}'` # true if folding a prokariotic multimer, false otherwise
echo $FASTA $PRO
singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
	python3 $AFHOME/alphafold/run_alphafold.py \
		--fasta_paths=${FOLDER}/${FASTA}.fasta \
                --is_prokaryote_list=$PRO \
                --data_dir=$PARAM \
		--output_dir=$FOLDER \
                --uniref90_database_path=$UNIREF \
                --mgnify_database_path=$MGNIFY \
		--bfd_database_path=$BFD \
		--uniclust30_database_path=$UNICL30 \
		--uniprot_database_path=$UNIPROT \
                --pdb_seqres_database_path=$SEQRES \
		--model_preset=$PRESET \
                --use_precomputed_msas=False \
                --db_preset=full_dbs

