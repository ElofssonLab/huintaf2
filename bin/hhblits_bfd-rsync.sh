#!/bin/bash -x
#SBATCH -A SNIC2021-5-297
#SBATCH --output=/proj/nobackup/snic2019-35-62/arnee/hhblits-runs/out/%j.out
#SBATCH --error=/proj/nobackup/snic2019-35-62/arnee/hhblits-runs/err/%j.err
#SBATCH --array=1-1
#SBATCH -c 8
#SBATCH -t 12:00:00


list=$1
#offset=$2

#pos=$(($SLURM_ARRAY_TASK_ID + $offset))

#id=`tail -n+$pos $list | head -n 1`

for id in `cat $list`
do
    if [ ! -f a3m-bfd//${id}.a3m ]
    then
	echo 'Processing ' $id '...'
	touch a3m-bfd//${id}.a3m
	mkdir -p /scratch/arnee/
	rsync -arv /proj/nobackup/snic2019-35-62/Database/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt_* /scratch/arnee/
	/proj/nobackup/snic2019-35-62/arnee/bin/hhblits  -i seq/${id}.fasta -d /scratch/arnee/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt -E 0.001 -all -oa3m a3m-bfd//${id}.a3m > out/${id}.log
    fi
done
rm -rf /scratch/arnee/


