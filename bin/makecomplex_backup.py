#!/usr/bin/env python3
import argparse
#from Bio.PDB.vectors import rotaxis, calc_angle, calc_dihedral
#from Bio.PDB.Polypeptide import is_aa
#from math import pi
import re
import math
import numpy  as np
from Bio.PDB import PDBIO
from Bio import SeqIO
from Bio.PDB import PDBParser
from Bio.PDB import MMCIFParser
from Bio.PDB import Selection
from Bio.PDB import MMCIFIO
from Bio.PDB import PDBIO
from Bio.PDB import Select
from Bio.PDB.Chain import Chain
#from Bio.PDB.Polypeptide import PPBuilder
import subprocess
import sys
import pandas as pd
from Bio.PDB import Entity



def center_of_mass(entity, geometric=False):
    """
    Returns gravitic [default] or geometric center of mass of an Entity.
    Geometric assumes all masses are equal (geometric=True)
    """
    
    # Structure, Model, Chain, Residue
    if isinstance(entity, Entity.Entity):
        atom_list = entity.get_atoms()
    # List of Atoms
    elif hasattr(entity, '__iter__') and [x for x in entity if x.level == 'A']:
        atom_list = entity
    else: # Some other weirdo object
        raise ValueError("Center of Mass can only be calculated from the following objects:\n"
                            "Structure, Model, Chain, Residue, list of Atoms.")
    
    masses = []
    positions = [ [], [], [] ] # [ [X1, X2, ..] , [Y1, Y2, ...] , [Z1, Z2, ...] ]
    
    for atom in atom_list:
        
        masses.append(atom.mass)
        
        for i, coord in enumerate(atom.coord.tolist()):
            positions[i].append(coord)

    # If there is a single atom with undefined mass complain loudly.
    if 'ukn' in set(masses) and not geometric:
        raise ValueError("Some Atoms don't have an element assigned.\n"
                         "Try adding them manually or calculate the geometrical center of mass instead.")
    
    if geometric:
        return [sum(coord_list)/len(masses) for coord_list in positions]
    else:       
        w_pos = [ [], [], [] ]
        for atom_index, atom_mass in enumerate(masses):
            w_pos[0].append(positions[0][atom_index]*atom_mass)
            w_pos[1].append(positions[1][atom_index]*atom_mass)
            w_pos[2].append(positions[2][atom_index]*atom_mass)

        return [sum(coord_list)/sum(masses) for coord_list in w_pos]

class ChainSelect(Select):
    def __init__(self,chain):
        self.chain=chain
    def accept_chain(self,chain):
        if (re.search(chain.get_id(),self.chain)):
            return 1
        else:
            return 0

if __name__ == "__main__":
    arg_parser = argparse.\
        ArgumentParser(                description="Translate and rotate a pdb file")
    #in_group = arg_parser.add_mutually_exclusive_group(required=True)
    #in_group.add_argument("-p", "--pdb_file", type=argparse.FileType('r'))
    #in_group.add_argument("-m", "--mmCIF_file", type=argparse.FileType('r'))
    arg_parser.add_argument('-i','--input', nargs='+', help='List of input sequences', required=True)
    #rot_group = arg_parser.add_mutually_exclusive_group(required=True)
    #rot_group.add_argument("-i", "--input", "--rot_file") #, type=argparse.FileType('r'))
    #rot_group.add_argument("-r", "--rotation", required=False, help="12 parameters for translation and rotation (Note you need a space before a - sign)",nargs=12,type=float)
    #rot_group.add_argument("-a", "--angles", required=False, help="6 parameters for translation and rotation (Note you need a space before a - sign)",nargs=6,type=float)

    arg_parser.add_argument("-o","--outfile", type=str,required=False)
    #arg_parser.add_argument("-c", "--chain", type=str, default='A')
    #arg_parser.add_argument("-s", "--std", default=1, type=float,
    #                         help="Standard deviation in Ångström")
    args = arg_parser.parse_args()
    std = 1
    #print  (args)



    MM="/scratch3/arnee/HuMap2/bin/MMalign"
    
    #name1=sys.argv[1]
    #name2=sys.argv[2]
    name1=args.input[0]
    name2=args.input[1]
    print (name1,name2)
    try:
        parser = MMCIFParser()
        tempname1=re.sub(".cif$","",name1)
        structure1 = parser.get_structure("protein1", name1)
        io=PDBIO()
        io.set_structure(structure1)
        io.save(tempname1+".pdb")
    except:
        parser = PDBParser()
        tempname1=re.sub(".pdb$","",name1)
        structure1 = parser.get_structure("protein1", name1)
    
    try:
        parser = MMCIFParser()
        tempname2=re.sub(".cif$","",name2)
        structure2 = parser.get_structure("protein2", name2)
        io=PDBIO()
        io.set_structure(structure2)
        io.save(tempname2+".pdb")
    except:
        parser = PDBParser()
        tempname2=re.sub(".pdb$","",name2)
        structure2 = parser.get_structure("protein2", name2)
        
    
    
    outnames1=[]
    outnames2=[]
    io=PDBIO()
    io.set_structure(structure1)
    #print ("test",structure1)        
    chains1={}
    Chains1=[]
    Chains2=[]
    for chain1 in structure1.get_chains():
        print (chain1)
        chain=str(chain1.id)
        out=tempname1+"_"+chain+".pdb"
        print ("TEST",out,chain)
        io.save(out,ChainSelect(chain))
        outnames1+=[out]
        chains1[out]=chain1.id
        Chains1+=[chain1.id]
    io.set_structure(structure2)
    chains2={}
    for chain2 in structure2.get_chains():
        chain=str(chain2.id)
        out=tempname2+"_"+chain+".pdb"
        io.save(out,ChainSelect(chain))
        outnames2+=[out]
        chains2[out]=chain2.id
        Chains2+=[chain2.id]
    
    df_MM=pd.DataFrame(columns=['Name1', 'Name2',"File1","File2","OrgFile1","OrgFile2","Chain1","Chain2",'TM1',"TM2","RMSD","SeqID","MMfile","rotfile"])
    
    for i in outnames1:
        for j in outnames2:
            
            tempdata={"File1":i,"File2":j,"OrgFile1":tempname1,"OrgFile2":tempname2}
            tempdata["Chain1"]=chains1[i]
            tempdata["Chain2"]=chains2[j]
            tempdata["Outdir"]=re.sub(r'\/.*','',i)
            name=re.sub(r'.*/','',i)
            name=re.sub(r'.pdb$','',name)
            name=re.sub(r'.cif$','',name)
            tempdata["Name1"]=name
            name=re.sub(r'.*/','',j)
            name=re.sub(r'.pdb$','',name)
            name=re.sub(r'.cif$','',name)
            tempdata["Name2"]=name
            tempdata["MMfile"]=tempdata["Outdir"]+"/"+tempdata["Name1"]+":"+tempdata["Name2"]+".MM"
            tempdata["rotfile"]=tempdata["Outdir"]+"/"+tempdata["Name1"]+":"+tempdata["Name2"]+".rot"
            with open(tempdata["MMfile"], "w+") as file:
                mm=subprocess.run([MM,"-m",tempdata["rotfile"],tempdata["File1"],tempdata["File2"]],stdout=file)
            file.close()
            file=open(tempdata["MMfile"], "r")
            for line in file.readlines():
                if re.match(r'TM-score=.*Chain_1',line):
                    tempdata["TM1"]=float(line.split()[1])
                elif re.match(r'TM-score=.*Chain_2',line):
                    tempdata["TM2"]=float(line.split()[1])
                elif re.match(r'Aligned.*RMSD',line):
                    tempdata["RMSD"]=float(re.sub(r'\,','',line.split()[4]))
                    tempdata["SeqID"]=float(line.split()[6])
                #print (tempdata)
            df_MM = df_MM.append(tempdata, ignore_index=True)
    df_MM["TM"]=df_MM[["TM1","TM2"]].max(axis=1)
    print (df_MM)
    best=df_MM.sort_values("TM",ascending=False).iloc[0]
    print (best)
    #mm=subprocess.run([MM,"-m",tempdata["rotfile"],tempdata["File1"],tempdata["File2"]],stdout=file)
    #print(["/scratch3/arnee/HuMap2/bin/translatepdb.py",
    #                     "-i",best["rotfile"],"-p",best["OrgFile1"]+".pdb","-o",best["OrgFile1"]+"_rot.pdb"])
    
    #rotation=subprocess(["/scratch3/arnee/HuMap2/bin/translatepdb.py",
    #                     "-i",best["rotfile"],"-p",best["OrgFile1"]+".pdb","-o",best["OrgFile1"]+"_rot.pdb"])
    

    # Using readlines()
    rotfile = open(best["rotfile"], 'r')
    Lines = rotfile.readlines()
    translation_matrix=np.array((0,0,0),"f")
    rotation_matrix=np.array(((0,0,0),(0,0,0),(0,0,0)),"f")
    translation=[0,0,0]
    for line in Lines:
        if (line[0]=="0"):
            temp=line.split()
            #print (temp)
            translation_matrix[0]=1*float(temp[1])
            rotation_matrix[0,0]=1*float(temp[2])
            rotation_matrix[0,1]=1*float(temp[3])
            rotation_matrix[0,2]=1*float(temp[4])
        elif (line[0]=="1"):
            temp=line.split()
            translation_matrix[1]=1*float(temp[1])
            rotation_matrix[1,0]=1*float(temp[2])
            rotation_matrix[1,1]=1*float(temp[3])
            rotation_matrix[1,2]=1*float(temp[4])
        elif (line[0]=="2"):
            temp=line.split()
            translation_matrix[2]=1*float(temp[1])
            rotation_matrix[2,0]=1*float(temp[2])
            rotation_matrix[2,1]=1*float(temp[3])
            rotation_matrix[2,2]=1*float(temp[4])
            #rotation_matrix=rotation_matrix.T
    print (rotation_matrix)
    #print (rotation_matrix[0,0])
    print (translation_matrix)

    structure1.transform(rotation_matrix.T, translation_matrix)
        
    structure=structure1
    
    for chain in structure2.get_chains():
        #print ("Test",chain.id)
        if chain.id == best["Chain2"]:
            print ("Detaching Structure2 chain:",chain.id)
            #structure2.detach_child[chain.id]
            break
        while chain.id in Chains1:
            #print ("test",chain.id,Chains1)
            i=1
            while chr(ord(chain.id)+i) in Chains2:
                #print ("I",i)
                i+=1
            chain.id=chr(ord(chain.id)+i)
            #chain.serial_number=len(Chains1)+1
        Chains1+=[chain.id]
        for c in structure.get_chains():
            print ("Test1",c,c.id)
        print ("Adding", chain.id)
        structure[0].add(chain)
        for c in structure.get_chains():
            print ("Test2",c,c.id)

    io = PDBIO()
    io.set_structure(structure)
    io.save(args.outfile)
