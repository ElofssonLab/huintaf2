#!/bin/bash -l
#SBATCH -A NAISS2023-5-249
#SBATCH -J MSA
#SBATCH -p shared
#SBATCH -t 4:00:00
#SBATCH -c 16
##SBATCH --mem=[XXXX]MB
#SBATCH --output=log/msa_hhblits_%A_%a.out
#SBATCH --error=log/msa_hhblits_%A_%a.error
#SBATCH --array=1-500

#ml PDC/21.11
#ml parallel/20210922

export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

if [ -z $2 ]
then
        offset=0
else
        offset=$2
fi

abspath=`pwd`


ID_FILE=$1
POS=$(($SLURM_ARRAY_TASK_ID + $offset))
ID=$(sed -n ${POS}p $ID_FILE)
#echo $ID
#protID=${ID::-2}  # everything but last two letters, example: 7bdc
#chainID=${ID:0-1} # select last letter, example: A 


protID=`basename ${ID} .fasta`
chainID="A"



## run hhblits
SECONDS=0
ncores=24
echo "number of cores: ${ncores}"
bash ${abspath}/bin/dardel_hhblits_small.bash ${protID} ${chainID} ${ncores}
#bash ${abspath}/bin/dardel_hhblits.sh ${protID} ${chainID} ${ncores}
duration=$SECONDS
echo "Elapsed Time: $SECONDS seconds"
echo "$(($SECONDS/3600))h $((($SECONDS/60)%60))m $(($SECONDS%60))s"


