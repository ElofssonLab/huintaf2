#!/bin/bash -x
#SBATCH -A Berzelius-2022-106
#SBATCH --output=out/FFsingle-%j.out
#SBATCH --error=err/FFsingle-%j.err
#SBATCH --array=1
#SBATCH -N 1
#SBATCH --export=ALL,CUDA_VISIBLE_DEVICES
#SBATCH --gpus=1
#SBATCH -t 4:00:00


# #SBATCH --exclusive

#export CUDA_VISIBLE_DEVICES='all'  # this makes cuda not availabke
export NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'
export TF_CPP_MIN_LOG_LEVEL=0
##


# 


##### AF2 CONFIGURATION #####
COMMON="/proj/berzelius-2021-29/"
FFHOME="/proj/berzelius-2021-29/users/x_arnel/git/fastfold-mod/"



AFHOME=${fddir}/src/alphafold/ # Path of alphafold directory in FoldDock 
#SINGULARITY=/proj/berzelius-2021-29/singularity_images/alphafold.sif
PARAM=/proj/berzelius-2021-29/Database/af_params/params/params_  #Path to AF2 params \
OUTFOLDER=fastfold-single/ # Path where AF2 generates its output folder structure
MAX_RECYCLES=10
BIN="/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/"



#### ONLY ONE OF THE FOLLOWING SHOULD BE UNCOMMENTED ####################
#MODEL_SET="model_1"							# Uncomment to run single standard model.
MODEL_SET="model_1_ptm"						# Uncomment to run single ptm model.
#MODEL_SET="multimer" 						# Uncomment to run single multimer model.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}"; done		# Uncomment to run all 5 standard models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_ptm"; done	# Uncomment to run all 5 ptm models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_multimer"; done	# Uncomment to run all 5 multimers models.
#########################################################################

### RUNTIME SPECIFICATIONS ###
LIST=$1 	# List with each line containing: fastaID(no format) [true or false] set true if protein is prokariote, false otherwise.
OFFSET=$2 	# ignore first $OFFSET lines of list; to set according to SLURM array size, in order to run all the list.
#FOLDER=$3 	# Path where AF2 picks fasta files and generates its output folder structure.
FOLDER="fastfold-single/"
#PRO=false
POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
LINE=`tail -n+$POS $LIST | head -n 1`
id=`echo $LINE | sed "s/\ /-/g" `	# path of input fasta file (without .fasta suffix)
#id2=`echo $LINE | awk '{print $2}'`	# path of input fasta file (without .fasta suffix)
FASTA="fastfold-single/${id}.fasta"
#FASTA="complexes-v2/${id}/msas/A/"${id}".fasta"
#PRO=`echo $LINE | awk '{print $2}'`	# true if folding a prokariotic multimer, false otherwise
PRO=false,false

MSAS="MSAs/"${id}"/A/" # runs all a3m and sto files in this directory
MMCIF="./fastfold-single/${id}/"


KALIGN=/proj/berzelius-2021-29/users/x_arnel/.conda/envs/af_server/bin/kalign
HHBLITS=/proj/berzelius-2021-29/users/x_arnel/.conda/envs/af_server/bin/hhblits
HHSEARCH=/proj/berzelius-2021-29/users/x_arnel/.conda/envs/af_server/bin/hhsearch
JACKHMMER=/proj/berzelius-2021-29/users/x_arnel/.conda/envs/af_server/bin/jackhmmer



#module load buildenv-gcccuda/11.4-system-nsc1
module load buildenv-gcccuda/11.3-8.3.1-bare
module load Anaconda/2021.05-nsc1
# conda init bash
conda activate /proj/berzelius-2021-29/users/x_arnel/.conda/envs/FastFold/

#conda info

#nvcc -V
#python -c "import torch;print(torch.version.cuda)"
#nvidia-smi
#lspci
#echo $LD_LIBRARY_PATH
#python3 /proj/berzelius-2021-29/users/x_arnel/.conda/envs/FastFold/lib/python3.8/site-packages/torch/utils/collect_env.py

for PRESET in $MODEL_SET
do
    if [[ ! -f  ${FOLDER}/${id}/ranked_5.pdb ]]
    then
        ##### TO JUST FOLD, GIVEN AN AF DEFAULT FOLDER STRUCTURE WITH MSAS #####
        ##### ALREADY EXIST AT $FOLDER/$FASTA                              ##### 
	#singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
	    #singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \

	
        #python3 $FFHOME/alphafold/run_alphafold.py \
	python3 $FFHOME/inference.py \
		--jackhmmer_binary_path=$JACKHMMER \
		--hhblits_binary_path=$HHBLITS \
		--hhsearch_binary_path=$HHSEARCH \
		--kalign_binary_path=$KALIGN \
		--use_precomputed_alignments=${MSAS} \
		--output_dir=$FOLDER/${id}/ \
		--model_name=$PRESET \
		--param_path=${PARAM}${PRESET}.npz \
		${FASTA} ${MMCIF}
                
	# srun berzelius-2022-106 --gpus=1  python3   ../git/fastfold-mod/inference.py  --use_precomputed_alignments=MSAs/T1123/A/ --output_dir=fastfold-single/ --model_name=model_1 --param_path=/proj/berzelius-2021-29/Database/af_params/params/params_model_1.npz MSAs/T1123/A/T1123.fasta ./

#--run_relax=True \
	        #--param_path=$PARAM/params/params_${PRESET}.npz 
		# --preset
		#--is_prokaryote_list=$PRO \
		#--uniref90_database_path=  \
		#--mgnify_database_path= \
		#--pdb70_database_path== \
		#--uniclust30_database_path= \
		#--bfd_database_path= \


    fi

done


#pip install --upgrade "jax[cuda]" -f https://storage.googleapis.com/jax-releases/jax_releases.html
#export TF_FORCE_UNIFIED_MEMORY=1
#export XLA_PYTHON_CLIENT_MEM_FRACTION=10.0


# Command that seems to work
# python3 /proj/berzelius-2021-29/users/x_arnel/git/fastfold-mod//inference.py --jackhmmer_binary_path=/proj/berzelius-2021-29/users/x_arnel/.conda/envs/af_server/bin/jackhmmer --hhblits_binary_path=/proj/berzelius-2021-29/users/x_arnel/.conda/envs/af_server/bin/hhblits --hhsearch_binary_path=/proj/berzelius-2021-29/users/x_arnel/.conda/envs/af_server/bin/hhsearch --kalign_binary_path=/proj/berzelius-2021-29/users/x_arnel/.conda/envs/af_server/bin/kalign --output_dir=fastfold-single//T1123/ --model_name=model_1_ptm --param_path=/proj/berzelius-2021-29/Database/af_params/params/params_model_1_ptm.npz --bfd_database_path=/proj/beyondfold/apps/alphafold_data/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt --uniclust30_database_path=/proj/beyondfold/apps/alphafold_data/uniclust30/uniclust30_2018_08/uniclust30_2018_08  fastfold-single/T1123.fasta fastfold-single/T1123/
