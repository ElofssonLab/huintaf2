#!/bin/bash
#SBATCH -A SNIC2019-3-319
#SBATCH --job-name=HHblits
#SBATCH --output=hhblits_logs/hhblits%a.slurm.out
#SBATCH --error=hhblits_logs/hhblits%a.slurm.error
#SBATCH --array=1-10
#SBATCH -c 14
#SBATCH -t 20:00:00
# echo "Before load"
ml GCC/5.4.0-2.26 OpenMPI/1.10.3
ml HH-suite/2.0.16
# Support for given an argument as offset in the array
if [ -z $1 ]
then
	offset=0
else
	offset=$1
fi
# echo "Before array"
# ID=`tail -n+$SLURM_ARRAY_TASK_ID+$1 /pfs/nobackup/home/j/jlamb/pfam_fasta.txt | head -n1`
ID=`tail -n+$(($SLURM_ARRAY_TASK_ID+$offset)) /pfs/nobackup/home/j/jlamb/pfam_all_list.txt | head -n1`  # Handle loops
# ID=`tail -n+$(($SLURM_ARRAY_TASK_ID+$offset)) /pfs/nobackup/home/j/jlamb/pfam_pdb.txt | head -n1`  # Handle loops
# ID=$1
ulimit -s unlimited
# echo $ID
# ID=`head -n1 /pfs/nobackup/home/j/jlamb/pconsfold2test.txt`
# #IN=$1
# #ID=$1
# offset=$1
# pos=$(($SLURM_ARRAY_TASK_ID + $offset))
# #pos=$((1 + $offset))
# #ID=`tail -n+$pos IDs_29.0_nopdb_0.75_E3.txt | head -n1`
# #ID=`tail -n+$pos pdb_diff.txt | head -n1`
# ID=`tail -n+$pos tmp.hh | head -n1`
# #ID=`tail -n+$SLURM_ARRAY_TASK_ID IDs_29.0_nopdb_0.75_E3.txt | head -n1`
# #IN=29.0/${ID}/${ID}.fa
# echo "Before IN"
IN=`ls /pfs/nobackup/home/j/jlamb/pfam_samples/${ID}/all/*.fa`
echo $IN
# #module load python/2.7.6

# echo "Before if"
if [ ! -f ${IN}.hhE0.a3m ]; then
	echo "Creating E0 file"
	# if [ ! -s $SNIC_TMP/uniprot20_2016_02/uniprot20_2016_02_a3m_db ]; then
	#     cp -r /pfs/nobackup/home/m/mircomic/databases/hhsuite_db/uniprot20/uniprot20_2016_02 $SNIC_TMP/
	# fi
	hhblits -cpu 14 -all -oa3m ${IN}.hhE0.a3m -maxfilt 999999 -realign_max 999999 -e 1 -i ${IN} -d /pfs/nobackup/home/j/jlamb/uniprot20_2016_02/uniprot20_2016_02
	rm -rf ${IN}*.hhr
	# python /pfs/nobackup/home/m/mircomic/pcons-fold/pconsc/scripts/a3mToTrimmed.py ${IN}.hhE0.a3m > ${IN}.hhE0.trimmed
fi

if [ ! -f ${IN}.hhE4.a3m ]; then

	echo "Creating E4 file"
	# if [ ! -s $SNIC_TMP/uniprot20_2016_02/uniprot20_2016_02_a3m_db ]; then
	#     cp -r /pfs/nobackup/home/m/mircomic/databases/hhsuite_db/uniprot20/uniprot20_2016_02 $SNIC_TMP/
	# fi
	hhblits -cpu 14 -all -oa3m ${IN}.hhE4.a3m -maxfilt 999999 -realign_max 999999 -e 1e-4 -i ${IN} -d /pfs/nobackup/home/j/jlamb/uniprot20_2016_02/uniprot20_2016_02
	rm -rf ${IN}*.hhr
	# python /pfs/nobackup/home/m/mircomic/pcons-fold/pconsc/scripts/a3mToTrimmed.py ${IN}.hhE4.a3m > ${IN}.hhE4.trimmed
fi
wait
