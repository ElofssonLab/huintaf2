#!/bin/bash -x

#A=$1
#B=$2
while I= read -r A B k ; do
    if [ -d  pdb/$A-$B ]
    then
	for dir in EXPERIMENTAL MODEL_MODEL PDB_PDB
	do
	    if [ -f $dir/*$A*$B*pdb ]
	    then
		PDB=`ls $dir/*$A*$B*pdb`
		echo $A-$B $PDB
	    fi
	    if [ -f $dir/*$B*$A*pdb ]
	    then
		PDB=`ls $dir/*$B*$A*pdb`
		echo $A-$B $PDB
	    fi
	done
    fi
done < $1
