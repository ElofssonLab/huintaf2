#!/bin/bash 



i=`basename $1 .pdb`
d=`dirname $1 `
#j=`basename $2 .pdb`
j=`echo $i | sed "s/_.*//g"`
k=pdb/$j.pdb

A=`echo $i | sed "s/-.*//g" | sed "s/.*_//g" `
B=`echo $i | sed "s/.*-//g" | sed "s/_1$//g" | sed "s/.*_//g" `

echo "Name,DockQ" > $d/$i.DockQ
echo -n	$i"," >> $d/$i.DockQ

python3 ~/git/DockQ/DockQ.py -short $1 $k  -model_chain1 A -model_chain2 B -native_chain1 $A -native_chain2 $B | gawk '{print $2}' >> $d/$i.DockQ
