#!/bin/bash -x

for i in pdb/[a-z0-9][a-z0-9][a-z0-9][a-z0-9].pdb ; do j=`basename $i .pdb` ; bin/chaincontacts.py -p $i > pdb/$j.contacts.csv ; done
for i in pdb/[a-z0-9][a-z0-9][a-z0-9][a-z0-9].pdb ; do j=`basename $i .pdb` ; bin/chaincontacts.py -p $i > pdb/$j.contacts.csv ; done
for i in seq/*.fasta_cluster.tsv ; do j=`basename $i .fasta_cluster.tsv` ; bin/findcontacts.py -p pdb/$j.contacts.csv -c $i > pdb/$j.contacts; done
find pdb-multi/ -name "unrelaxed_model_1.pdb" -exec  bin/split_unrelaxed.bash {} \;
find pdb-multi/ -name "*-*.pdb" -exec  bin/pDockQ.bash {} \;


grep -h Name pdb-multi/*/*csv  | head -1 > data/pdbcomplex-pdockq.csv
grep -vh Name pdb-multi/*/*csv >> data/pdbcomplex-pdockq.csv


grep -h Contacts pdb/*contacts  | head -1 > data/pdbcomplex-contacts.csv
grep -vh Contacts pdb/*contacts >> data/pdbcomplex-contacts.csv

