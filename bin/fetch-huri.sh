#!/bin/bash -x
#Fetch plDDT from predicted structures and analyze results

dir=/proj/berzelius-2021-29/users/x_arnel/  # Hu.Map-2.0/
#########New dimers###########
DIR=$dir/HuRI/$1-$2/
MODEL=$DIR/unrelaxed_model_1.pdb
METRIC=$DIR/result_model_1.pkl
IT=10 #Interface threshold Å
FETCH_ATOMS='ALL'
CBR=200 #How many residues that were used as a chain break insertion

L1=`$dir/bin/seqlen.bash $dir/HuRI-fasta//$1.fasta`
L2=`$dir/bin/seqlen.bash $dir/HuRI-fasta//$2.fasta`
#for i in {1..5}
#do
python3 $dir/bin/fetch_plDDT.py --model $MODEL --fetch_atoms $FETCH_ATOMS --metric $METRIC --it $IT --cbr $CBR --l1 $L1 --l2 $L2
#done

