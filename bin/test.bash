#!/bin/bash -x
#SBATCH -A berzelius-2021-64
#SBATCH --output=out/AFmulti%j.out
#SBATCH --error=err/AFmulti%j.err
#SBATCH --array=1-1
#SBATCH -N 1
#SBATCH --export=ALL,CUDA_VISIBLE_DEVICES
#SBATCH --gpus=1
#SBATCH -t 8:00:00
export CUDA_VISIBLE_DEVICES='all'
export NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'
export TF_CPP_MIN_LOG_LEVEL=0
##
#nvidia-smi

singularity exec --nv --bind /proj/berzelius-2021-29/:/proj/berzelius-2021-29/ /proj/berzelius-2021-29/users/x_gabpo/af2-v2.2.0/data/af2.sif nvidia-smi



