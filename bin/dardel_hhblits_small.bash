#!/bin/bash -lx

#ml PDC/21.11
#ml parallel/20210922

ID=$1
ncpus=$3
CHAIN=$2 # last letter in ID

abspath=`pwd`
MSAS=$abspath"/multimer/"
FASTAPATH=$abspath"/seq/"

echo "Processing ${ID}..."

#FASTA=$FASTAPATH/${ID}_${CHAIN}.fasta
FASTA=$FASTAPATH/${ID}.fasta
echo "FASTA: ${FASTA} CHAIN: ${CHAIN}"

## create individual folder
mkdir -p $MSAS/$ID
mkdir -p $MSAS/$ID"/msas"
mkdir -p $MSAS/$ID"/msas/${CHAIN}"
OUTPUT=$MSAS/$ID"/msas/${CHAIN}/bfd_uniref_hits.a3m"

echo "OUTPUT: ${OUTPUT}"

#DATABASES
DATABASE_DIR=/cfs/klemming/projects/snic/snic2019-35-62/databases/
bfd_database_path=$DATABASE_DIR/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt
#bfd_database_path=$DATABASE_DIR/small_bfd/bfd-first_non_consensus_sequences.fasta
#uniclust30_database_path=$DATABASE_DIR/uniclust30/uniclust30_2018_08/uniclust30_2018_08
uniclust30_database_path=$DATABASE_DIR/uniclust30/UniRef30_2021_03/UniRef30_2021_03

#hhblits
hhblits_path=/cfs/klemming/projects/snic/snic2019-35-62/programs/hh-suite/bin/hhblits

#fi
if [ ! -s $OUTPUT ];then
    #srun -p shared
    ${hhblits_path} -i $FASTA -oa3m $OUTPUT -o /dev/null -cpu ${ncpus} -e 0.001 -maxseq 1000000 -realign_max 100000 -maxfilt 100000 -min_prefilter_hits 1000 -n 3 -d ${bfd_database_path}  -d ${uniclust30_database_path}
echo "$ID hhblits bfd done"
fi

