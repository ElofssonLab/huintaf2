#!/bin/bash -x

sed "s/,/ /g" data/humap_pairs.csv > foo

rm HuMAP-missing.txt
rm HuMAP-clusters/cluster-*.txt
while I= read -r i j k
do
    #echo $i,$j,$k
    if [ -d pdb/$j-$k/$j-$k.pdb ]
    then
	echo $j-$k >> HuMAP-clusters/cluster-$i.txt
    elif [ -f pdb/$k-$j/$k-$j.pdb ]
    then
	echo $k-$j  >> HuMAP-clusters/cluster-$i.txt
    elif [ -d CORUM/$j-$k/$j-$k.pdb ]
    then
	echo $j-$k >> HuMAP-clusters/cluster-$i.txt
    elif [ -f CORUM/$k-$j/$k-$j.pdb ]
    then
	echo $k-$j  >> HuMAP-clusters/cluster-$i.txt
    else
	echo $j-$k >> HuMAP-missing.txt
	#echo $j-$k
    fi
done < foo

sort -u HuMAP-missing.txt > foo
mv foo HuMAP-missing.txt

#for c in  CORUM-clusters/cluster*.txt
#do
#    j=`basename $c .txt`
#    rm -rf CORUM-clusters/$j/
#    mkdir -p CORUM-clusters/$j
#    unset line
#    for i in `cat $c`
#    do
#	line=`echo -n $line " " $i `
#	cp CORUM/${i}/${i}.pdb CORUM-clusters/$j/
#    done
#    echo $line > CORUM-clusters/$j/${j}-files.txt
#    #ls
#    bin/makecomplex.py --mintm 0.8 -i CORUM-clusters/$j/*pdb -o CORUM-clusters/$j/${j}_mintm08.pdb
#    # Some cleaning up
#    rm CORUM-clusters/$j/*_[A-Za-z].pdb
#    rm CORUM-clusters/$j/*_[A-Za-z].rot
#    rm CORUM-clusters/$j/*_[A-Za-z].MM
#done
