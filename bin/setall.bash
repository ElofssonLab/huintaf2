export NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'
COMMON="/proj/berzelius-2021-29/"
AFHOME="/proj/berzelius-2021-29/users/x_arnel/git/af2-multimer-mod/"
#AFHOME=$COMMON"/AF2-multimer-mod/" 			# Path of AF2-multimer-mod directory.
SINGULARITY=$COMMON"/AF2-multimer-mod//AF_data/alphafold-multimer.sif" 	# Path of singularity image.
PARAM=$COMMON"/AF2-multimer-mod/AF_data/" 				# path of param folder containing AF2 Neural Net parameters.
MAX_RECYCLES=10
export PRESET="model_1_multimer"
FOLDER="complexes/"
FASTA=complexes/ClpQ.fasta
