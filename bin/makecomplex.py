#!/usr/bin/env python3
import argparse
#from Bio.PDB.vectors import rotaxis, calc_angle, calc_dihedral
#from Bio.PDB.Polypeptide import is_aa
#from math import pi
import re
import math
import numpy  as np
from Bio.PDB import PDBIO
from Bio import SeqIO
from Bio.PDB import PDBParser
from Bio.PDB import MMCIFParser
from Bio.PDB import Selection
from Bio.PDB import MMCIFIO
from Bio.PDB import PDBIO
from Bio.PDB import Select
from Bio.PDB.Chain import Chain
#from Bio.PDB.Polypeptide import PPBuilder
import subprocess
import sys
import pandas as pd
from Bio.PDB import Entity



def center_of_mass(entity, geometric=False):
    """
    Returns gravitic [default] or geometric center of mass of an Entity.
    Geometric assumes all masses are equal (geometric=True)
    """
    
    # Structure, Model, Chain, Residue
    if isinstance(entity, Entity.Entity):
        atom_list = entity.get_atoms()
    # List of Atoms
    elif hasattr(entity, '__iter__') and [x for x in entity if x.level == 'A']:
        atom_list = entity
    else: # Some other weirdo object
        raise ValueError("Center of Mass can only be calculated from the following objects:\n"
                            "Structure, Model, Chain, Residue, list of Atoms.")
    
    masses = []
    positions = [ [], [], [] ] # [ [X1, X2, ..] , [Y1, Y2, ...] , [Z1, Z2, ...] ]
    
    for atom in atom_list:
        
        masses.append(atom.mass)
        
        for i, coord in enumerate(atom.coord.tolist()):
            positions[i].append(coord)

    # If there is a single atom with undefined mass complain loudly.
    if 'ukn' in set(masses) and not geometric:
        raise ValueError("Some Atoms don't have an element assigned.\n"
                         "Try adding them manually or calculate the geometrical center of mass instead.")
    
    if geometric:
        return [sum(coord_list)/len(masses) for coord_list in positions]
    else:       
        w_pos = [ [], [], [] ]
        for atom_index, atom_mass in enumerate(masses):
            w_pos[0].append(positions[0][atom_index]*atom_mass)
            w_pos[1].append(positions[1][atom_index]*atom_mass)
            w_pos[2].append(positions[2][atom_index]*atom_mass)

        return [sum(coord_list)/sum(masses) for coord_list in w_pos]

class ChainSelect(Select):
    def __init__(self,chain):
        self.chain=chain
    def accept_chain(self,chain):
        if (re.search(chain.get_id(),self.chain)):
            return 1
        else:
            return 0


def overlap(structure,chain,cutoff):
    overlap=np.zeros(len(chain))
    j=0
    for residue1 in chain:
        # Get some atoms
        ca1 = residue1["CA"]
        for c in structure[0]:
            for residue2 in c:
                #print ("Test",residue1,residue2)
                ca2 = residue2["CA"]
                distance = ca1 - ca2
                if (distance < cutoff):
                    overlap[j]+=1
                    break
        j+=1
    return(overlap)
        
def runMM(outnames1,outnames2,chains1,chains2):
    df_MM=pd.DataFrame(columns=['Name1', 'Name2',"File1","File2","Chain1","Chain2",'TM1',"TM2","RMSD","SeqID","MMfile","rotfile"])
    for i in outnames1:
        for j in outnames2:
            tempdata={"File1":i,"File2":j} # ,"OrgFile1":tempname1,"OrgFile2":tempname2}
            tempdata["Chain1"]=chains1[i]
            tempdata["Chain2"]=chains2[j]
            tempdata["Outdir"]=re.sub(r'\/.*','',i)
            name=re.sub(r'.*/','',i)
            name=re.sub(r'.pdb$','',name)
            name=re.sub(r'.cif$','',name)
            tempdata["Name1"]=name
            name=re.sub(r'.*/','',j)
            name=re.sub(r'.pdb$','',name)
            name=re.sub(r'.cif$','',name)
            tempdata["Name2"]=name
            tempdata["MMfile"]=tempdata["Outdir"]+"/"+tempdata["Name1"]+":"+tempdata["Name2"]+".MM"
            tempdata["rotfile"]=tempdata["Outdir"]+"/"+tempdata["Name1"]+":"+tempdata["Name2"]+".rot"
            with open(tempdata["MMfile"], "w+") as file:
                mm=subprocess.run([MM,"-m",tempdata["rotfile"],tempdata["File1"],tempdata["File2"]],stdout=file)
            file.close()
            file=open(tempdata["MMfile"], "r")
            for line in file.readlines():
                if re.match(r'TM-score=.*Chain_1',line):
                    tempdata["TM1"]=float(line.split()[1])
                elif re.match(r'TM-score=.*Chain_2',line):
                    tempdata["TM2"]=float(line.split()[1])
                elif re.match(r'Aligned.*RMSD',line):
                    tempdata["RMSD"]=float(re.sub(r'\,','',line.split()[4]))
                    tempdata["SeqID"]=float(line.split()[6])
                #print (tempdata)
            df_MM = df_MM.append(tempdata, ignore_index=True)
    df_MM["TM"]=df_MM[["TM1","TM2"]].max(axis=1)
    return (df_MM)


def rotate(file):    
    # Using readlines()
    rotfile = open(file, 'r')
    Lines = rotfile.readlines()
    translation_matrix=np.array((0,0,0),"f")
    rotation_matrix=np.array(((0,0,0),(0,0,0),(0,0,0)),"f")
    translation=[0,0,0]
    for line in Lines:
        if (line[0]=="0"):
            temp=line.split()
            #print (temp)
            translation_matrix[0]=1*float(temp[1])
            rotation_matrix[0,0]=1*float(temp[2])
            rotation_matrix[0,1]=1*float(temp[3])
            rotation_matrix[0,2]=1*float(temp[4])
        elif (line[0]=="1"):
            temp=line.split()
            translation_matrix[1]=1*float(temp[1])
            rotation_matrix[1,0]=1*float(temp[2])
            rotation_matrix[1,1]=1*float(temp[3])
            rotation_matrix[1,2]=1*float(temp[4])
        elif (line[0]=="2"):
            temp=line.split()
            translation_matrix[2]=1*float(temp[1])
            rotation_matrix[2,0]=1*float(temp[2])
            rotation_matrix[2,1]=1*float(temp[3])
            rotation_matrix[2,2]=1*float(temp[4])
            #rotation_matrix=rotation_matrix.T
    #print (rotation_matrix)
    #print (rotation_matrix[0,0])
    #print (translation_matrix)
    return(rotation_matrix,translation_matrix)

if __name__ == "__main__":
    arg_parser = argparse.\
        ArgumentParser(                description="Translate and rotate a pdb file")
    arg_parser.add_argument('-i','--input', nargs='+', help='List of input pdb files (currenly only 2)', required=True)
    arg_parser.add_argument("-o","--outfile", type=str,required=False)
    arg_parser.add_argument("-m","--maxoverlap", help="Maxmimum fraction of overlap to include chain", default=0.5, type=float,required=False)
    arg_parser.add_argument("-c","--cutoff",default=5,help="Distance cutoff to calculate overlap", type=float,required=False)
    arg_parser.add_argument("-t","--mintm",default=0.90,help="Min TM score for overlapping chains", type=float,required=False)
    
    args = arg_parser.parse_args()
    std = 1
    cutoff=args.cutoff
    maxoverlap=args.maxoverlap
    mintm=args.mintm
    MM="/scratch3/arnee/HuMap2/bin/MMalign"
    
    #name1=sys.argv[1]
    #name2=sys.argv[2]
    names=[]
    tempnames=[]
    structures=[]
    i=0
    for name in args.input:
        names+=[name]
        tempnames+=[re.sub(".pdb$","",re.sub(".cif$","",name))]
        try:
            parser = MMCIFParser()
            tempstruct = parser.get_structure("protein-"+str(i), name)
            io=PDBIO()
            io.set_structure(tempstruct)
            io.save(tempname[-1]+".pdb")
        except:
            parser = PDBParser()
            tempstruct = parser.get_structure("protein-"+str(i), name)
        structures+=[tempstruct]
        i+=1
    chains=[]
    df_structures=pd.DataFrame(columns=["StructureID",'Structure',"Chains","outnames","ChainIDs","Chaindict"])
    i=0
    #print (structures)
    for structure in structures:
        #print ("Str:",i,structure,structure[0])
        temp={"StructureID":i,"Structure":structure,"Chains":[],"ChainIDs":[],"outnames":[],"Chaindict":{}}
        io=PDBIO()
        io.set_structure(structure)
        for chain in structure.get_chains():
            temp["Chains"]+=[chain]
            temp["ChainIDs"]+=[chain.id]
            out=tempnames[i]+"_"+chain.id+".pdb"
            temp["outnames"]+=[out]
            temp["Chaindict"][out]=chain.id
            io.save(out,ChainSelect(chain.id))
        df_structures = df_structures.append(temp, ignore_index=True)
        i+=1
    # We need to sort structures in some way (perhaps starting with the first)
        
    #print (df_structures)
    outnames1=df_structures["outnames"][0]
    structure1=df_structures["Structure"][0]
    #structure2=df_structures["Structure"][1]
    chains1=df_structures["ChainIDs"][1]
    chaindict1=df_structures["Chaindict"][0]
    for i in df_structures.StructureID.to_list():
        print ("Trying to add structure:",i)
        if i==0: continue
        outnames2=df_structures["outnames"][i]
        chaindict2=df_structures["Chaindict"][i]
        df_MM=runMM(outnames1,outnames2,chaindict1,chaindict2)
        #print (df_MM)
        best=df_MM.sort_values("TM",ascending=False).iloc[0]
        if (best["TM"]<mintm):
            print ("Skipping, mintm:",mintm,best["TM"])
            continue 
        rotation_matrix,translation_matrix=rotate(best["rotfile"])
        structure1[0].transform(rotation_matrix.T, translation_matrix)
        #structure=structure1
        chains2=df_structures["ChainIDs"][i]
        structure2=df_structures["Structure"][i]
        #print (structure2,chains2)
        for chain in structure2.get_chains():
            print ("CHAIN",chain.id)
            if chain.id == best["Chain2"]:
                #print ("Detaching Structure2 chain:",chain.id)
                #structure2.detach_child[chain.id]
                continue
            while chain.id in chains1:
                #print ("test",chain.id,chains1,chains2)
                j=1
                while chr(ord(chain.id)+j) in chains2:
                    #print ("test2",i,chain.id)
                    j+=1
                chain.id=chr(ord(chain.id)+j)
            chains2+=[chain.id]
            #chain.serial_number=len(Chains1)+1
            chains1+=[chain.id]
            o=overlap(structure1,chain,cutoff)
            print ("Overlap",i,o,np.sum(o)/len(o))
            if (np.sum(o)/len(o)<maxoverlap):
                print ("Adding", i,chain.id)
                structure1[0].add(chain)
            else:
                print ("Skipping", i,chain.id)

    io = PDBIO()
    io.set_structure(structure1)
    io.save(args.outfile)

