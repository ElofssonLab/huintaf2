#!/bin/bash -x

while I= read -r i j k l n o
do
    if [ ! -f HuRI/${i}/${i}.MMall2 ]
    then
      bin/runMMall-HuRI.bash $i $k
    fi
    if [ ! -f HuRI/${i}/${i}.DockQall2 ]
    then
	touch HuRI/${i}/${i}.DockQall2
	m=`basename $j .pdb`
	bin/reversechains.py HuRI/${i}/${i}.pdb
	bin/matchpdb.py HuRI/${i}/${i}.pdb HuRI/${i}/${m}.pdb
	cp  HuRI/${i}/${m}.pdb HuRI/${i}/${m}_extra.pdb
	bin/matchpdb.py HuRI/${i}/${i}_reorder.pdb HuRI/${i}/${m}_extra.pdb
	pdb_reres HuRI/${i}/${m}_matching.pdb  > HuRI/${i}/${m}_renum.pdb 
	pdb_reres HuRI/${i}/${m}_extra_matching.pdb > HuRI/${i}/${m}_extra_matching_renum.pdb
	pdb_reres HuRI/${i}/${i}_matching.pdb >HuRI/${i}/${i}_matching_renum.pdb
	pdb_reres HuRI/${i}/${i}_reorder_matching.pdb >HuRI/${i}/${i}_reorder_matching_renum.pdb
	#pdb_reres $2 > $B
	#

	if [ -f HuRI/${i}/${i}*.MMall2 ]
	then
	    for p in HuRI/${i}/${i}*.MMall2
	    do
		pdb=`gawk '{print $2}' $p `
		chain=`gawk '{print $3}' $p `
		if [ -f cif/${pdb}_${chain}_rechain.pdb ]
		then
		    cp cif/${pdb}_${chain}_rechain.pdb HuRI/${i}/${m}_mmall.pdb 
		fi
	    done
	fi
	if [ -f cif/${k}_${l}${n}_rechain.pdb ]
	then
	    cp cif/${k}_${l}${n}_rechain.pdb HuRI/${i}/${m}_pdborg.pdb
	fi
	
	for a in HuRI/${i}/${i}*.pdb
	do
	    for b in  HuRI/${i}/${m}*.pdb
	    do
		A=`basename $a`
		B=`basename $b`
		python3 ~/git/DockQ/DockQ-mod.py -short -useCA  $a $b > HuRI/${i}/${A}-${B}.DockQall
	    done
	done
	python3 ~/git/DockQ/DockQ-mod.py -short -useCA HuRI/${i}/${i}.pdb HuRI/${i}/${m}.pdb > HuRI/${i}/${i}.DockQ 
	python3 ~/git/DockQ/DockQ-mod.py -short -useCA HuRI/${i}/${i}_reorder.pdb HuRI/${i}/${m}.pdb > HuRI/${i}/${i}.DockQ.reorder
	cp HuRI/${i}/${i}.DockQ HuRI/${i}/${i}-${m}.DockQall
	cp HuRI/${i}/${i}.DockQ.reorder HuRI/${i}/${i}-${m}_reorder.DockQall
	grep -H DockQ HuRI/${i}/${i}*.DockQall HuRI/${i}/${i}.DockQ HuRI/${i}/${i}.DockQ.reorder | sort -nk2 |tail -1 >  HuRI/${i}/${i}.DockQall2
	
    
    
	# Original and different chain order
	

	~/git/bioinfo-toolbox/trRosetta/MMalign HuRI/${i}/${i}.pdb HuRI/${i}/${m}.pdb > HuRI/${i}/${i}.MM
	~/git/bioinfo-toolbox/trRosetta/MMalign HuRI/${i}/${i}_reorder.pdb HuRI/${i}/${m}.pdb > HuRI/${i}/${i}.MM.reorder
    fi
    
done <  data/HuRI-petras.tsv

