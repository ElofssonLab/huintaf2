#!/bin/bash -x
#SBATCH -A berzelius-2021-64
#SBATCH --output=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/out/%j.out
#SBATCH --error=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/err/%j.err
#SBATCH --array=1-500
#SBATCH -n 1
#SBATCH --gpus-per-task=1  
#SBATCH -t 01:00:00

##### AF2 CONFIGURATION #####
BIND=/proj/berzelius-2021-29/
COMMON="/proj/berzelius-2021-29/users/x_arnel/"
BIN="/proj/berzelius-2021-29/users/x_arnel/git/huintaf2/bin/"
AFHOME=$COMMON"/git/af2-multimer-mod/" 		# Path of AF2-multimer-mod directory.
SINGULARITY="/proj/berzelius-2021-29/singularity_images/alphafold-multimer.sif" 	# Path of singularity image.
PARAM="/proj/berzelius-2021-29/AF2-multimer-mod/AF_data/"				# path of param folder containing AF2 Neural Net parameters.
MAX_RECYCLES=10

#### ONLY ONE OF THE FOLLOWING SHOULD BE UNCOMMENTED ####################
#MODEL_SET="model_1"							# Uncomment to run single standard model.
#MODEL_SET="model_1_ptm"						# Uncomment to run single ptm model.
MODEL_SET="model_1_multimer" 						# Uncomment to run single multimer model.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}"; done		# Uncomment to run all 5 standard models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_ptm"; done	# Uncomment to run all 5 ptm models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_multimer"; done	# Uncomment to run all 5 multimers models.
#########################################################################

### RUNTIME SPECIFICATIONS ###
LIST=$1 	# List with each line containing: fastaID(no format) [true or false] set true if protein is prokariote, false otherwise.
OFFSET=$2 	# ignore first $OFFSET lines of list; to set according to SLURM array size, in order to run all the list.
FOLDER="./AF-MULTI2" 	# Path where AF2 picks fasta files and generates its output folder structure.

POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
LINE=`tail -n+$POS $LIST | head -n 1`
#FASTA=`echo $LINE | awk '{print $1}'`	# path of input fasta file (without .fasta suffix)
#PRO=`echo $LINE | awk '{print $2}'`	# true if folding a prokariotic multimer, false otherwise
PRO=false

ml singularity/3.7.1

# We need to check for homology between pairs

PAIRMSA=""
MSAS=""
NAME=`echo $LINE | sed "s/\ /-/g"`
FASTA=${FOLDER}/${NAME}.fasta
rm -f $FASTA
mkdir -p ${FOLDER}/
#mkdir -p ${FOLDER}-msas/
MSAFOLDER="NEWMSA/"
for name in `echo $LINE`
do
    cat seq/${name}.fasta >> $FASTA
    mkdir -o ${MSAFOLDER}/${name}
    # We have a problem with specied in target name, so we add an extra empty entry first
    if [ ! -s ${MSAFOLDER}/${name}/${name}.a3m ]
    then
	sed  "s/^>sp.*$/> target/g" seq/${name}.fasta > ${MSAFOLDER}/${name}/${name}.a3m
	cat a3m/${name}.a3m >>  ${MSAFOLDER}/${name}/${name}.a3m
    fi
    if [ ! -s ${MSAFOLDER}/${name}/${name}-bfd.a3m ]
    then
	sed  "s/^>sp.*$/> target/g" seq/${name}.fasta > ${MSAFOLDER}/${name}/${name}-bfd.a3m
	cat a3m-bfd/${name}.a3m >>  ${MSAFOLDER}/${name}/${name}-bfd.a3m
    fi
    if [ ! -s ${MSAFOLDER}/${name}/${name}-JH.sto ]
    then
	#sed "s/GS 00000\|.*/GS 00000  Target/g" JH-STO/${name}.sto >  ${MSAFOLDER}/${name}/${name}-JH.sto
	a=`head -10 JH-STO/${name}.sto | grep =GS | head -1 `
	b=`echo $a |sed "s/OS=.*//"`
	sed "s/$a/$b/g" JH-STO/${name}.sto > ${MSAFOLDER}/${name}/${name}-JH.sto
    fi
    MSAS=${MSAS},${MSAFOLDER}/${name}/${name}.a3m,${MSAFOLDER}/${name}/${name}-bfd.a3m,${MSAFOLDER}/${name}/${name}-JH.sto
    PAIRMSAS=${PAIRMSAS},${MSAFOLDER}/${name}/${name}-JH.sto
done
MSAS=`echo $MSAS | sed "s/^,//g"`
PAIRMSAS=`echo $PAIRMSAS | sed "s/^,//g"`


$BIN/cdhit -c 0.9 -i ${FASTA} -o ${FASTA}.CD > ${FASTA}.cdhit
numclusters=`grep clusters ${FASTA}.cdhit | gawk '{print $3}'`

#if [ numclusters == 1 ]
#then
#    PAIR=""
#else
PAIR="--pairing_custom_msas=$PAIRMSAS"
#fi

#
for PRESET in $MODEL_SET; do
    ##### TO JUST FOLD, GIVEN AN AF DEFAULT FOLDER STRUCTURE WITH MSAS #####
    ##### ALREADY EXIST AT $FOLDER/$FASTA                              #####
    #singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
        #        python3 $AFHOME/alphafold/run_alphafold.py \
        #                --fasta_paths=$FOLDER/${FASTA}.fasta \
        #                --model_preset=$PRESET \
        #                --output_dir=$FOLDER \
        #                --is_prokaryote_list=$PRO \
        #                --data_dir=$PARAM

    
    
    ##### TO JUST FOLD, PROVIDING CUSTOM MSAS #####
    singularity exec --nv --bind $BIND:$BIND $SINGULARITY \
		python3 $AFHOME/alphafold/run_alphafold.py \
		--fasta_paths=${FASTA} \
		--model_preset=$PRESET \
		--output_dir=$FOLDER/ \
		--is_prokaryote_list=$PRO \
		--data_dir=$PARAM \
		--custom_msas=$MSAS $PAIR
    
done

rm -f ${FASTA}.CD*
rm -f ${FASTA}.cdhit
