#!/bin/bash -x

dir=`echo $1 | sed "s/\/$//g"`

mkdir -p ${dir}

rsync --exclude "*.sto" --exclude "*.a3m" --exclude "*.json" --exclude "*.fasta" --exclude "out/*"  --exclude "err/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/${dir}/ /scratch5/arnee/HuMap2/${dir}/


for i in ${dir}/*/un*pdb ; do bin/split_unrelaxed.bash $i ; done

bin/runallDockq-new.bash ${dir} map_petras.tsv


find ${dir} -size 0 -exec rm {} \;

for i in ${dir}/*/*DockQall2
do
    j=`basename $i .DockQall2`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  >  data/${dir}-DockQall.csv


find ${dir} -name "*-*.pdb" -exec bin/pDockQ.bash {} \;



grep -h Name ${dir}/*/*csv  | head -1 > data/${dir}-humap.csv
for i in ${dir}/*/
do
    j=`basename $i`
    grep -vh Name ${dir}/$j/$j.csv >> data/${dir}-humap.csv
done


cd data/
git add ${dir}*.csv
cd ..
