#!/usr/bin/env python3

import pandas as pd

df_hurimap=pd.read_csv("../data/HuRI-mappingPB.csv")
df=pd.read_csv("../data/HI-union.tsv",sep="\s+",names=["id1","id2"])

df_map=df_hurimap[["Id","Entry","Cross-reference (PDB)"]]
a=df[["id1"]]
b=df[["id2"]]
a=a.rename(columns={"id1":"Id"})
b=b.rename(columns={"id2":"Id"})
df_map=df_map.rename(columns={"Entry":"UniProt","Cross-reference (PDB)":"PDB"})


df_temp=pd.merge(df,df_map,left_on=["id1"],right_on=["Id"],how="outer")
df_HuRI=pd.merge(df_temp,df_map,left_on=["id2"],right_on=["Id"],how="outer")
df_HuRI=df_HuRI.dropna(subset=["UniProt_x","UniProt_y"])[["id1","id2","UniProt_x","UniProt_y","PDB_x","PDB_y"]].drop_duplicates()


df_HuRI=df_HuRI.rename(columns={
    "id1":"Ensembl1",
    "id2":"Ensembl2",
    "UniProt_x":"UniProt1",
    "UniProt_y":"UniProt2",
    "PDB_x":"PDB1",
    "PDB_y":"PDB2"}
)


df_HuRI["EnsName"]=df_HuRI["Ensembl1"]+"-"+df_HuRI["Ensembl2"]
df_HuRI["Name"]=['-'.join(sorted(tup)) for tup in zip(df_HuRI['UniProt1'], df_HuRI['UniProt2'])]
df_HuRI["id1"]=[(sorted(tup)[0]) for tup in zip(df_HuRI['UniProt1'], df_HuRI['UniProt2'])]
df_HuRI["id2"]=[(sorted(tup)[1]) for tup in zip(df_HuRI['UniProt1'], df_HuRI['UniProt2'])]


df_HuRI.to_csv("../data/HuRI-merged.csv")


# Now we also have to map this to the UniProt codes used in HuMap.

df_idmap=pd.read_csv("../data/HUMAN_9606_idmapping.tsv",sep="\s+",header=None,names=["id","foo","Ensembl"])
df_temp=pd.merge(df_HuRI,df_idmap,left_on=["Ensembl1"],right_on=["Ensembl"],how="outer")
df_temp=pd.merge(df_temp,df_idmap,left_on=["Ensembl2"],right_on=["Ensembl"],how="outer")
df_merged=df_temp.dropna(subset=["UniProt1","UniProt2","id1","id2"])[['Name','Ensembl1', 'Ensembl2', 'PDB1', 'PDB2','id1', 'id2']].drop_duplicates()

df_merged["EnsName"]=df_merged["Ensembl1"]+"-"+df_merged["Ensembl2"]
df_merged["SortedName"]=['-'.join(sorted(tup)) for tup in zip(df_merged['id1'], df_merged['id2'])]
df_merged["id1"]=[(sorted(tup)[0]) for tup in zip(df_merged['id1'], df_merged['id2'])]
df_merged["id2"]=[(sorted(tup)[1]) for tup in zip(df_merged['id1'], df_merged['id2'])]

df_merged.to_csv("../data/HuRI-merged-allnames.csv")

