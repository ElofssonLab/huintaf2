#!/bin/bash -x

# Trying to automatize AF2 runs


if [ $# == 1 ]
then
    list=`echo "$@" | tr " " "\n" ` 
else
    list=$@
fi
echo $list

list1=`echo $list | gawk '{print $1}'`
list2=`echo $list | gawk '{print $2}'`

#dir=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/
dir=./
fddir=/proj/berzelius-2021-29/users/x_arnel/git/FoldDock/
seq1=seq/$list1.fasta
seq2=seq/$list2.fasta

name1=`basename $seq1 .fasta`
name2=`basename $seq2 .fasta`

A3M1=a3m/${name1}.a3m
A3M2=a3m/${name2}.a3m
MGF=0.9

# if both does not exist exit

FUSEDMSA=merged/${name1}-${name2}_fused.a3m
PAIREDMSA=merged/${name1}-${name2}_paired.a3m

if [ -f $A3M1 ] && [ -f $A3M2  ] 
then
    if [ ! -f $PAIREDMSA ]
    then
	python3 ${dir}/bin/oxmatch.py --a3m1 $A3M1 --a3m2 $A3M2 --max_gap_fraction $MGF --outname $PAIREDMSA
    fi
    #if [ ! -f $FUSEDMSA ]
    #then
    #	python3 ${dir}/bin/fuse_msas.py --a3m1 $A3M1 --a3m2 $A3M2 --max_gap_fraction $MGF --outname $FUSEDMSA
    #fi
else
    exit 0
fi
