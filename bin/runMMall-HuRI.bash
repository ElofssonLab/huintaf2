#!/bin/bash -x


name=$1
code=$2

if [ ! -f HuRI/$1/$1.MMall2 ]
then
    touch -f HuRI/$1/$1.MMall2 
    for i in cif/$2_*.pdb # cif/$2.cif
    do
	j=`basename $i .pdb `
	if [ ! -f HuRI/$1/$1_$j.MMall ]
	then
	    touch HuRI/$1/$1_$j.MMall
	    bin/runMMscore.bash HuRI/$1/$1.pdb $i > HuRI/$1/$1_$j.MMall
	fi
    done

    # Find Max
    grep -H TM-score= HuRI/$1/${name}*.MM* | sort -nk 2 | tail -1 |  sed "s/.*\///g" | sed "s/.MM.*:/ /g" | sed "s/\_/ /g" > HuRI/$1/$1.MMall2
fi
