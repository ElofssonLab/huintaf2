#!/usr/bin/env python3

import pandas as pd
import numpy as np

df_dockqreorder=pd.read_csv("data/DockQreorder.csv",sep=",",names=["Name","DockQreorder"])
df_MMreorder=pd.read_csv("data/MMreorder.csv",sep=",",names=["Name","MMreorder"])
df_dockq=pd.read_csv("data/DockQ.csv",sep=",",names=["Name","DockQ"])
df_dockqall=pd.read_csv("data/DockQall.csv",sep=",",names=["Name","DockQall"])
df_MM=pd.read_csv("data/MM.csv",sep=",",names=["Name","MM"])
df_MMall=pd.read_csv("data/MMall.csv",sep=",",names=["Name","MMall"])
df_petras=pd.read_csv("data/map_petras.csv",sep=",")
df_seqlen=pd.read_csv("data/seqlen.csv",sep=",",names=["Name","SeqLen1","SeqLen2"])
df_seqlen["SeqLen"]=df_seqlen.SeqLen2+df_seqlen.SeqLen1

columns=["Name","NumRes","IF_plDDT","plDDT"]
columns2=["Name","NumResOverlap","IF_plDDTOverlap","plDDTOverlap"]

df_plDDT=pd.read_csv("data/pLDDT.csv",sep=",",names=columns)
#df_plDDT["SumIF"]=df_plDDT.IF_plDDT*df_plDDT.NumRes
df_plDDT["Dataset"]="HuMap2.0"
df_overlap=pd.read_csv("data/overlap.csv",sep=",",names=columns2)

df_temp=pd.merge(df_dockq,df_MM,on=["Name"],how="outer")
df_temp2=pd.merge(df_temp,df_MMall,on=["Name"],how="outer")
df_temp=pd.merge(df_temp2,df_MMreorder,on=["Name"],how="outer")
df_temp2=pd.merge(df_temp,df_dockqreorder,on=["Name"],how="outer")
df_temp=pd.merge(df_temp2,df_dockqall,on=["Name"],how="outer")
df_temp2=pd.merge(df_temp,df_petras,on=["Name"],how="outer")
df_temp=pd.merge(df_temp2,df_seqlen,on=["Name"],how="outer")
df_merged=pd.merge(df_temp,df_plDDT,on=["Name"],how="outer")
df_merged=pd.merge(df_merged,df_overlap,on=["Name"],how="outer")
df_merged=df_merged.dropna(subset=["NumRes","IF_plDDT"])
df_merged["SumIF"]=df_merged.NumRes*df_merged.IF_plDDT
df_merged[["id1","id2"]]=df_merged["Name"].str.split("-",n=2,expand=True)





df_HuRI_dockqreorder=pd.read_csv("data/HuRI-DockQreorder.csv",sep=",",names=["Name","DockQreorder"])
df_HuRI_MMreorder=pd.read_csv("data/HuRI-MMreorder.csv",sep=",",names=["Name","MMreorder"])
df_HuRI_dockq=pd.read_csv("data/HuRI-DockQ.csv",sep=",",names=["Name","DockQ"])
df_HuRI_dockqall=pd.read_csv("data/HuRI-DockQall.csv",sep=",",names=["Name","DockQall"])
df_HuRI_MM=pd.read_csv("data/HuRI-MM.csv",sep=",",names=["Name","MM"])
df_HuRI_MMall=pd.read_csv("data/HuRI-MMall.csv",sep=",",names=["Name","MMall"])

df_HuRI_petras=pd.read_csv("data/HuRI-petras.csv",sep=",")
df_HuRI_seqlen=pd.read_csv("data/HuRI-seqlen.csv",sep=",",names=["Name","SeqLen1","SeqLen2"])


df_HuRI_seqlen["SeqLen"]=df_HuRI_seqlen.SeqLen2+df_HuRI_seqlen.SeqLen1

columns=["Name","NumRes","IF_plDDT","plDDT"]
columns2=["Name","NumResOverlap","IF_plDDTOverlap","plDDTOverlap"]

df_HuRI_plDDT=pd.read_csv("data/HuRI-pLDDT.csv",sep=",")
#df_HuRI_plDDT["SumIF"]=df_HuRI_plDDT.IF_plDDT*df_HuRI_plDDT.NumRes
df_HuRI_plDDT["Dataset"]="HuMap2.0"
df_HuRI_overlap=pd.read_csv("data/HuRI-overlap.csv",sep=",",names=columns2)

df_HuRI_temp=pd.merge(df_HuRI_dockq,df_HuRI_MM,on=["Name"],how="outer")
df_HuRI_temp2=pd.merge(df_HuRI_temp,df_HuRI_MMall,on=["Name"],how="outer")
df_HuRI_temp=pd.merge(df_HuRI_temp2,df_HuRI_MMreorder,on=["Name"],how="outer")
df_HuRI_temp2=pd.merge(df_HuRI_temp,df_HuRI_dockqreorder,on=["Name"],how="outer")
df_HuRI_temp=pd.merge(df_HuRI_temp2,df_HuRI_dockqall,on=["Name"],how="outer")
df_HuRI_temp2=pd.merge(df_HuRI_temp,df_HuRI_petras,on=["Name"],how="outer")
df_HuRI_temp=pd.merge(df_HuRI_temp2,df_HuRI_seqlen,on=["Name"],how="outer")
df_HuRI_merged=pd.merge(df_HuRI_temp,df_HuRI_plDDT,on=["Name"],how="outer")
df_HuRI_merged=pd.merge(df_HuRI_merged,df_HuRI_overlap,on=["Name"],how="outer")
df_HuRI_merged=df_HuRI_merged.dropna(subset=["NumRes","IF_plDDT"])
df_HuRI_merged["SumIF"]=df_HuRI_merged.NumRes*df_HuRI_merged.IF_plDDT
df_HuRI_merged[["id1","id2"]]=df_HuRI_merged["Name"].str.split("-",n=2,expand=True)






def sigmoid(x, L ,x0, k, b):
    y = L / (1 + np.exp(-k*(x-x0)))+b
    return (y)

popt=[7.07140240e-01, 3.88062162e+02, 3.14767156e-02, 3.13182907e-02]
tiny=1.e-20
df_merged["pDockQ"]=sigmoid(np.log(df_merged.NumRes+tiny)*df_merged.IF_plDDT,*popt)
df_merged.to_csv("data/evaluation.csv")

popt=[7.07140240e-01, 3.88062162e+02, 3.14767156e-02, 3.13182907e-02]
tiny=1.e-20
df_HuRI_merged["pDockQ"]=sigmoid(np.log(df_HuRI_merged.NumRes+tiny)*df_HuRI_merged.IF_plDDT,*popt)
#df_HuRI_merged.to_csv("data/HuRI-evaluation.csv")



# Huei
#df_HuRI=pd.read_csv("./data/HuRI-pLDDT.csv")
df_HuRI=df_HuRI_merged
#df_hurimap=pd.read_csv("./data/merged_evaluation.csv")
df_hurimap=pd.read_csv("./data/humap_ensembl_id.csv")
df_hurimap["Name"]=df_hurimap.Ensembl_id1+"-"+df_hurimap.Ensembl_id2
a=df_hurimap[["Id1","Ensembl_id1"]]
b=df_hurimap[["Id2","Ensembl_id2"]]
a=a.rename(columns={"Id1":"UniProt","Ensembl_id1":"Ensembl"})
b=b.rename(columns={"Id2":"UniProt","Ensembl_id2":"Ensembl"})
#df_ensmap=pd.concat([a,b]).drop_duplicates()
df_ensmap=pd.read_csv("./data/HUMAN_9606_idmapping_subset.tsv",sep="\s+",names=["UniProt","temp","Ensembl"])
df_ensmap_full=pd.read_csv("./data/HUMAN_9606_idmapping.tsv",sep="\s+",names=["UniProt","temp","Ensembl"])
df_ensmap_full = df_ensmap_full.drop_duplicates(subset=['Ensembl'], keep='first')
df_ensmap=pd.concat([df_ensmap,df_ensmap_full]).drop_duplicates(subset=['Ensembl'], keep='first')
#print (df_ensmap)
#sys.exit()
df_temp=pd.merge(df_HuRI,df_ensmap,left_on=["id1"],right_on=["Ensembl"],how="outer")
df_HuRI_all=pd.merge(df_temp,df_ensmap,left_on=["id2"],right_on=["Ensembl"],how="outer")
df_HuRI=df_HuRI_all.dropna(subset=["UniProt_x","UniProt_y"])
#df_HuRI=df_HuRI_all
#df_HuRI["Name1"]=df_HuRI["UniProt_x"]+"-"+df_HuRI["UniProt_y"]
#df_HuRI["Name2"]=df_HuRI["UniProt_y"]+"-"+df_HuRI["UniProt_x"]
df_HuRI["SortedName"]=['-'.join(sorted(tup)) for tup in zip(df_HuRI['UniProt_x'], df_HuRI['UniProt_y'])]


#df_HuRI_all.to_csv("../data/Hurimapping_full.csv")


df_HuRI=df_HuRI.rename(columns={
    "Name":"EnsName",
    "SortedName":"Name",
    "id1":"Gene1",
    "id2":"Gene2",
    "UniProt_x":"id1",
    "UniProt_y":"id2",
})

#df_HuRI["pDockQ"]=sigmoid(np.log(df_HuRI.NumRes+tiny)*df_HuRI.IF_plDDT,*popt)
#df_HuRI[["Name","id1","id2","EnsName","Gene1","Gene2","NumRes","IF_plDDT","plDDT","pDockQ"]].to_csv("./data/Hurimapping.csv")
df_HuRI=df_HuRI.drop(columns=["temp_x","temp_y"])
df_HuRI.to_csv("./data/Hurimapping.csv")




df_marks=pd.read_csv("data/Marks-pLDDT.csv")
df_marks_overlap=pd.read_csv("data/Marks-overlap.csv")
df_marks_dockq=pd.read_csv("data/dockqstats_marks_af.csv")
df_temp=df_marks_dockq[["complex_id","DockQ_dockqstats_marks_af2_pairedandfused_model_1_rec10_run1"]]
df_temp=df_temp.rename(columns={
    "complex_id":"Name"})


df_marks=pd.merge(df_marks,df_marks_overlap,on=["Name"])
df_marks=pd.merge(df_marks,df_temp,on=["Name"])

df_marks=df_marks.rename(columns={
    "id1_x":"id1",
    "id2_x":"id2",
    "NumRes_x":"NumRes",
    "plDDT_x":"plDDT",
    "IF_plDDT_x":"IF_plDDT",
    "NumRes_y":"Overlap",
    "DockQ_dockqstats_marks_af2_pairedandfused_model_1_rec10_run1":"DockQ",
})

df_marks["pDockQ"]=sigmoid(np.log(df_marks.NumRes+tiny)*df_marks.IF_plDDT,*popt)
df_marks[["Name","id1","id2","NumRes","IF_plDDT","plDDT","Overlap","DockQ","pDockQ"]].to_csv("data/Marks.csv")

         
