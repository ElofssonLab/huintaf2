#!/bin/bash -x
#SBATCH -A berzelius-2021-64
#SBATCH --output=out/AF%j.out
#SBATCH --error=err/AF%j.err
#SBATCH --array=1-2
#SBATCH -N 1
#SBATCH --gpus=1
#SBATCH -t 01:00:00


##For multiple GPUs
##SBATCH --exlusive
export NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'


##### AF2 CONFIGURATION #####
COMMON="/proj/berzelius-2021-29/"
AFHOME=$COMMON"/AF2-multimer-mod/" 			# Path of AF2-multimer-mod directory.
SINGULARITY=$AFHOME"/AF_data/alphafold-multimer.sif" 	# Path of singularity image.
PARAM=$AFHOME"/AF_data/" 				# path of param folder containing AF2 Neural Net parameters.
MAX_RECYCLES=10

#### ONLY ONE OF THE FOLLOWING SHOULD BE UNCOMMENTED ####################
#MODEL_SET="model_1"							# Uncomment to run single standard model.
#MODEL_SET="model_1_ptm"						# Uncomment to run single ptm model.
MODEL_SET="model_1_multimer" 						# Uncomment to run single multimer model.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}"; done		# Uncomment to run all 5 standard models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_ptm"; done	# Uncomment to run all 5 ptm models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_multimer"; done	# Uncomment to run all 5 multimers models.
#########################################################################

### RUNTIME SPECIFICATIONS ###
LIST=$1 	# List with each line containing: fastaID(no format) [true or false] set true if protein is prokariote, false otherwise.
OFFSET=$2 	# ignore first $OFFSET lines of list; to set according to SLURM array size, in order to run all the list.
#FOLDER=$3 	# Path where AF2 picks fasta files and generates its output folder structure.
FOLDER="AF-MULTI-PDB/"

POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
LINE=`tail -n+$POS $LIST | head -n 1`
id1=`echo $LINE | awk '{print $1}'`	# path of input fasta file (without .fasta suffix)
id2=`echo $LINE | awk '{print $2}'`	# path of input fasta file (without .fasta suffix)
FASTA=${FOLDER}"/${id1}-${id2}.fasta"
#PRO=`echo $LINE | awk '{print $2}'`	# true if folding a prokariotic multimer, false otherwise
PRO=false,false

if [[ ! -f  ${FOLDER}/unrelaxed_model_1_multimer.pdb ]]
then
    for PRESET in $MODEL_SET; do
        ##### TO JUST FOLD, GIVEN AN AF DEFAULT FOLDER STRUCTURE WITH MSAS #####
        ##### ALREADY EXIST AT $FOLDER/$FASTA                              #####
        singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
                python3 $AFHOME/alphafold/run_alphafold.py \
                        --fasta_paths=${FASTA} \
                        --model_preset=$PRESET \
                        --output_dir=$FOLDER \
                        --data_dir=$PARAM
                        #--is_prokaryote_list=$PRO \

#	##### TO JUST FOLD, PROVIDING CUSTOM MSAS #####
#	singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
#		python3 $AFHOME/alphafold/run_alphafold.py \
#			--fasta_paths=$FOLDER/${FASTA}.fasta \
#			--model_preset=$PRESET \
#			--output_dir=$FOLDER \
#			--is_prokaryote_list=$PRO \
#			--data_dir=$PARAM \
#			--custom_msas=$MSAS

    done
fi
