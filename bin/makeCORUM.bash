#!/bin/bash -x

find pdb-multi/ -name "unrelaxed_model_1.pdb" -exec  bin/split_unrelaxed.bash {} \;
find pdb-multi/ -name "*-*.pdb" -exec  bin/pDockQ.bash {} \;


d=`pwd`
name=`basename $d`

grep -h Name pdb-multi/*/*csv  | head -1 > data/${name}-pdockq.csv
grep -vh Name pdb-multi/*/*csv >> data/${name}-pdockq.csv


