#!/bin/bash -x
#SBATCH -A Berzelius-2022-106
#SBATCH --output=out/AFmono%j.out
#SBATCH --error=err/AFmono%j.err
#SBATCH --array=1-4
#SBATCH -N 1
#SBATCH --gpus=1
#SBATCH --export=ALL,CUDA_VISIBLE_DEVICES
#SBATCH -t 72:00:00


# #SBATCH --exclusive

xport NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'



##### AF2 CONFIGURATION #####
COMMON="/proj/berzelius-2021-29/"
AFHOME="/proj/berzelius-2021-29/users/x_arnel/git/alphafold/"
#AFHOME=$COMMON"/AF2-multimer-mod/" 			# Path of AF2-multimer-mod directory.
#SINGULARITY=$COMMON"/AF2-multimer-mod//AF_data/alphafold-multimer.sif" 	# Path of singularity image.
SINGULARITY=$COMMON"/singularity_images/alphafold.sif" 	# Path of singularity image.
PARAM=$COMMON"/AF2-multimer-mod/AF_data/" 				# path of param folder containing AF2 Neural Net parameters.
MAX_RECYCLES=10

#### ONLY ONE OF THE FOLLOWING SHOULD BE UNCOMMENTED ####################
#MODEL_SET="model_1"							# Uncomment to run single standard model.
#MODEL_SET="model_1_ptm"						# Uncomment to run single ptm model.
#MODEL_SET="model_1_multimer" 						# Uncomment to run single multimer model.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}"; done		# Uncomment to run all 5 standard models.
for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_ptm"; done	# Uncomment to run all 5 ptm models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_multimer"; done	# Uncomment to run all 5 multimers models.
#########################################################################

### RUNTIME SPECIFICATIONS ###
LIST=$1 	# List with each line containing: fastaID(no format) [true or false] set true if protein is prokariote, false otherwise.
OFFSET=$2 	# ignore first $OFFSET lines of list; to set according to SLURM array size, in order to run all the list.
#FOLDER=$3 	# Path where AF2 picks fasta files and generates its output folder structure.
FOLDER="single/"
POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
LINE=`tail -n+$POS $LIST | head -n 1`
id=`echo $LINE | sed "s/\ /-/g" `	# path of input fasta file (without .fasta suffix)


# Trying to automatize AF2 runs

#dir=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/
dir=./
fddir=/proj/berzelius-2021-29/users/x_arnel/git/FoldDock/
seq1=seq/$id.fasta
#seq2=seq/$2.fasta
#id=$1
name1=`basename $seq1 .fasta`
#name2=`basename $seq2 .fasta`

A3M1=a3m/${name1}.a3m
#A3M2=a3m/${name2}.a3m
MGF=0.9

# if both does not exist exit


#CB=`${dir}/bin/seqlen.bash seq/${name1}.fasta`
FASTAFILE=$seq1

#MSAS="$A3M1" #Comma separated list of msa paths
#MSAS="MSAs/"${id}"/A/bfdextra.a3m"
#MSAS=${MSAS}",MSAs/"${id}"/A/bfd_uniclust_hits.a3m"
MSAS="MSAs/"${id}"/A/bfd_uniclust_hits.a3m"
#MSAS=${MSAS}",MSAs/"${id}"/A/"${id}"-hits-search.sto"
MSAS=${MSAS}",MSAs/"${id}"/A/mgnify_hits.sto"
#MSAS=${MSAS}",MSAs/"${id}"/A/pdb_hits.hhr"
MSAS=${MSAS}",MSAs/"${id}"/A/uniref90_hits.sto"

#MSAS="MSAs/"${id}".a3m"

AFHOME=${fddir}/src/alphafold/ # Path of alphafold directory in FoldDock 
SINGULARITY=/proj/berzelius-2021-29/singularity_images/alphafold.sif
PARAM=/proj/berzelius-2021-29/Database/af_params/  #Path to AF2 params \
OUTFOLDER=single-hits/ # Path where AF2 generates its output folder structure

PRESET='full_dbs' #Choose preset model configuration - no ensembling (full_dbs) and (reduced_dbs) or 8 model ensemblings (casp14). \
MAX_RECYCLES=50 #max_recycles (default=3) \

COMMON=/proj/

NAME=${name1}
#pushd $AFHOME
#cd $AFHOME
#NAME="./" # to not run this part

for MODEL in $MODEL_SET
do
    if [ ! -d $OUTFOLDER/$NAME/relaxed_${MODEL}.pdb ]
    then
	mkdir -p $OUTFOLDER/$NAME/
	singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
		    python3 $AFHOME/run_alphafold.py \
                    --fasta_paths=$FASTAFILE \
                    --msas=$MSAS \
                    --output_dir=$OUTFOLDER \
                    --model_names=$MODEL \
                    --data_dir=$PARAM \
                    --fold_only \
                    --uniref90_database_path='' \
                    --mgnify_database_path='' \
                    --bfd_database_path='' \
                    --uniclust30_database_path='' \
                    --pdb70_database_path='' \
                    --template_mmcif_dir='' \
                    --obsolete_pdbs_path='' \
                    --preset=$PRESET \
                    --max_recycles=$MAX_RECYCLES \
		    --relax=True
	#popd                 --chain_break_list= \
	    fi
done
