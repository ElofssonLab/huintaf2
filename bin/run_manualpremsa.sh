#!/bin/bash -x

module load Anaconda/2021.05-nsc1
conda activate af_server
export PATH=/proj/beyondfold/apps/kalign-3.3.2/bin:$PATH
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'

fasta=$1
outdir=$2
type=$3
norelax=$4

alphafold=/proj/beyondfold/apps/alphafold/run_alphafold.py
#alphafold=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/huintaf2/bin/run_alphafold.py
if [[ $type == "multimer" ]]; then
	flagfile=/proj/beyondfold/apps/alphafold/flagfiles/multimer_full_dbs.flag
elif [[ $type == "multimer_1" ]]; then
#--num_multimer_predictions_per_model=1
        flagfile=/proj/beyondfold/apps/alphafold/flagfiles/multimer_full_dbs_1.flag
elif [[ $type == "multimer_templates" ]]; then
    flagfile=/proj/berzelius-2021-29/users/x_arnel/bin/multimertemplates.flag
elif [[ $type == "monomer_templates" ]]; then
    flagfile=/proj/berzelius-2021-29/users/x_arnel/bin/monomerstemplates.flag
else
        flagfile=/proj/beyondfold/apps/alphafold/flagfiles/monomer_full_dbs.flag
fi
echo python $alphafold  --recycles=24  --use_precomputed_msas --flagfile $flagfile --output_dir $outdir --fasta_paths $fasta $norelax
python $alphafold --recycles=24 --use_precomputed_msas -flagfile $flagfile --output_dir $outdir --fasta_paths $fasta $norelax  > err/log.$$ 2>&1

