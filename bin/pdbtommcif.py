#!/usr/bin/env python
"""
Script to convert mmCIF files to PDB format.
usage: python cif2pdb.py ciffile [pdbfile]
Requires python BioPython (`pip install biopython`). It should work with recent version of python 2 or 3.
@author Spencer Bliven <spencer.bliven@gmail.com>
"""

import sys
import argparse
import logging
from Bio.PDB.MMCIFParser import MMCIFParser
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB import PDBIO
from Bio.PDB import MMCIFIO


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert PDB to mmCIF format')
    parser.add_argument("ciffile",help="mmCIF output file")
    parser.add_argument("pdbfile",help="PDB output ")
    args = parser.parse_args()


    ciffile = args.ciffile
    pdbfile = args.pdbfile
    #Not sure why biopython needs this to read a cif file
    strucid = ciffile[:4] if len(ciffile)>4 else "1xxx"

    # Read file
    parser = PDBParser()
    structure = parser.get_structure(strucid, pdbfile)
    #
    #Write PDB
    io = MMCIFIO()

    print (structure)
    print (structure.header)
    io.set_structure(structure)
    #TODO What happens with large structures?
    io.save(ciffile)
