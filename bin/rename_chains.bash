#!/bin/bash -x
for i in cif/*_*pdb
do
    j=`basename $i .pdb `
    c=`echo $j |sed "s/.*_//g "`
    a=${c:0:1}
    b=${c:1:1}
    echo $i $a $b $c
    pdb_rplchain -${a}:A $i  | pdb_rplchain -${b}:B > cif/${j}_rechain.pdb
done
