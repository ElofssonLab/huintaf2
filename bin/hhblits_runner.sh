#!/bin/bash -x
#SBATCH -A SNIC2021-5-297
#SBATCH --output=./out/hh%j.out
#SBATCH --error=./err/hh%j.err
#SBATCH --array=1-1000
#SBATCH -c 16
#SBATCH -t 04:00:00

LIST=$1
OFFSET=$2
FOLDER=./MSAs/

POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
ID=`tail -n+$POS $LIST | head -n 1`

COMMON=/proj/snic2019-35-62/

BFD=$COMMON/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt
UNICLUST=$COMMON/databases/uniclust30/uniclust30_2018_08/uniclust30_2018_08

if [[ ! -d "$FOLDER/${ID}/msas/A/" ]]
then
    mkdir -p $FOLDER/${ID}/msas/A/
fi

if [[ ! -f "${FOLDER}/${ID}/msas/A/bfd_uniclust_hits.a3m" ]]
then 
    echo 'Processing ' $id '...'
    singularity exec --nv --bind $COMMON:$COMMON \
	$COMMON/AF2-multimer-mod/AF_data/alphafold-multimer.sif hhblits \
		-i seq/${ID}.fasta \
		-oa3m ${FOLDER}/${ID}/msas/A/bfd_uniclust_hits.a3m \
		-o out/${ID}.out \
		-d $UNICLUST \
		-d $BFD \
		-e 0.001 \
		-n 3 \
		-cpu 16 \
		-maxseq 1000000 \
		-maxfilt 100000 \
		-min_prefilter_hits 1000 \
		-realign_max 100000 
fi
