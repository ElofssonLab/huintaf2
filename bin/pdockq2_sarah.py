from Bio.PDB import PDBIO
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.Selection import unfold_entities

import numpy as np
import sys, os
import argparse
import pickle
import itertools
import pandas as pd
from scipy.optimize import curve_fit
from collections import defaultdict


parser = argparse.ArgumentParser(description="""Calculate chain_level pDockQ_i. """)
parser.add_argument("-pkl", nargs=1, type=str, required=True, help="Input pickle file.")
parser.add_argument("-pdb", nargs=1, type=str, required=True, help="Input pdb file.")
parser.add_argument("-tsv", type=str, default=None, help="Input tsv file.")
parser.add_argument("-pdbid", type=str, default=None, help="Id for current protein.")
parser.add_argument(
    "-cor_res",
    type=int,
    default=0,
    help="Define whether protein is a negative or positive sample.",
)
parser.add_argument(
    "-dist", help="maximum distance of a contact", nargs="?", type=int, default=8
)


# Methods for pdockq1
def parse_atm_record(line):
    """Get the atm record"""
    record = defaultdict()
    record["name"] = line[0:6].strip()
    record["atm_no"] = int(line[6:11])
    record["atm_name"] = line[12:16].strip()
    record["atm_alt"] = line[17]
    record["res_name"] = line[17:20].strip()
    record["chain"] = line[21]
    record["res_no"] = int(line[22:26])
    record["insert"] = line[26].strip()
    record["resid"] = line[22:29]
    record["x"] = float(line[30:38])
    record["y"] = float(line[38:46])
    record["z"] = float(line[46:54])
    record["occ"] = float(line[54:60])
    record["B"] = float(line[60:66])
    # print(record['B'])
    return record


def read_pdb(pdbfile):
    """Read a pdb file predicted with AF and rewritten to conatin all chains"""

    chain_coords, chain_plddt = {}, {}
    with open(pdbfile, "r") as file:
        for line in file:
            if not line.startswith("ATOM"):
                continue
            record = parse_atm_record(line)
            # Get CB - CA for GLY
            if record["atm_name"] == "CB" or (
                record["atm_name"] == "CA" and record["res_name"] == "GLY"
            ):
                if record["chain"] in [*chain_coords.keys()]:
                    chain_coords[record["chain"]].append(
                        [record["x"], record["y"], record["z"]]
                    )
                    chain_plddt[record["chain"]].append(record["B"])
                else:
                    chain_coords[record["chain"]] = [
                        [record["x"], record["y"], record["z"]]
                    ]
                    chain_plddt[record["chain"]] = [record["B"]]

    # Convert to arrays
    for chain in chain_coords:
        chain_coords[chain] = np.array(chain_coords[chain])
        chain_plddt[chain] = np.array(chain_plddt[chain])

    return chain_coords, chain_plddt


def calc_pdockq(chain_coords, chain_plddt, t):
    """Calculate the pDockQ scores
    pdockQ = L / (1 + np.exp(-k*(x-x0)))+b
    L= 0.724 x0= 152.611 k= 0.052 and b= 0.018
    """

    # Get coords and plddt per chain
    ch1, ch2 = [*chain_coords.keys()]
    coords1, coords2 = chain_coords[ch1], chain_coords[ch2]
    plddt1, plddt2 = chain_plddt[ch1], chain_plddt[ch2]

    # Calc 2-norm
    mat = np.append(coords1, coords2, axis=0)
    a_min_b = mat[:, np.newaxis, :] - mat[np.newaxis, :, :]
    dists = np.sqrt(np.sum(a_min_b.T**2, axis=0)).T
    l1 = len(coords1)
    contact_dists = dists[:l1, l1:]  # upper triangular --> first dim = chain 1
    contacts = np.argwhere(contact_dists <= t)

    if contacts.shape[0] < 1:
        pdockq = 0
        ppv = 0
    else:
        # Get the average interface plDDT
        avg_if_plddt = np.average(
            np.concatenate(
                [plddt1[np.unique(contacts[:, 0])], plddt2[np.unique(contacts[:, 1])]]
            )
        )
        # Get the number of interface contacts
        n_if_contacts = contacts.shape[0]
        x = avg_if_plddt * np.log10(n_if_contacts)
        pdockq = 0.724 / (1 + np.exp(-0.052 * (x - 152.611))) + 0.018

        # PPV
        PPV = np.array(
            [
                0.98128027,
                0.96322524,
                0.95333044,
                0.9400192,
                0.93172991,
                0.92420274,
                0.91629946,
                0.90952562,
                0.90043139,
                0.8919553,
                0.88570037,
                0.87822061,
                0.87116417,
                0.86040801,
                0.85453785,
                0.84294946,
                0.83367787,
                0.82238224,
                0.81190228,
                0.80223507,
                0.78549007,
                0.77766077,
                0.75941223,
                0.74006263,
                0.73044282,
                0.71391784,
                0.70615739,
                0.68635536,
                0.66728511,
                0.63555449,
                0.55890174,
            ]
        )

        pdockq_thresholds = np.array(
            [
                0.67333079,
                0.65666073,
                0.63254566,
                0.62604391,
                0.60150931,
                0.58313803,
                0.5647381,
                0.54122438,
                0.52314392,
                0.49659878,
                0.4774676,
                0.44661346,
                0.42628389,
                0.39990988,
                0.38479715,
                0.3649393,
                0.34526004,
                0.3262589,
                0.31475668,
                0.29750023,
                0.26673725,
                0.24561247,
                0.21882689,
                0.19651314,
                0.17606258,
                0.15398168,
                0.13927677,
                0.12024131,
                0.09996019,
                0.06968505,
                0.02946438,
            ]
        )
        inds = np.argwhere(pdockq_thresholds >= pdockq)
        if len(inds) > 0:
            ppv = PPV[inds[-1]][0]
        else:
            ppv = PPV[0]

    return pdockq, ppv


def get_pdockq(filename):
    chain_coords, chain_plddt = read_pdb(filename)
    # Check chains
    if len(chain_coords.keys()) < 2:
        print("Only one chain in pdbfile", model)
        sys.exit()

    # Calculate pdockq
    t = 8  # Distance threshold, set to 8 Å
    pdockq, ppv = calc_pdockq(chain_coords, chain_plddt, t)
    return pdockq, ppv


# Methods for pdockq2
def retrieve_IFplddt(structure, chain1, chain2_lst, max_dist):
    ## generate a dict to save IF_res_id
    chain_lst = list(chain1) + chain2_lst

    ifplddt = []
    contact_chain_lst = []
    for res1 in structure[0][chain1]:
        for chain2 in chain2_lst:
            count = 0
            for res2 in structure[0][chain2]:
                if res1.has_id("CA") and res2.has_id("CA"):
                    dis = abs(res1["CA"] - res2["CA"])
                    ## add criteria to filter out disorder res
                    if dis <= max_dist:
                        ifplddt.append(res1["CA"].get_bfactor())
                        count += 1

                elif res1.has_id("CB") and res2.has_id("CB"):
                    dis = abs(res1["CB"] - res2["CB"])
                    if dis <= max_dist:
                        ifplddt.append(res1["CB"].get_bfactor())
                        count += 1
            if count > 0:
                contact_chain_lst.append(chain2)
    contact_chain_lst = sorted(list(set(contact_chain_lst)))

    if len(ifplddt) > 0:
        IF_plddt_avg = np.mean(ifplddt)
    else:
        IF_plddt_avg = 0

    return IF_plddt_avg, contact_chain_lst


def retrieve_IFPAEinter(structure, paeMat, contact_lst, max_dist):
    ## contact_lst:the chain list that have an interface with each chain. For eg, a tetramer with A,B,C,D chains and A/B A/C B/D C/D interfaces,
    ##             contact_lst would be [['B','C'],['A','D'],['A','D'],['B','C']]

    chain_lst = [x.id for x in structure[0]]
    seqlen = [len(x) for x in structure[0]]
    ifch1_col = []
    ifch2_col = []
    ch1_lst = []
    ch2_lst = []
    ifpae_avg = []
    d = 10
    for ch1_idx in range(len(chain_lst)):
        ## extract x axis range from the PAE matrix
        idx = chain_lst.index(chain_lst[ch1_idx])
        ch1_sta = sum(seqlen[:idx])
        ch1_end = ch1_sta + seqlen[idx]
        ifpae_col = []
        ## for each chain that shares an interface with chain1, retrieve the PAE matrix for the specific part.
        for contact_ch in contact_lst[ch1_idx]:
            index = chain_lst.index(contact_ch)
            ch_sta = sum(seqlen[:index])
            ch_end = ch_sta + seqlen[index]
            remain_paeMatrix = paeMat[ch1_sta:ch1_end, ch_sta:ch_end]
            # print(contact_ch, ch1_sta, ch1_end, ch_sta, ch_end)

            ## get avg PAE values for the interfaces for chain 1
            mat_x = -1
            for res1 in structure[0][chain_lst[ch1_idx]]:
                mat_x += 1
                mat_y = -1
                for res2 in structure[0][contact_ch]:
                    mat_y += 1
                    if res1["CA"] - res2["CA"] <= max_dist:
                        ifpae_col.append(remain_paeMatrix[mat_x, mat_y])
        ## normalize by d(10A) first and then get the average
        if not ifpae_col:
            ifpae_avg.append(0)
        else:
            norm_if_interpae = np.mean(1 / (1 + (np.array(ifpae_col) / d) ** 2))
            ifpae_avg.append(norm_if_interpae)

    return ifpae_avg


def calc_pmidockq(ifpae_norm, ifplddt):
    df = pd.DataFrame()
    df["ifpae_norm"] = ifpae_norm
    df["ifplddt"] = ifplddt
    df["prot"] = df.ifpae_norm * df.ifplddt
    fitpopt = [
        1.31034849e00,
        8.47326239e01,
        7.47157696e-02,
        5.01886443e-03,
    ]  ## from orignal fit function
    df["pmidockq"] = sigmoid(df.prot.values, *fitpopt)

    return df


def sigmoid(x, L, x0, k, b):
    y = L / (1 + np.exp(-k * (x - x0))) + b
    return y


def fit_newscore(df, column):
    testdf = df[df[column] > 0]

    colval = testdf[column].values
    dockq = testdf.DockQ.values
    xdata = colval[np.argsort(colval)]
    ydata = dockq[np.argsort(dockq)]

    p0 = [
        max(ydata),
        np.median(xdata),
        1,
        min(ydata),
    ]  # this is an mandatory initial guess
    popt, pcov = curve_fit(sigmoid, xdata, ydata, p0)  # method='dogbox', maxfev=50000)

    #    tiny=1.e-20
    #    print('L=',np.round(popt[0],3),'x0=',np.round(popt[1],3), 'k=',np.round(popt[2],3), 'b=',np.round(popt[3],3))

    ## plotting
    #    x_pmiDockQ = testdf[column].values
    #    x_pmiDockQ = x_pmiDockQ[np.argsort(x_pmiDockQ)]
    #    y_pmiDockQ = sigmoid(x_pmiDockQ, *popt)
    #    print("Average error for sigmoid fit is ", np.average(np.absolute(y_pmiDockQ-ydata)))

    # sns.kdeplot(data=df,x=column,y='DockQ',kde=True,levels=5,fill=True, alpha=0.8, cut=0)
    #    sns.scatterplot(data=df,x=column,y='DockQ', hue='class')
    #    plt.legend([],[], frameon=False)

    #    plt.plot(x_pmiDockQ, y_pmiDockQ,label='fit',color='k',linewidth=2)
    return popt


def main():
    args = parser.parse_args()
    pdbp = PDBParser(QUIET=True)
    iopdb = PDBIO()

    structure = pdbp.get_structure("", args.pdb[0])
    chains = []
    for chain in structure[0]:
        chains.append(chain.id)

    remain_contact_lst = []
    ## retrieve interface plDDT at chain-level
    plddt_lst = []
    for idx in range(len(chains)):
        chain2_lst = list(set(chains) - set(chains[idx]))
        IF_plddt, contact_lst = retrieve_IFplddt(
            structure, chains[idx], chain2_lst, args.dist
        )
        plddt_lst.append(IF_plddt)
        remain_contact_lst.append(contact_lst)

    ## retrieve interface PAE at chain-level
    with open(args.pkl[0], "rb") as f:
        data = pickle.load(f)

    avgif_pae = retrieve_IFPAEinter(
        structure, data["predicted_aligned_error"], remain_contact_lst, args.dist
    )
    ## calculate pmiDockQ
    res = calc_pmidockq(avgif_pae, plddt_lst)
    pdockq, ppv = get_pdockq(args.pdb[0])

    ## print output
    # print('pDockQ_i is:')
    # for ch in range(len(chains)):
    # print(chains[ch]+' '+str(res['pmidockq'].tolist()[ch]))
    if args.tsv is not None:
        if args.pdbid is None:
            raise ValueError("pdbid cannot be None")

        filename = args.pdb[0].split("/")[-1]
        filename = filename.split("_")
        if os.path.isfile(args.tsv):
            df = pd.read_table(args.tsv)
            df_temp = pd.DataFrame(
                {
                    "ID": [args.pdbid],
                    "Model_type": [filename[0] + "_" + filename[3] + "_" + filename[4]],
                    "Model_number": [filename[2]],
                    "Prediction_number": [filename[-1].split(".")[0]],
                    "pdockq": [pdockq],
                    "ppv": [ppv],
                    "pdockq2": [res["pmidockq"].tolist()],
                    "pae": [avgif_pae],
                    "plddt": [plddt_lst],
                    "ptm": [data["ptm"]],
                    "iptm": [data["iptm"]],
                    "ranking_confidence": [data["ranking_confidence"]],
                    "correct_result": [args.cor_res],
                    "af_PAE": [data["predicted_aligned_error"]],
                }
            )
            df = pd.concat([df, df_temp])
            df.to_csv(path_or_buf=args.tsv, sep="\t", header=True, index=False)
        else:
            # PDB-id Model-type Model-number PdockQv2, other scores
            df = pd.DataFrame()
            df_temp = pd.DataFrame(
                {
                    "ID": [args.pdbid],
                    "Model_type": [filename[0] + "_" + filename[3] + "_" + filename[4]],
                    "Model_number": [filename[2]],
                    "Prediction_number": [filename[-1].split(".")[0]],
                    "pdockq": [pdockq],
                    "ppv": [ppv],
                    "pdockq2": [res["pmidockq"].tolist()],
                    "pae": [avgif_pae],
                    "plddt": [plddt_lst],
                    "ptm": [data["ptm"]],
                    "iptm": [data["iptm"]],
                    "ranking_confidence": [data["ranking_confidence"]],
                    "correct_result": [args.cor_res],
                    "alphafold_PAE": [data["predicted_aligned_error"]],
                }
            )
            df = pd.concat([df, df_temp])
            df.to_csv(path_or_buf=args.tsv, sep="\t", header=True, index=False)


if __name__ == "__main__":
    main()
