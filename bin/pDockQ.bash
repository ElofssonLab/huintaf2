#!/bin/bash -x


dir=`dirname $1`;
name=`basename $1 .pdb`


if [ ! -f $dir/$name.csv ]
then
    ~/git/huintaf2/bin/pDockQ.py -v -p $1 >  $dir/$name.csv
fi
