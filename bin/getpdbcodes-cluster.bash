#!/bin/bash


M=CORUM # HuMap
# First we get all chains in a cluster.
for k in ${m}-clusters/cluster-*txt
do j=`basename $k .txt`   for i in `sed "s/-/ /g" $k`
   do
       echo $i
   done | sort -u > ${m}-clusters/$j.chains
done

# Get all unique chains
cat ${m}-clusters/*chains | sort -u > allchains-${m}.txt


# Get pdb codes
for i in `cat allchains-${m}.txt`
do
    python3 bin/getpdbcode.py $i
done > allchains-${m}-pdb.txt


# Map PDB codes to clusters

for i in ${m}-clusters/*chains
do
    k=`basename $i .chains`
    for j in `cat $i `
    do
	echo -n $i
	grep $j allchains-pdb.txt
	echo ""
    done > ${m}-clusters/$k.pdbIDs
done


for d in ${m}-clusters/cluster-*.pdbIDs
do 
    for i in `sed s/.*://g ${d}  | sed "s/[\',}]//g" | sed "s/\]//g" | sed "s/\[//g"  `
    do
	echo $i
    done | sort -u  > ${d}.txt 
done

for d in ${m}-clusters/cluster-*.pdbIDs.txt
do
    e=`basename $d .txt`
    for i in `cat $d `
    do
	j=`basename $i .pdbIDs.txt`
	echo -n $j": "
	grep -c $i ${m}-clusters/$e
	echo " "
    done  | sort -rnk 2 > ${m}-clusters/$e.count
done


# get CIF files
for i in ${m}-clusters/*count
do
    j=`basename $i .pdbIDs.count `
    pdb=`head -1 $i | sed "s/:.*//g"`
    wget https://files.rcsb.org/download/${pdb}.cif -O ${m}-clusters/$j/${pdb}.cif
done 

# Run MMalign

for i in ${m}-clusters/*/*cif
do
    p=`basename $i`
    j=`dirname $i`
    k=`basename $j`
    if [ ! -f $j/$k-$p.MMalign ]
    then
	~/git/bioinfo-toolbox/trRosetta/MMalign $j/${k}_mintm08.pdb $i > $j/$k-$p.MMalign
    fi
done


for i in ${m}-clusters/*/clu*pdb 	
do
    j=`basename $i .pdb`
    d=`dirname $i `
    tail $i | grep TER > $d/$j.lastchain
done

for i in ${m}-clusters/*/*MMalign
do
    j=`basename $i .MMalign`
    d=`dirname $i`
    b=`basename $d`
    tm1=`grep TM-score= $i |head -1 | gawk '{print $2}'`
    tm2=`grep TM-score= $i |tail -1 | gawk '{print $2}'`
    a=`grep TER $d/${b}_mintm08.lastchain`
    b=`grep -d TER $d/${b}_mintm08.pdb`
    chain=${a:21:1}
    echo $j $chain $tm1 $tm2
done | sort -rnk 3 > ${m}-MMalign.txt

# Find "best" methods 


#Proteosome
