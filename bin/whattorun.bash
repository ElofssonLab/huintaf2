#!/bin/bash

#for i in `ls merged/*fused.a3m | sed "s/merged.//g" | sed "s/_fused.a3m//g"`
#for i in `gawk '{print $1"-"$2}' list.csv` 
#for i in `gawk '{print $1"-"$2}' pedro.csv list.csv` 
for i in `gawk '{print $1"-"$2;print $2"-"$1}' /proj/berzelius-2021-29/users/x_petku/GWIDD_DATA/HUMAN/ppi_human.dat`
do
    if [  -f  pdb/$i/unrelaxed_model_1.pdb ]
    then
	
	echo $i  | sed "s/-/ /g"
    fi
done
