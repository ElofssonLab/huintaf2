#!/bin/bash -x
#SBATCH -A berzelius-2021-64
#SBATCH --output=/proj/berzelius-2021-29/users/x_gabpo/job_out/AF%j.out
#SBATCH --error=/proj/berzelius-2021-29/users/x_gabpo/job_err/AF%j.err
#SBATCH --array=1-217
#SBATCH --gpus=1
#SBATCH -t 12:00:00

##### AF2 CONFIGURATION #####
COMMON="/proj/berzelius-2021-29/users/x_gabpo"
AFHOME="$COMMON/af2-v2.2.0"           		# Path of AF2-multimer-mod directory.
SINGULARITY="$AFHOME/data/af2.sif"		# Path of singularity image.
PARAM="$AFHOME/data/"                           # path of param folder containing AF2 Neural Net parameters.
MAX_RECYCLES=10
MODEL_SET="multimer"

### RUNTIME SPECIFICATIONS ###
LIST=$1
OFFSET=$2
FOLDER=$3

export NVIDIA_VISIBLE_DEVICES="all"
export TF_FORCE_UNIFIED_MEMORY="1"
export XLA_PYTHON_CLIENT_MEM_FRACTION="4.0"




#### ONLY ONE OF THE FOLLOWING SHOULD BE UNCOMMENTED ####################
#MODEL_SET="model_1"							# Uncomment to run single standard model.
#MODEL_SET="model_1_ptm"						# Uncomment to run single ptm model.
MODEL_SET="multimer" 						# Uncomment to run single multimer model.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}"; done		# Uncomment to run all 5 standard models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_ptm"; done	# Uncomment to run all 5 ptm models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_multimer"; done	# Uncomment to run all 5 multimers models.
#########################################################################

### RUNTIME SPECIFICATIONS ###
LIST=$1 	# List with each line containing: fastaID(no format) [true or false] set true if protein is prokariote, false otherwise.
OFFSET=$2 	# ignore first $OFFSET lines of list; to set according to SLURM array size, in order to run all the list.
#FOLDER=$3 	# Path where AF2 picks fasta files and generates its output folder structure.
FOLDER="complexes-v2/"
PRO=false
POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
LINE=`tail -n+$POS $LIST | head -n 1`
id=`echo $LINE | sed "s/\ /-/g" `	# path of input fasta file (without .fasta suffix)
#id2=`echo $LINE | awk '{print $2}'`	# path of input fasta file (without .fasta suffix)
FASTA=${FOLDER}"/${id}.fasta"
#PRO=`echo $LINE | awk '{print $2}'`	# true if folding a prokariotic multimer, false otherwise
PRO=false,false

for PRESET in $MODEL_SET
do
    if [[ ! -f  ${FOLDER}/${id}/ranked_5.pdb ]]
    then
        ##### TO JUST FOLD, GIVEN AN AF DEFAULT FOLDER STRUCTURE WITH MSAS #####
        ##### ALREADY EXIST AT $FOLDER/$FASTA                              #####
        singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
                python3 $AFHOME/run_alphafold.py \
                        --fasta_paths=${FASTA} \
			--run_relax=True \
                        --model_preset=$PRESET \
                        --output_dir=$FOLDER \
                        --data_dir=$PARAM \
			--num_multimer_predictions_per_model=1
                        #--is_prokaryote_list=$PRO \

#	##### TO JUST FOLD, PROVIDING CUSTOM MSAS #####
#	singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
#		python3 $AFHOME/alphafold/run_alphafold.py \
#			--fasta_paths=$FOLDER/${FASTA}.fasta \
#			--model_preset=$PRESET \
#			--output_dir=$FOLDER \
#			--is_prokaryote_list=$PRO \
#			--data_dir=$PARAM \
#			--custom_msas=$MSAS
	# Now we shoudl extract the pTM etc

    fi

done


#pip install --upgrade "jax[cuda]" -f https://storage.googleapis.com/jax-releases/jax_releases.html
#export TF_FORCE_UNIFIED_MEMORY=1
#export XLA_PYTHON_CLIENT_MEM_FRACTION=10.0

