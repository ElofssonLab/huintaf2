#!/bin/bash -x


dir=`dirname $1`;
name=`basename $1 .pkl`
basedir=`pwd`

if [ ! -f $dir/$name.ptm ]
then
    bin/getptm.py -m $1 >  $dir/$name.ptm
fi

