#!/usr/bin/env python3
import pandas as pd
import os
import argparse
from pathlib import Path
# get input file from command line
arg_parser = argparse.ArgumentParser()
arg_parser.add_argument("-i", "--input", required=True, help="path to input file")
arg_parser.add_argument("-o", "--outdir", required=True, help="path to output directory")
arg_parser.add_argument("-s", "--seqdir", required=True, help="path to output directory")
args = arg_parser.parse_args()

input=args.input
outdir=args.outdir
seqdir=args.seqdir

inpdir=os.path.dirname(input)
df=pd.read_json(input)

for i in df.keys():
    #print(i, df[i].sequence,df[i].description)
    for j in df.keys():
        if (j<i):
            continue
        elif(j==i):
            name=df[i].description+"_"+df[j].description
            print(name)
            open(seqdir+"/"+name+".fasta","w").write(">"+df[i].description+"\n"+df[i].sequence+"\n"+">"+df[j].description+"\n"+df[j].sequence)
            output=Path(outdir+"/"+name+"/msas/")
            output.mkdir(exist_ok=True,parents=True)
            os.symlink(inpdir+"/"+i,outdir+"/"+name+"/msas/A",)
        else:
            name=df[i].description+"_"+df[j].description
            print(name)
            open(seqdir+"/"+name+".fasta","w").write(">"+df[i].description+"\n"+df[i].sequence+"\n"+">"+df[j].description+"\n"+df[j].sequence)
            output=Path(outdir+"/"+name+"/msas/")
            output.mkdir(exist_ok=True,parents=True)
            os.symlink(inpdir+"/"+i,outdir+"/"+name+"/msas/A",)
            os.symlink(inpdir+"/"+j,outdir+"/"+name+"/msas/B")            
