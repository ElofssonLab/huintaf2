#!/usr/bin/env python3
import pandas as pd
import sys
import numpy as np
import argparse
import re
import matplotlib.pyplot as plt
import seaborn as sns

from pathlib import Path
home = str(Path.home())

multicut=3


df_CORUM=pd.read_csv(home+"/git/huintaf2/data/CORUM-pdockq.csv")
df_HURI=pd.read_csv(home+"/git/huintaf2/data/HuRI.csv")
df_humap=pd.read_csv(home+"/git/huintaf2/data/humap.csv")
df_humap2=pd.read_csv(home+"/git/huintaf2/data/HuMap2-pdockq.csv")
df_corumpairs=pd.read_csv(home+"/git/huintaf2/data/CORUM-complexes.tsv",sep="\t")
df=pd.read_csv(home+"/git/huintaf2/data/evaluation.csv")

df_contacts=pd.read_csv(home+"/git/huintaf2/data/pdbcomplex-contacts.csv")
df_pdockq=pd.read_csv(home+"/git/huintaf2/data/pdbcomplex-pdockq.csv")

df_contacts["Name"]=df_contacts["Chain1"]+"-"+df_contacts["Chain2"]+"_1"

df_huriMeff=pd.read_csv(home+"/git/huintaf2/data/HuRI-Meff.csv")
df_HURI=pd.merge(df_HURI,df_huriMeff,on=["Name"])

df_humapMeff=pd.read_csv(home+"/git/huintaf2/data/humap-Meff.csv")
df_humap=pd.merge(df_humap,df_humapMeff,on=["Name"])

df_humapMeff=pd.read_csv(home+"/git/huintaf2/data/humap2-Meff.csv")
df_humap2=pd.merge(df_humap2,df_humapMeff,on=["Name"])
df_humap2["Name"]=df_humap2.Name.str.split("_1$").str[0]

df_CORUMMeff=pd.read_csv(home+"/git/huintaf2/data/CORUM-Meff.csv")
df_CORUM=pd.merge(df_CORUM,df_CORUMMeff,on=["Name"])


df_COMPLEXESMeff=pd.read_csv(home+"/git/huintaf2/data/pdbcomplex-Meff.csv")
df_pdockq=pd.merge(df_pdockq,df_COMPLEXESMeff,on=["Name"])

df_merge=pd.merge(df_contacts,df_pdockq,on="Name")
df_merge["Interact"]=np.where(df_merge["Contacts"]>20,"Direct","Indirect")

df_merge.to_csv(home+"/git/huintaf2/data/PDBCOMPLEXdata.csv")

df_humap_uniprot=pd.read_csv(home+"/git/huintaf2/data/HuMap-uniprot.tab",sep="\t")
df_HuRI_uniprot=pd.read_csv(home+"/git/huintaf2/data/HuRI-uniprot.tab",sep="\t")
df_hurimapping=pd.read_csv(home+"/git/huintaf2/data/HuRI-uniprot-mapping.csv",sep=",",names=["ENSGENE","UniProt"])

#df_structure=pd.read_csv(home+"/git/huintaf2/data/petras-pdb-humap.csv")
df_temp=pd.concat([df_humap,df_HURI])
df_structure=df[df.DockQ.notna()]
df_structure=pd.merge(df_structure,df_temp,on="Name")
df_structure["pDockQ"]=df_structure["pDockQ_x"]

df_huristring=pd.read_csv(home+"/git/huintaf2/data/huri-string.tsv",sep="\t")
df_humapstring=pd.read_csv(home+"/git/huintaf2/data/humap-string.tsv",sep="\t")
df_string=pd.read_csv(home+"/git/huintaf2/data/9606.protein.links.full.v11.5.txt",sep=" ")



def getname(id1,id2):
    if (id1>id2):
        Name=id2+"-"+id1
    else:
        Name=id1+"-"+id2
    return (Name)


def getclustersize(Name):
    #print (Name,re.findall(r"-",Name))
    name,model=Name.split("_",1)
    if (len(re.findall(r"-",Name))==1):
        a,b=re.split(r"[-]",name)
    elif (len(re.findall(r"-",Name))==2):
        a,b,c=re.split(r"-",name)
        if (len(b)==1):
            a=a+"-"+b
            b=c
        else:
            b=b+"-"+c  
    elif (len(re.findall(r"-",Name))==3):
        a,b,c,d=re.split(r"[-]",name)
        a=a+"-"+b
        c=c+"-"+d
    else:
        return(0)     
    list=df_corumpairs[df_corumpairs.UniProt==a]["ID"]
    l=df_corumpairs[(df_corumpairs.UniProt==b) & (df_corumpairs.ID.isin(list))]["ID"].to_list()
    #print (Name,a,b,l)
    return (len(df_corumpairs[df_corumpairs.ID==l[0]]))

def getclustername(Name):
    #print (Name,re.findall(r"-",Name))
    name,model=Name.split("_",1)
    if (len(re.findall(r"-",Name))==1):
        a,b=re.split(r"[-]",name)
    elif (len(re.findall(r"-",Name))==2):
        a,b,c=re.split(r"-",name)
        if (len(b)==1):
            a=a+"-"+b
            b=c
        else:
            b=b+"-"+c  
    elif (len(re.findall(r"-",Name))==3):
        a,b,c,d=re.split(r"[-]",name)
        a=a+"-"+b
        c=c+"-"+d
    else:
        return(0)     
    list=df_corumpairs[df_corumpairs.UniProt==a]["ID"]
    l=df_corumpairs[(df_corumpairs.UniProt==b) & (df_corumpairs.ID.isin(list))]["ID"].to_list()
    #print (Name,a,b,l)
    return (l[0])



df_CORUM["ClusterSize"]=df_CORUM["Name"].apply(getclustersize)
df_CORUM["ClusterName"]=df_CORUM["Name"].apply(getclustername)


# String
df_HURI[["temp1","temp2"]]=df_HURI["Name"].str.split("-",n=1,expand=True)
df_HURI=pd.merge(df_HURI,df_hurimapping,left_on="temp1",right_on="ENSGENE")
df_HURI=pd.merge(df_HURI,df_hurimapping,left_on="temp2",right_on="ENSGENE")
df_HURI["id1"]=df_HURI["UniProt_x"]
df_HURI["id2"]=df_HURI["UniProt_y"]
df_HURI["Name"] = df_HURI.apply(lambda x: getname(x["id1"],x["id2"]),axis=1)

df_HURI=pd.merge(df_HURI,df_HuRI_uniprot,left_on="id1",right_on="Entry")
df_HURI=pd.merge(df_HURI,df_HuRI_uniprot,left_on="id2",right_on="Entry")



#print (df_string)


df_temp1=pd.merge(df_string,df_humapstring,left_on="protein1",right_on="To")
df_temp1["id1"]=df_temp1["From"]
df_temp1=pd.merge(df_temp1,df_humapstring,left_on="protein2",right_on="To")
df_temp1["id2"]=df_temp1["From_y"]


df_temp2=pd.merge(df_string,df_huristring,left_on="protein1",right_on="To")
df_temp2["id1"]=df_temp2["From"]
df_temp2=pd.merge(df_temp2,df_huristring,left_on="protein2",right_on="To")
df_temp2["id2"]=df_temp2["From_y"]

df_string=pd.concat([df_temp1,df_temp2])
df_string["Name"] = df_string.apply(lambda x: getname(x["id1"],x["id2"]),axis=1)

#print (df_string)



#df_CORUM.to_csv(home+"/git/huintaf2/data/CORUMdata.csv")

f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize<=3], x="pDockQ",common_norm=False,label="CORUM-trimers")
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>3], x="pDockQ",common_norm=False,label="CORUM-complex")
sns.kdeplot(data=df_CORUM, x="pDockQ",common_norm=False,label="CORUM")
sns.kdeplot(data=df_HURI, x="pDockQ",common_norm=False,label="HuRI")
sns.kdeplot(data=df_humap, x="pDockQ",common_norm=False,label="HuMap")
#sns.kdeplot(data=df_humap2, x="pDockQ",common_norm=False,label="HuMap2")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/CORUM.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/CORUM.svg",dpi=600)


cutoff=0.5
CORUM2=len(df_CORUM[(df_CORUM.ClusterSize<=2)&(df_CORUM.pDockQ>cutoff)])/len(df_CORUM[df_CORUM.ClusterSize<=2])
CORUM3=len(df_CORUM[(df_CORUM.ClusterSize<=3)&(df_CORUM.pDockQ>cutoff)])/len(df_CORUM[df_CORUM.ClusterSize<=3])
CORUMall=len(df_CORUM[(df_CORUM.ClusterSize>2)&(df_CORUM.pDockQ>cutoff)])/len(df_CORUM[df_CORUM.ClusterSize>2])
CORUM6=len(df_CORUM[(df_CORUM.ClusterSize>5)&(df_CORUM.pDockQ>cutoff)])/len(df_CORUM[df_CORUM.ClusterSize>5])
CORUM=len(df_CORUM[(df_CORUM.pDockQ>cutoff)])/len(df_CORUM)
HURI=len(df_HURI[(df_HURI.pDockQ>cutoff)])/len(df_HURI)
HUMAP=len(df_humap[(df_humap.pDockQ>cutoff)])/len(df_humap)
HUMAP2=len(df_humap2[(df_humap2.pDockQ>cutoff)])/len(df_humap2)
PDBdirect=len(df_merge[(df_merge.Interact=="Direct")&(df_merge.pDockQ>cutoff)])/len(df_merge[df_merge.Interact=="Direct"])
PDBindirect=len(df_merge[(df_merge.Interact=="Indirect")&(df_merge.pDockQ>cutoff)])/len(df_merge[df_merge.Interact=="Indirect"])
PDBindirect=len(df_merge[(df_merge.Interact=="Indirect")&(df_merge.pDockQ>cutoff)])/len(df_merge[df_merge.Interact=="Indirect"])


PDB=len(df_structure[(df_structure.pDockQ>cutoff)])/len(df_structure)

print ("CORUM2",CORUM2)
print ("CORUM3",CORUM3)
print ("CORUM6",CORUM6)
print ("CURUM",CORUM)
print ("CURUMall",CORUMall)
print ("HuRI",HURI)
print ("HuMap",HUMAP)
#print ("HuMap2",HUMAP2)
print ("PDB-direct",PDBdirect)
print ("PDB-indirect",PDBindirect)
print ("Structure",PDB)

for i in range(2,df_CORUM.ClusterSize.max()):
    print(i,round(df_CORUM[df_CORUM.ClusterSize==i]["pDockQ"].mean(),2)
           ,round(len(df_CORUM[(df_CORUM.ClusterSize==i)&(df_CORUM.pDockQ>0.5)])
          /(len(df_CORUM[df_CORUM.ClusterSize==i])+0.000001),2)
          ,len(df_CORUM[(df_CORUM.ClusterSize==i)&(df_CORUM.pDockQ>0.5)])
          ,len(df_CORUM[df_CORUM.ClusterSize==i]))
    

my_pal = {"CORUM-dimers": "orange", "CORUM-multimers": "darkorange", "CORUM": "orange",
          "HuRI": "lightgreen" ,"HuMap":"grey" ,"HuMap2":"black" ,"COMPLEXES-direct":"royalblue" ,
          "Direct":"royalblue" ,"Indirect":"mediumblue" ,
          "COMPLEXES-indirect":"mediumblue" ,"COMPLEXES":"blue" ,"PDB":"red",
          "Different": "red",
          "Organell": "blue",
          "SubOrganell": "royalblue",
          "HuRI: Different": "lightgreen",
          "HuRI: Organell": "darkgreen",
          "HuRI: SubOrganell": "green",
          "HuMap: Different": "lightgrey",
          "HuMap: Organell": "darkgrey",
          "HuMap: SubOrganell": "grey",
          "HuRI: 0": "palegreen",
          "HuRI: <250": "lightgreen",
          "HuRI: <500": "limegreen",
          "HuRI: <750": "green",
          "HuRI: >750": "darkgreen",
          "HuMap: 0": "gainsboro",
          "HuMap: <250": "lightgrey",
          "HuMap: <500": "silver",
          "HuMap: <750": "grey",
          "HuMap: >750": "darkgrey",
          "HuRI: NoCoExp": "lightgreen",
          "HuMap: NoCoExp": "lightgrey",
          "HuRI: CoExp": "green",
          "HuMap: CoExp": "grey",
          "PDB: Different": "pink",
          "PDB: Organell": "lightsalmon",
          "PDB: SubOrganell": "red",
          "PDB: 0": "pink",
          "PDB: <250": "lightsalmon",
          "PDB: <500": "tomato",
          "PDB: <750": "red",
          "PDB: >750": "firebrick",
          "PDB: NoCoExp": "pink",
          "PDB: CoExp": "red"
}
          

f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
#sns.kdeplot(data=df_CORUM, x="pDockQ",common_norm=False,label="CORUM: "+str(round(CORUM,3)),color="orange", linestyle="solid", linewidth=3)
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize<=2], x="pDockQ",common_norm=False,label="CORUM-dimers: "+str(round(CORUM2,3)),color="orange", linestyle="solid", linewidth=3)
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize<=3], x="pDockQ",common_norm=False,label="CORUM-trimers: "+str(round(CORUM3,3)),color="orange", linewidth=3)
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>2], x="pDockQ",common_norm=False,label="CORUM: "+str(round(CORUMall,3)),color="orange", linewidth=3,linestyle="dotted")
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>5], x="pDockQ",common_norm=False,label="CORUM-hexa+: "+str(round(CORUM6,3)),color="orange", linewidth=5,linestyle=":")
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>3], x="pDockQ",common_norm=False,label="CORUM-tetra+: "+str(round(CORUM,3)),color="orange",linestyle="--", linewidth=3)
g=sns.kdeplot(data=df_HURI, x="pDockQ",common_norm=False,label="HuRI: "+str(round(HURI,2)),linewidth=1,linestyle="--",color="lightgreen")
g=sns.kdeplot(data=df_humap, x="pDockQ",common_norm=False,label="HuMap: "+str(round(HUMAP,2)),linewidth=1,color="grey")
#g=sns.kdeplot(data=df_humap2, x="pDockQ",common_norm=False,label="HuMap2: "+str(round(HUMAP2,2)),linewidth=3,color="black")
sns.kdeplot(data=df_merge[df_merge.Interact=="Direct"], x="pDockQ",common_norm=False,label="Direct: "+str(round(PDBdirect,2)),color="blue",linewidth=3)
sns.kdeplot(data=df_merge[df_merge.Interact=="Indirect"], x="pDockQ",common_norm=False,label="Indirect: "+str(round(PDBindirect,2)),color="blue",linestyle="--", linewidth=3)
#sns.kdeplot(data=df_structure,x="pDockQ",label="PDB:"+str(round(PDB,3)), linewidth=1,linestyle="-",common_norm=False,color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/PdockQ-datasets.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/PdockQ-datasets.svg",dpi=600)


f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.kdeplot(data=df_CORUM, x="pDockQ",common_norm=False,label="CORUM: "+str(round(CORUM,2)),color="orange", linestyle="solid", linewidth=3)
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize<=2], x="pDockQ",common_norm=False,label="CORUM-dimers: "+str(round(CORUM2,3)),color="orange", linestyle="solid", linewidth=3)
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize<=3], x="pDockQ",common_norm=False,label="CORUM-trimers: "+str(round(CORUM3,3)),color="orange", linewidth=3)
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>2], x="pDockQ",common_norm=False,label="CORUM: "+str(round(CORUMall,3)),color="orange", linewidth=3,linestyle="dotted")
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>5], x="pDockQ",common_norm=False,label="CORUM-hexa+: "+str(round(CORUM6,3)),color="orange", linewidth=5,linestyle=":")
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>3], x="pDockQ",common_norm=False,label="CORUM-tetra+: "+str(round(CORUM,3)),color="orange",linestyle="--", linewidth=3)
g=sns.kdeplot(data=df_HURI, x="pDockQ",common_norm=False,label="HuRI: "+str(round(HURI,2)),linewidth=1,linestyle="--",color="lightgreen")
g=sns.kdeplot(data=df_humap, x="pDockQ",common_norm=False,label="HuMap: "+str(round(HUMAP,2)),linewidth=1,color="grey")
#g=sns.kdeplot(data=df_humap2, x="pDockQ",common_norm=False,label="HuMap2: "+str(round(HUMAP2,3)),linewidth=3,color="black")
sns.kdeplot(data=df_merge[df_merge.Interact=="Direct"], x="pDockQ",common_norm=False,label="Direct: "+str(round(PDBdirect,2)),color="blue",linewidth=3)
sns.kdeplot(data=df_merge[df_merge.Interact=="Indirect"], x="pDockQ",common_norm=False,label="Indirect: "+str(round(PDBindirect,2)),color="blue",linestyle="--", linewidth=3)
sns.kdeplot(data=df_structure,x="pDockQ",label="PDB:"+str(round(PDB,2)), linewidth=1,linestyle="-",common_norm=False,color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/PdockQ-datasets-ALL.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/PdockQ-datasets-ALL.svg",dpi=600)


f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.kdeplot(data=df_CORUM, x="pDockQ",common_norm=False,label="CORUM: "+str(round(CORUM,2)),color="orange", linestyle="solid", linewidth=3)
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize<=2], x="pDockQ",common_norm=False,label="CORUM-dimers: "+str(round(CORUM2,3)),color="orange", linestyle="solid", linewidth=3)
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize<=3], x="pDockQ",common_norm=False,label="CORUM-trimers: "+str(round(CORUM3,3)),color="orange", linewidth=3)
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>2], x="pDockQ",common_norm=False,label="CORUM: "+str(round(CORUMall,3)),color="orange", linewidth=3,linestyle="dotted")
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>5], x="pDockQ",common_norm=False,label="CORUM-hexa+: "+str(round(CORUM6,3)),color="orange", linewidth=5,linestyle=":")
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>3], x="pDockQ",common_norm=False,label="CORUM-tetra+: "+str(round(CORUM,3)),color="orange",linestyle="--", linewidth=3)
g=sns.kdeplot(data=df_HURI, x="pDockQ",common_norm=False,label="HuRI: "+str(round(HURI,3)),linewidth=3,linestyle="--",color="lightgreen")
sns.kdeplot(data=df_humap, x="pDockQ",common_norm=False,label="HuMap: "+str(round(HUMAP,3)),linewidth=3,color="grey")
#g=sns.kdeplot(data=df_humap2, x="pDockQ",common_norm=False,label="HuMap2: "+str(round(HUMAP2,3)),linewidth=3,color="black")
#sns.kdeplot(data=df_merge[df_merge.Interact=="Direct"], x="pDockQ",common_norm=False,label="Direct: "+str(round(PDBdirect,2)),color="blue",linewidth=3)
#sns.kdeplot(data=df_merge[df_merge.Interact=="Indirect"], x="pDockQ",common_norm=False,label="Indirect: "+str(round(PDBindirect,2)),color="blue",linestyle="--", linewidth=3)
#sns.kdeplot(data=df_structure,x="pDockQ",label="PDB:"+str(round(PDB,3)), linewidth=3,linestyle="-",common_norm=False,color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/PdockQ-datasets2.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/PdockQ-datasets2.svg",dpi=600)


f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
#sns.kdeplot(data=df_CORUM, x="pDockQ",common_norm=False,label="CORUM: "+str(round(CORUM,3)),color="orange", linestyle="solid", linewidth=3)
sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize<=2], x="pDockQ",common_norm=False,label="CORUM-dimers: "+str(round(CORUM2,2)),color="orange", linestyle="solid", linewidth=3)
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize<=3], x="pDockQ",common_norm=False,label="CORUM-trimers: "+str(round(CORUM3,3)),color="orange", linewidth=3)
sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>2], x="pDockQ",common_norm=False,label="CORUM: "+str(round(CORUMall,2)),color="orange", linewidth=3,linestyle="dotted")
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>5], x="pDockQ",common_norm=False,label="CORUM-hexa+: "+str(round(CORUM6,3)),color="orange", linewidth=5,linestyle=":")
#sns.kdeplot(data=df_CORUM[df_CORUM.ClusterSize>3], x="pDockQ",common_norm=False,label="CORUM-tetra+: "+str(round(CORUM,3)),color="orange",linestyle="--", linewidth=3)
#g=sns.kdeplot(data=df_HURI, x="pDockQ",common_norm=False,label="HuRI: "+str(round(HURI,3)),linewidth=3,linestyle="--",color="lightgreen")
#g=sns.kdeplot(data=df_humap, x="pDockQ",common_norm=False,label="HuMap: "+str(round(HUMAP,3)),linewidth=3,color="grey")
#g=sns.kdeplot(data=df_humap2, x="pDockQ",common_norm=False,label="HuMap2: "+str(round(HUMAP2,3)),linewidth=3,color="black")
sns.kdeplot(data=df_merge[df_merge.Interact=="Direct"], x="pDockQ",common_norm=False,label="Direct: "+str(round(PDBdirect,2)),color="blue",linewidth=3)
sns.kdeplot(data=df_merge[df_merge.Interact=="Indirect"], x="pDockQ",common_norm=False,label="Indirect: "+str(round(PDBindirect,2)),color="blue",linestyle="--", linewidth=3)
#sns.kdeplot(data=df_structure,x="pDockQ",label="PDB:"+str(round(PDB,3)), linewidth=3,linestyle="-",common_norm=False,color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/PdockQ-datasets3.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/PdockQ-datasets3.svg",dpi=600)



df_HURI["Length"]=(df_HURI["len1"]+df_HURI["len2"])
df_humap["Length"]=(df_humap["len1"]+df_humap["len2"])
df_humap2["Length"]=(df_humap2["len1"]+df_humap2["len2"])
df_structure["Length"]=(df_structure["len1"]+df_structure["len2"])
df_CORUM["Length"]=(df_CORUM["len1"]+df_CORUM["len2"])
df_merge["Length"]=(df_merge["len1"]+df_merge["len2"])

df_HURI["FracDiso"]=(df_HURI["NumDiso1-50"]+df_HURI["NumDiso2-50"])/(df_HURI["len1"]+df_HURI["len2"])
df_humap["FracDiso"]=(df_humap["NumDiso1-50"]+df_humap["NumDiso2-50"])/(df_humap["len1"]+df_humap["len2"])
df_humap2["FracDiso"]=(df_humap2["NumDiso1-50"]+df_humap2["NumDiso2-50"])/(df_humap2["len1"]+df_humap2["len2"])
df_structure["FracDiso"]=(df_structure["NumDiso1-50"]+df_structure["NumDiso2-50"])/(df_structure["len1"]+df_structure["len2"])
df_CORUM["FracDiso"]=(df_CORUM["NumDiso1-50"]+df_CORUM["NumDiso2-50"])/(df_CORUM["len1"]+df_CORUM["len2"])
df_merge["FracDiso"]=(df_merge["NumDiso1-50"]+df_merge["NumDiso2-50"])/(df_merge["len1"]+df_merge["len2"])





df_all=pd.DataFrame({'Name' : [] ,"pDockQ":[] ,"FracDiso":[] ,"Length":[] , "Dataset":[] })
df_all2=pd.DataFrame({'Name' : [] ,"pDockQ":[] ,"FracDiso":[] ,"Length":[] , "Dataset":[] })

df_temp=df_CORUM[["Name","pDockQ","FracDiso","Length"]]
df_temp["Dataset"]="CORUM"
df_all2=pd.concat([df_all2,df_temp])
#df_temp=df_CORUM[df_CORUM.ClusterSize<=2][["Name","pDockQ","FracDiso","Length"]]
#df_temp["Dataset"]="CORUM-dimers"
#df_all=pd.concat([df_all,df_temp])
#df_temp=df_CORUM[df_CORUM.ClusterSize>2][["Name","pDockQ","FracDiso","Length"]]
#df_temp["Dataset"]="CORUM-multimers"
#df_all=pd.concat([df_all,df_temp])
df_temp=df_HURI[["Name","pDockQ","FracDiso","Length"]]
df_temp["Dataset"]="HuRI"
df_all=pd.concat([df_all,df_temp])
df_all2=pd.concat([df_all2,df_temp])
df_temp=df_humap[["Name","pDockQ","FracDiso","Length"]]
df_temp["Dataset"]="HuMap"
df_all=pd.concat([df_all,df_temp])
df_all2=pd.concat([df_all2,df_temp])
#df_temp=df_humap2[["Name","pDockQ","FracDiso","Length"]]
#df_temp["Dataset"]="HuMap2"
#df_all=pd.concat([df_all,df_temp])
df_temp=df_merge[df_merge.Interact=="Direct"][["Name","pDockQ","FracDiso","Length"]]
df_temp["Dataset"]="Direct"
df_all2=pd.concat([df_all2,df_temp])
df_temp=df_merge[df_merge.Interact=="Indirect"][["Name","pDockQ","FracDiso","Length"]]
df_temp["Dataset"]="Indirect"
df_all2=pd.concat([df_all2,df_temp])
df_temp=df_structure[["Name","pDockQ","FracDiso","Length"]]
df_temp["Dataset"]="PDB"
df_all=pd.concat([df_all,df_temp])
df_all2=pd.concat([df_all2,df_temp])
#df_temp=df_structure[["Name","pDockQ","FracDiso","Length"]]
#df_temp["Dataset"]="PDB"
#df_all=pd.concat([df_all,df_temp])







sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 60) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_all2,y="Dataset",x="pDockQ",scale="width",cut=0,
               showmeans=True, showextrema=True, showmedians=True,palette=my_pal)
#ax.annotate(str(round(CORUM2,2)),(.75, 0))
ax.annotate(str(round(CORUM,2)),(.75, 0))
ax.annotate(str(round(HURI,2)),(.75, 1))
ax.annotate(str(round(HUMAP,2)),(.75, 2))
ax.annotate(str(round(PDBdirect,2)),(.75, 3))
ax.annotate(str(round(PDBindirect,2)),(.75, 4))
ax.annotate(str(round(PDB,2)),(.75, 5))
#ax.annotate(str(round(HUMAP2,2)),(.75, 7))
plt.savefig(home+"/git/huintaf2/plots/pDockQ-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/pDockQ-violin.svg",dpi=600)



f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
#sns.kdeplot(data=df_CORUM, x="FracDiso",common_norm=False,label="CORUM",color="orange", linestyle="solid", linewidth=3)
g=sns.kdeplot(data=df_HURI, x="FracDiso",common_norm=False,label="HuRI",linewidth=3,linestyle="-",color="lightgreen")
g=sns.kdeplot(data=df_humap, x="FracDiso",common_norm=False,label="HuMap",linewidth=3,color="grey")
#sns.kdeplot(data=df_merge[df_merge.Interact=="Direct"], x="FracDiso",common_norm=False,label="Direct",color="blue",linewidth=3)
#sns.kdeplot(data=df_merge[df_merge.Interact=="Indirect"], x="FracDiso",common_norm=False,label="Indirect",color="blue",linestyle="--", linewidth=3)
sns.kdeplot(data=df_structure,x="FracDiso",label="PDB", linewidth=3,linestyle="-",common_norm=False,color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/FracDiso-datasets.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/FracDiso-datasets.svg",dpi=600)



sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 60) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_all,y="Dataset",x="FracDiso",scale="width",cut=0,
               showmeans=True, showextrema=True, showmedians=False,palette=my_pal)
#ax.annotate(str(round(CORUM2,2)),(.75, 5))
plt.savefig(home+"/git/huintaf2/plots/FracDiso-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/FracDiso-violin.svg",dpi=600)


f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
#sns.kdeplot(data=df_CORUM, x="Length",common_norm=False,label="CORUM",color="orange", linestyle="solid", linewidth=3)
g=sns.kdeplot(data=df_HURI, x="Length",common_norm=False,label="HuRI",linewidth=3,linestyle="-",color="lightgreen")
g=sns.kdeplot(data=df_humap, x="Length",common_norm=False,label="HuMap",linewidth=3,color="grey")
#sns.kdeplot(data=df_merge[df_merge.Interact=="Direct"], x="Length",common_norm=False,label="Direct",color="blue",linewidth=3)
#sns.kdeplot(data=df_merge[df_merge.Interact=="Indirect"], x="Length",common_norm=False,label="Indirect",color="blue",linestyle="--", linewidth=3)
sns.kdeplot(data=df_structure,x="Length",label="PDB", linewidth=3,linestyle="-",common_norm=False,color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/Length-datasets.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Length-datasets.svg",dpi=600)




sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 60) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_all,y="Dataset",x="Length",scale="width",cut=0,
               showmeans=True, showextrema=True, showmedians=False,palette=my_pal)
#ax.annotate(str(round(CORUM2,2)),(.75, 5))
plt.savefig(home+"/git/huintaf2/plots/Length-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Length-violin.svg",dpi=600)


 
# plot it


sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
sns.lineplot(df_HURI.sort_values(by=["FracDiso"]).FracDiso,df_HURI.sort_values(by=["FracDiso"])["pDockQ"].rolling(200,min_periods=200, center=True).mean(),color="lightgreen",label="HuRI")
sns.lineplot(df_humap.sort_values(by=["FracDiso"]).FracDiso,df_humap.sort_values(by=["FracDiso"])["pDockQ"].rolling(200, min_periods=200, center=True).mean(),color="grey",label="HuMap")
sns.lineplot(df_humap2.sort_values(by=["FracDiso"]).FracDiso,df_humap2.sort_values(by=["FracDiso"])["pDockQ"].rolling(200, min_periods=200, center=True).mean(),color="black",label="HuMap2")
sns.lineplot(df_structure.sort_values(by=["FracDiso"]).FracDiso,df_structure.sort_values(by=["FracDiso"])["pDockQ"].rolling(200, min_periods=200, center=True).mean(),color="red",label="Structure")
sns.lineplot(df_CORUM.sort_values(by=["FracDiso"]).FracDiso,df_CORUM.sort_values(by=["FracDiso"])["pDockQ"].rolling(200, min_periods=200, center=True).mean(),color="orange",label="CORUM")
#sns.lineplot(df_CORUM[df_CORUM.ClusterSize<=2].sort_values(by=["FracDiso"]).FracDiso,df_CORUM[df_CORUM.ClusterSize<=2].sort_values(by=["FracDiso"])["pDockQ"].rolling(200, min_periods=200, center=True).mean(),color="orange",label="CORUM-dimers")
#sns.lineplot(df_CORUM[df_CORUM.ClusterSize>2].sort_values(by=["FracDiso"]).FracDiso,df_CORUM[df_CORUM.ClusterSize>2].sort_values(by=["FracDiso"])["pDockQ"].rolling(200, min_periods=200, center=True).mean(),linestyle="--",color="darkorange",label="CORUM-multimers")
#sns.lineplot(df_merge.sort_values(by=["FracDiso"]).FracDiso,df_merge.sort_values(by=["FracDiso"])["pDockQ"].rolling(200, min_periods=200, center=True).mean(),color="blue",label="COMPLEXES")
sns.lineplot(df_merge[df_merge.Interact=="Direct"].sort_values(by=["FracDiso"]).FracDiso,df_merge[df_merge.Interact=="Direct"].sort_values(by=["FracDiso"])["pDockQ"].rolling(200, min_periods=200, center=True).mean(),color="royalblue",label="Direct")
sns.lineplot(df_merge[df_merge.Interact=="Indirect"].sort_values(by=["FracDiso"]).FracDiso,df_merge[df_merge.Interact=="Indirect"].sort_values(by=["FracDiso"])["pDockQ"].rolling(200, min_periods=200, center=True).mean(),color="mediumblue",linestyle="--",label="Indirect")
plt.legend()
plt.savefig(home+"/git/huintaf2/plots/FracDiso-rolling.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/FracDiso-rolling.svg",dpi=600)


sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))


plt.close()
df_Meff=pd.DataFrame({'Name' : [] ,"pDockQ":[] ,"FracDiso":[] ,"Meff":[], "Length":[] ,"Log(Meff)":[], "Dataset":[] })

#df_temp=df_CORUM[df_CORUM.ClusterSize<=2][["Name","pDockQ","FracDiso","Meff","Length"]]
#df_temp["Dataset"]="CORUM-dimers"
#df_temp["Log(Meff)"]=np.log10(df_temp.Meff)
#df_Meff=pd.concat([df_Meff,df_temp])
#df_temp=df_CORUM[df_CORUM.ClusterSize>2][["Name","pDockQ","FracDiso","Meff","Length"]]
#df_temp["Dataset"]="CORUM-multimers"
#df_temp["Log(Meff)"]=np.log10(df_temp.Meff)
#df_Meff=pd.concat([df_Meff,df_temp])
#df_temp=df_CORUM[["Name","pDockQ","FracDiso","Meff","Length"]]
#df_temp["Dataset"]="CORUM"
#df_temp["Log(Meff)"]=np.log10(df_temp.Meff)
#df_Meff=pd.concat([df_Meff,df_temp])
df_temp=df_HURI[["Name","pDockQ","FracDiso","Meff","Length"]]
df_temp["Dataset"]="HuRI"
df_temp["Log(Meff)"]=np.log10(df_temp.Meff)
df_Meff=pd.concat([df_Meff,df_temp])
df_temp=df_humap[["Name","pDockQ","FracDiso","Meff","Length"]]
df_temp["Log(Meff)"]=np.log10(df_temp.Meff)
df_temp["Dataset"]="HuMap"
df_Meff=pd.concat([df_Meff,df_temp])
#df_temp=df_humap2[["Name","pDockQ","FracDiso","Meff","Length"]]
#df_temp["Dataset"]="HuMap2"
#df_temp["Log(Meff)"]=np.log10(df_temp.Meff)
#df_Meff=pd.concat([df_Meff,df_temp])
#df_temp=df_merge[df_merge.Interact=="Direct"][["Name","pDockQ","FracDiso","Meff","Length"]]
#df_temp["Log(Meff)"]=np.log10(df_temp.Meff)
#df_temp["Dataset"]="Direct"
#df_Meff=pd.concat([df_Meff,df_temp])
#df_temp=df_merge[df_merge.Interact=="Indirect"][["Name","pDockQ","FracDiso","Meff","Length"]]
#df_temp["Log(Meff)"]=np.log10(df_temp.Meff)
#df_temp["Dataset"]="Indirect"
#df_Meff=pd.concat([df_Meff,df_temp])
df_temp=df_structure[["Name","pDockQ","FracDiso","Meff","Length"]]
df_temp["Log(Meff)"]=np.log10(df_temp.Meff)
df_temp["Dataset"]="PDB"
df_Meff=pd.concat([df_Meff,df_temp])





#print (df_Meff)
sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
#df_HURI["Disorder"]=np.where(df_HURI["FracDiso"]>0.10,"Disorder","Order")
sns.jointplot(data=df_Meff[df_Meff.Dataset!="PDB"],x="FracDiso",y="pDockQ",hue="Dataset",kind="kde",palette=my_pal,common_norm=False)
plt.savefig(home+"/git/huintaf2/plots/FracDiso-pDockQ.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/FracDiso-pDockQ.svg",dpi=600)


sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
#df_HURI["Disorder"]=np.where(df_HURI["FracDiso"]>0.10,"Disorder","Order")
sns.jointplot(data=df_Meff[df_Meff.Dataset!="PDB"],x="Meff",y="pDockQ",hue="Dataset",kind="kde",palette=my_pal,common_norm=False)
plt.savefig(home+"/git/huintaf2/plots/Meff-pDockQ.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Meff-pDockQ.svg",dpi=600)

sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
#df_HURI["Disorder"]=np.where(df_HURI["FracDiso"]>0.10,"Disorder","Order")
sns.jointplot(data=df_Meff[df_Meff.Dataset!="PDB"],x="Log(Meff)",y="pDockQ",hue="Dataset",kind="kde",palette=my_pal,common_norm=False)
plt.savefig(home+"/git/huintaf2/plots/Log-Meff-pDockQ.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Log-Meff-pDockQ.svg",dpi=600)


sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 60) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_Meff[df_Meff.Dataset!="PDB"],y="Dataset",x="Meff",scale="width",cut=0,
               showmeans=True, showextrema=True, showmedians=False,palette=my_pal)
#ax.annotate(str(round(CORUM2,2)),(.75, 5))
plt.savefig(home+"/git/huintaf2/plots/Meff-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Meff-violin.svg",dpi=600)

sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 60) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_Meff[df_Meff.Dataset!="PDB"],y="Dataset",x="Log(Meff)",scale="width",cut=0,
               showmeans=True, showextrema=True, showmedians=False,palette=my_pal)
#ax.annotate(str(round(CORUM2,2)),(.75, 5))
plt.savefig(home+"/git/huintaf2/plots/Meff-log-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Meff-log-violin.svg",dpi=600)


f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
#sns.kdeplot(data=df_CORUM, x="Meff",common_norm=False,label="CORUM",color="orange", linestyle="solid", linewidth=3)
g=sns.kdeplot(data=df_HURI, x="Meff",common_norm=False,label="HuRI",linewidth=3,linestyle="-",color="lightgreen")
g=sns.kdeplot(data=df_humap, x="Meff",common_norm=False,label="HuMap",linewidth=3,color="grey")
#sns.kdeplot(data=df_merge[df_merge.Interact=="Direct"], x="Meff",common_norm=False,label="Direct",color="blue",linewidth=3)
#sns.kdeplot(data=df_merge[df_merge.Interact=="Indirect"], x="Meff",common_norm=False,label="Indirect",color="blue",linestyle="--", linewidth=3)
sns.kdeplot(data=df_structure,x="Meff",label="PDB", linewidth=3,linestyle="-",common_norm=False,color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/Meff-datasets.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Meff-datasets.svg",dpi=600)




f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
#sns.kdeplot(data=df_CORUM, x="Log(Meff)",common_norm=False,label="CORUM",color="orange", linestyle="solid", linewidth=3)
g=sns.kdeplot(data=df_Meff[df_Meff.Dataset=="HuRI"], x="Log(Meff)",common_norm=False,label="HuRI",linewidth=3,linestyle="-",color="lightgreen")
g=sns.kdeplot(data=df_Meff[df_Meff.Dataset=="HuMap"], x="Log(Meff)",common_norm=False,label="HuMap",linewidth=3,color="grey")
#sns.kdeplot(data=df_merge[df_merge.Interact=="Direct"], x="Log(Meff)",common_norm=False,label="Direct",color="blue",linewidth=3)
#sns.kdeplot(data=df_merge[df_merge.Interact=="Indirect"], x="Log(Meff)",common_norm=False,label="Indirect",color="blue",linestyle="--", linewidth=3)
sns.kdeplot(data=df_Meff[df_Meff.Dataset=="PDB"],x="Log(Meff)",label="PDB", linewidth=3,linestyle="-",common_norm=False,color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/Meff-log-datasets.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Meff-log-datasets.svg",dpi=600)





# Now also do this for string

stringorder=["HuMap: 0","HuMap: <250","HuMap: <500","HuMap: <750","HuMap: >750",
             "HuRI: 0","HuRI: <250","HuRI: <500","HuRI: <750","HuRI: >750",
             "PDB: 0","PDB: <250","PDB: <500","PDB: <750","PDB: >750"]

stringorder2=["HuMap: NoCoExp","HuMap: CoExp","HuRI: NoCoExp",
              "HuRI: CoExp","PDB: NoCoExp","PDB: CoExp"]
             


df_string["STRING"]=np.where(df_string["combined_score"]>0,
                                   np.where(df_string["combined_score"]>250,
                                   np.where(df_string["combined_score"]>500,
                                   np.where(df_string["combined_score"]>750,
                                   ">750","<750"),
                                   "<500"),"<250"),
                                   "0")

df_string["coExpression"]=np.where(df_string["coexpression"]>0,
                                   np.where(df_string["coexpression"]>250,
                                   np.where(df_string["coexpression"]>500,
                                   np.where(df_string["coexpression"]>750,
                                   ">750","<750"),
                                   "<500"),"<250"),
                                   "0")

df_string["coExpression2"]=np.where(df_string["coexpression"]>0,"CoExp","NoCoExp")


df_temp1=pd.merge(df_humap,df_string,on="Name")[["Name","pDockQ",
         'neighborhood', 'neighborhood_transferred',
       'fusion', 'cooccurence', 'homology', 'coexpression',
       'coexpression_transferred', 'experiments', 'experiments_transferred',
       'database', 'database_transferred', "combined_score",
                                                 "coExpression","STRING","coExpression2"]]
df_temp1["Dataset"]="HuMap"
df_temp2=pd.merge(df_HURI,df_string,on="Name")[["Name","pDockQ",
        'neighborhood', 'neighborhood_transferred',
       'fusion', 'cooccurence', 'homology', 'coexpression',
       'coexpression_transferred', 'experiments', 'experiments_transferred',
       'database', 'database_transferred', "combined_score",
                        "coExpression","STRING","coExpression2"]]
df_temp2["Dataset"]="HuRI"


df_temp=pd.concat([df_temp1,df_temp2])

df_temp2=pd.merge(df_structure,df_string,on="Name")[["Name","pDockQ",
        'neighborhood', 'neighborhood_transferred',
       'fusion', 'cooccurence', 'homology', 'coexpression',
       'coexpression_transferred', 'experiments', 'experiments_transferred',
       'database', 'database_transferred', "combined_score",
                        "coExpression","STRING","coExpression2"]]
df_temp2["Dataset"]="PDB"

df_temp=pd.concat([df_temp,df_temp2])


#print (df_temp,df_temp1,df_temp2)


df_temp["String-score"]=df_temp.Dataset+": "+df_temp.STRING
df_temp["CoExpression"]=df_temp.Dataset+": "+df_temp.coExpression
df_temp["CoExpression2"]=df_temp.Dataset+": "+df_temp.coExpression2


sns.set(font_scale=1.5,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 60) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_temp,x="pDockQ",y="String-score",kind="kde",cut=0,
               showmeans=True, showextrema=True, showmedians=False,palette=my_pal ) # ,order=stringorder
plt.savefig(home+"/git/huintaf2/plots/STRING-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/STRING-violin.svg",dpi=600) #,scale="width"


sns.set(font_scale=1.4,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 60) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_temp,x="pDockQ",y="CoExpression",kind="kde",cut=0,
               showmeans=True, showextrema=True, showmedians=False,palette=my_pal,order=stringorder)
plt.savefig(home+"/git/huintaf2/plots/coExpress-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/coExpress-violin.svg",dpi=600) #,scale="width"

sns.set(font_scale=1.5,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 60) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_temp,x="pDockQ",y="CoExpression2",kind="kde",cut=0,
               showmeans=True, showextrema=True, showmedians=False,palette=my_pal,order=stringorder2)
plt.savefig(home+"/git/huintaf2/plots/coExpress2-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/coExpress2-violin.svg",dpi=600) #,scale="width"



f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
g=sns.kdeplot(data=df_temp[df_temp["CoExpression2"]=="HuRI: NoCoExp"], x="pDockQ",common_norm=False,label="HuRI: No coexpression",linewidth=3,linestyle="--",color="lightgreen")
g=sns.kdeplot(data=df_temp[df_temp["CoExpression2"]=="HuRI: CoExp"], x="pDockQ",common_norm=False,label="HuRI: coexpression",linewidth=3,linestyle="-",color="green")
g=sns.kdeplot(data=df_temp[df_temp["CoExpression2"]=="HuMap: NoCoExp"], x="pDockQ",common_norm=False,label="HuMap: No coexpression",linewidth=3,linestyle="--",color="lightgrey")
g=sns.kdeplot(data=df_temp[df_temp["CoExpression2"]=="HuMap: CoExp"], x="pDockQ",common_norm=False,label="HuMap: coexpression",linewidth=3,linestyle="-",color="grey")
g=sns.kdeplot(data=df_temp[df_temp["CoExpression2"]=="PDB: NoCoExp"], x="pDockQ",common_norm=False,label="PDB: No coexpression",linewidth=3,linestyle="--",color="pink")
sns.kdeplot(data=df_temp[df_temp["CoExpression2"]=="PDB: CoExp"], x="pDockQ",common_norm=False,label="PDB: coexpression",linewidth=3,linestyle="-",color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/CoExpress-datasets.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/CoExpress-datasets.svg",dpi=600)



sns.set(font_scale=1.,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
#df_HURI["Disorder"]=np.where(df_HURI["FracDiso"]>0.10,"Disorder","Order")
sns.jointplot(data=df_temp[df_temp.Dataset!="PDB"],x="cooccurence",y="pDockQ",hue="Dataset",kind="kde",palette=my_pal,common_norm=False)
plt.savefig(home+"/git/huintaf2/plots/cooccurence-pDockQ.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/cooccurence-pDockQ.svg",dpi=600)

sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
#df_HURI["Disorder"]=np.where(df_HURI["FracDiso"]>0.10,"Disorder","Order")
sns.jointplot(data=df_temp[df_temp.Dataset!="PDB"],x="coexpression",y="pDockQ",hue="Dataset",kind="kde",palette=my_pal,common_norm=False)
plt.savefig(home+"/git/huintaf2/plots/coexpression-pDockQ.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/coexpression-pDockQ.svg",dpi=600)

sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
#df_HURI["Disorder"]=np.where(df_HURI["FracDiso"]>0.10,"Disorder","Order")
sns.jointplot(data=df_temp[df_temp.Dataset!="PDB"],x="coexpression_transferred",y="pDockQ",hue="Dataset",kind="kde",palette=my_pal,common_norm=False)
plt.savefig(home+"/git/huintaf2/plots/coexpression2-pDockQ.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/coexpression2-pDockQ.svg",dpi=600)

sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
#df_HURI["Disorder"]=np.where(df_HURI["FracDiso"]>0.10,"Disorder","Order")
sns.jointplot(data=df_temp[df_temp.Dataset!="PDB"],x="combined_score",y="pDockQ",hue="Dataset",kind="kde",palette=my_pal,common_norm=False)
plt.savefig(home+"/git/huintaf2/plots/string-pDockQ.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/string-pDockQ.svg",dpi=600)



# Extr
keys=['neighborhood', 'neighborhood_transferred',
       'fusion', 'cooccurence', 'homology', 'coexpression',
       'coexpression_transferred', 'experiments', 'experiments_transferred',
       'database', 'database_transferred']
    

#keys=['cooccurence',   'coexpression', 'experiments', 'database']
    
x=0
fig, ax = plt.subplots(int(len(keys)), 1)
for i in keys:
    sns.violinplot(data=df_temp[df_temp.Dataset!="PDB"],x=i,y="Dataset",kind="kde",cut=0,
               showmeans=True, showextrema=True, showmedians=False,ax=ax[x]) #,palette=my_pal) # ,order=stringorder)
    x+=1
plt.tight_layout()
plt.savefig(home+"/git/huintaf2/plots/string-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/string-violin.svg",dpi=600)


for i in keys:
    sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
    f, ax = plt.subplots(figsize=(12., 12.))
    #df_HURI["Disorder"]=np.where(df_HURI["FracDiso"]>0.10,"Disorder","Order")
    sns.jointplot(data=df_temp[df_temp.Dataset!="PDB"],x=i,
                  y="pDockQ",hue="Dataset",kind="kde",palette=my_pal,common_norm=False)
    plt.savefig(home+"/git/huintaf2/plots/string_"+i+"-pDockQ.png",dpi=600)
    plt.savefig(home+"/git/huintaf2/plots/string_"+i+"-pDockQ.svg",dpi=600)

