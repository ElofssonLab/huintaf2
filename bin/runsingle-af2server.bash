#!/bin/bash -x
#SBATCH -A Berzelius-2022-106
#SBATCH --output=out/run_manual%j.out
#SBATCH --error=err/run_manual%j.err
#SBATCH --array=1-1
#SBATCH -N 1
#SBATCH --gpus=1
#SBATCH --export=ALL,CUDA_VISIBLE_DEVICES
#SBATCH -t 24:00:00

# #SBATCH --exclusive

xport NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'



##### AF2 CONFIGURATION #####
COMMON="/proj/berzelius-2021-29/"
DIR="/proj/berzelius-2021-29/users/x_arnel/CASP15/"
AFHOME="/proj/berzelius-2021-29/users/x_arnel/git/alphafold/"
#AFHOME=$COMMON"/AF2-multimer-mod/" 			# Path of AF2-multimer-mod directory.
#SINGULARITY=$COMMON"/AF2-multimer-mod//AF_data/alphafold-multimer.sif" 	# Path of singularity image.
SINGULARITY=$COMMON"/singularity_images/alphafold.sif" 	# Path of singularity image.
PARAM=$COMMON"/AF2-multimer-mod/AF_data/" 				# path of param folder containing AF2 Neural Net parameters.
MAX_RECYCLES=10

#### ONLY ONE OF THE FOLLOWING SHOULD BE UNCOMMENTED ####################
#MODEL_SET="model_1"							# Uncomment to run single standard model.
#MODEL_SET="model_1_ptm"						# Uncomment to run single ptm model.
#MODEL_SET="model_1_multimer" 						# Uncomment to run single multimer model.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}"; done		# Uncomment to run all 5 standard models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_ptm"; done	# Uncomment to run all 5 ptm models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_multimer"; done	# Uncomment to run all 5 multimers models.
#########################################################################

### RUNTIME SPECIFICATIONS ###
LIST=$1 	# List with each line containing: fastaID(no format) [true or false] set true if protein is prokariote, false otherwise.
OFFSET=$2 	# ignore first $OFFSET lines of list; to set according to SLURM array size, in order to run all the list.
#FOLDER=$3 	# Path where AF2 picks fasta files and generates its output folder structure.
FOLDER="single/"
POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
LINE=`tail -n+$POS $LIST | head -n 1`
id=`echo $LINE | sed "s/\ /-/g" `	# path of input fasta file (without .fasta suffix)


# Using claudio run_manual script

$DIR/bin/run_manual.sh seq/$id.fasta $FOLDER monomer 
