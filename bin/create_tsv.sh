COMMON="/proj/berzelius-2021-29/users/x_sarna"
scripts=$COMMON/scripts
input=/proj/berzelius-2021-29/users/x_sarna/msas/2.3/neg_homomers/ids_neg_homomers.txt
model_dir=/proj/berzelius-2021-29/users/x_sarna/msas/2.3/neg_homomers
tsvs=/proj/berzelius-2021-29/users/x_sarna/msas/tsvs
#pdb_id=6l8v_5y2z # for testing one pdb-value, don't forget to comment out the for-loops if using this

while IFS= read -r pdb_id
do

	for file in $model_dir/$pdb_id/*unrelaxed*pred*.pdb; do
		filename="$(sed 's/.*unrelaxed\_//g' <<< $file)" 
		filename="$(sed 's/\.pdb//g' <<< $filename)"
		python3 $scripts/pdockq2.py -pkl $model_dir/$pdb_id/*$filename*.pkl -pdb $file -tsv $tsvs/homomers_alphafold_2_3.tsv -pdbid $pdb_id -cor_res 0
		echo done with $filename
	done
	echo done with $pdb_id
done < "$input"
echo done with negative homomers

input=/proj/berzelius-2021-29/users/x_sarna/msas/2.3/homomers/ids_homomers.txt
model_dir=/proj/berzelius-2021-29/users/x_sarna/msas/2.3/homomers

while IFS= read -r pdb_id
do

        for file in $model_dir/$pdb_id/*unrelaxed*pred*.pdb; do
                filename="$(sed 's/.*unrelaxed\_//g' <<< $file)"
                filename="$(sed 's/\.pdb//g' <<< $filename)"
                python3 $scripts/pdockq2.py -pkl $model_dir/$pdb_id/*$filename*.pkl -pdb $file -tsv $tsvs/homomers_alphafold_2_3.tsv -pdbid $pdb_id -cor_res 1
        	echo done with $filename

        done
	echo done with $pdb_id
done < "$input"
echo done with homomers

