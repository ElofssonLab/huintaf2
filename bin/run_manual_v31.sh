#!/bin/bash

module load Anaconda/2021.05-nsc1
conda activate /proj/beyondfold/apps/.conda/envs/af_server2 #af_server
#conda activate /proj/wallner-b/users/x_bjowa/.conda/envs/alphafold2
#export PATH=/proj/beyondfold/apps/kalign-3.3.2/bin:/proj/beyondfold/apps/.conda/envs/af_server/bin/:$PATH
export PATH=/proj/beyondfold/apps/kalign-3.3.2/bin:$PATH
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'

# path to fasta file
fasta=$1
target=$(basename $fasta .fasta)
shift
# path to output folder
outdir=$1
shift
# either "monomer" or "multimer"
type=$1
shift
# --norun_relax if want to skip relax step
#norelax=$4

alphafold=/proj/beyondfold/apps/alphafoldv2.3.1/run_alphafold.py
python=$(which python3.9)
echo $python

if [[ $type == "multimer" ]]; then
	flagfile=/proj/beyondfold/apps/alphafold/flagfiles/multimer_full_dbs_v3.flag
elif [[ $type == "multimer_1" ]]; then
#--num_multimer_predictions_per_model=1
        flagfile=/proj/beyondfold/apps/alphafold/flagfiles/multimer_full_dbs_1.flag
elif [[ $type == "multimer_single" ]]; then
#--num_multimer_predictions_per_model=1
        flagfile=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/huintaf2/bin/multimer_full_dbs_single.flag
elif [[ $type == "multimer_single_notemp" ]]; then
#--num_multimer_predictions_per_model=1
        flagfile=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/huintaf2/bin/multimer_full_dbs_single_notemp.flag
elif [[ $type == "multimer_notemp" ]]; then
	flagfile=/proj/beyondfold/apps/alphafold/flagfiles/multimer_full_dbs_notemp.flag
elif [[ $type == "monomer_notemp" ]]; then
	flagfile=/proj/beyondfold/apps/alphafold/flagfiles/monomer_full_dbs_notemp.flag
else
        flagfile=/proj/beyondfold/apps/alphafold/flagfiles/monomer_full_dbs.flag
fi
echo $python $alphafold --flagfile $flagfile --output_dir $outdir --fasta_paths $fasta $norelax $@
$python $alphafold --flagfile $flagfile --output_dir $outdir --fasta_paths $fasta $norelax $@ #&> ${target}.log


