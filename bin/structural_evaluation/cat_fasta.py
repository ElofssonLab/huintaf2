import argparse
import sys
import os
import numpy as np
import pandas as pd

import pdb

parser = argparse.ArgumentParser(description = '''Cat fasta files.''')
#Bench4
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with information of interactions.')
parser.add_argument('--fastadir', nargs=1, type= str, default=sys.stdin, help = 'Path to directory with fasta files.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Path to outname.')


################FUNCTIONS################
def read_fasta(filename):

    file_contents = []
    with open(filename, 'r') as file:
        for line in file:
            file_contents.append(line)

    return file_contents


#################MAIN####################

#Parse args
args = parser.parse_args()
meta = pd.read_csv(args.meta[0])
fastadir = args.fastadir[0]
outname = args.outname[0]


#Write all infividual fasta for running HHblits
with open(outname, 'w') as file:
    for i in range(len(meta)):
        row = meta.loc[i]
        file_contents = read_fasta(fastadir+row.Id+'.fasta')
        for item in file_contents:
            file.write(item)
        file.write('\n')
