
###########################hu.MAP############################
#Run CD-HIT
CDHIT=/home/pbryant/cdhit/cd-hit
INFILE=/home/pbryant/data/huMAP/ppv90_fasta_complex/all.fasta
OUTFILE=/home/pbryant/data/huMAP/ppv90_fasta_complex/all
#$CDHIT -i $INFILE -o $OUTFILE -c 0.4 -n 2 -d 0

#Parse the results
CLUSTERED_SEQS=/home/pbryant/data/huMAP/ppv90_fasta_complex/all.clstr
META=../../data/evaluation_ppv90.csv
OUTDIR=../../data/
python3 ./parse_clusters.py --cluster_file $CLUSTERED_SEQS --meta $META --outdir $OUTDIR
