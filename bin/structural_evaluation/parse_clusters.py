import argparse
import sys
import os
import numpy as np
import pandas as pd

import pdb

parser = argparse.ArgumentParser(description = '''Parse clusters.''')
#Bench4
parser.add_argument('--cluster_file', nargs=1, type= str, default=sys.stdin, help = 'Path to CD-hit file with clusters.')
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with information of interactions.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to outdir.')


################FUNCTIONS################
def read_clusters(filename):

    clusters = []
    ids = []
    with open(filename, 'r') as file:
        for line in file:
            line = line.rstrip()
            if '>Cluster' in line:
                clstr = int(line.split()[1])
            else:
                ids.append(line.split('>')[1].split('...')[0])
                clusters.append(clstr)

    cluster_df = pd.DataFrame()
    cluster_df['Id'] = ids
    cluster_df['Cluster'] = clusters

    return cluster_df


#################MAIN####################

#Parse args
args = parser.parse_args()
cluster_file = args.cluster_file[0]
meta = pd.read_csv(args.meta[0])
outdir = args.outdir[0]
#Parse clusters
cluster_df = read_clusters(cluster_file)

#Merge
merged = pd.merge(meta, cluster_df,left_on='Name',right_on='Id')
#Save
merged.to_csv(args.meta[0],index=None)
print('Parsed clusters')

#Get cluster representatives
clstr_repr = merged.loc[merged.Cluster.drop_duplicates().index]
clstr_repr.to_csv(outdir+'cluster_repr.csv', index=None)
