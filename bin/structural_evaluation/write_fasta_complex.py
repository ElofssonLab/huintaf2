import argparse
import sys
import os
import numpy as np
import pandas as pd

import pdb

parser = argparse.ArgumentParser(description = '''Cat fasta files.''')
#Bench4
parser.add_argument('--meta', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with information of interactions.')
parser.add_argument('--seqs', nargs=1, type= str, default=sys.stdin, help = 'Path to csv with sequences per chain.')
parser.add_argument('--outdir', nargs=1, type= str, default=sys.stdin, help = 'Path to outname.')


################FUNCTIONS################


#################MAIN####################

#Parse args
args = parser.parse_args()
meta = pd.read_csv(args.meta[0])
seqs = pd.read_csv(args.seqs[0])
outdir = args.outdir[0]

#Merge
merged = pd.merge(meta,seqs,left_on='id1',right_on='Id')
merged = pd.merge(merged,seqs,left_on='id2',right_on='Id')


#Write individual fasta files
for i in range(len(merged)):
    row  = merged.loc[i]
    with open(outdir+row.Name+'.fasta','w') as file:
        file.write('>'+row.Name+'\n')
        file.write(row.Sequence_x+row.Sequence_y)

#Write all together
with open(outdir+'all.fasta','w') as file:
    for i in range(len(merged)):
        row  = merged.loc[i]
        file.write('>'+row.Name+'\n')
        file.write(row.Sequence_x+row.Sequence_y+'\n')
