import argparse
import sys
import os
import numpy as np
import pandas as pd
import glob

import pdb

parser = argparse.ArgumentParser(description = '''Cat fasta files.''')
#Bench4
parser.add_argument('--fastadir', nargs=1, type= str, default=sys.stdin, help = 'Path to directory with fasta files.')
parser.add_argument('--outname', nargs=1, type= str, default=sys.stdin, help = 'Path to outname (csv file).')


################FUNCTIONS################
def read_fasta(filename):

    seq = ''
    with open(filename, 'r') as file:
        for line in file:
            line = line.rstrip()
            if line[0]=='>':
                continue
            else:
                seq+=line

    return seq


#################MAIN####################

#Parse args
args = parser.parse_args()

fastadir = args.fastadir[0]
outname = args.outname[0]

#Get all fasta files
ids = []
seqs = []
lens = []
fasta_files = glob.glob(fastadir+'*.fasta')

for i in range(len(fasta_files)):
    seq = read_fasta(fasta_files[i])
    ids.append(fasta_files[i].split('/')[-1].split('.')[0])
    seqs.append(seq)
    lens.append(len(seq))
#Create df
seq_df = pd.DataFrame()
seq_df['Id']=ids
seq_df['Sequence']=seqs
seq_df['Length']=lens
seq_df.to_csv(args.outname[0],index=None)
