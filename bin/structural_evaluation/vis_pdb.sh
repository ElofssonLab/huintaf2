#!/bin/bash -x


#hu.MAP


#INDIR=/home/pbryant/results/huMAP/ppv90/pdb #Path to pdb files
INDIR=/scratch3/arnee/HuMap2/pdb/ #Path to pdb files
OUTDIR=../../plots/cluster_representatives/huMAP #Path to outfolder
IDS=ppv90_humap_repr.txt
for i in {1..402}
do
  ID=$(sed -n $i'p' $IDS)
  pymol -cq $INDIR/$ID/$ID'.pdb' -d "hide all;bg white;show cartoon;
  set_color col4=[0.2,0.627451,0.172549];
  set_color col8=[1,0.4980392,0];
  color col4, chain A;
  color col8, chain B;
  orient organic; png $OUTDIR/$ID,  width=15cm, height=15cm, dpi=600, ray=1"
done

#set_color col4=[0.2,0.627451,0.172549]  ) and chain b (set_color col8=[1,0.4980392,0]

# #HuRI
# INDIR=../pdb_files/ppv90 #Path to pdb files
# OUTDIR=../plots/pdb #Path to outfolder
# IDS=ppv90_repr.txt
# for i in {1..117}
# do
#   ID=$(sed -n $i'p' $IDS)
#   pymol -cq $INDIR/$ID'.pdb' -d "hide all;bg white;show cartoon;
#   color skyblue, chain A;
#   orient organic; png $OUTDIR/$ID,  width=5cm, height=5cm, dpi=600, ray=1"
# done
