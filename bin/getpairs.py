#!/usr/bin/env python3

import pandas as pd

df=pd.read_csv("data/CORUM-complexes.tsv",names=["ID","proteins"],header=None)
k=0
complexes=df.ID.to_list()
for id in df.proteins:
    #print ("Complex: ",complexes[k])
    foo=id.split(' ')
    for j in foo:
        if j=="":continue
        for i in foo:
            if i=="":continue
            if (i>j):
                print (str(complexes[k])+","+str(i)+","+str(j))
    k+=1
