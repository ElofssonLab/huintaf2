#!/bin/bash -x

for i in HuMap-clusters/cluster-*/
do
    j=`basename $i`
    if [ ! -f $i/cluster.pdb ]
    then
	bin/buildcomplex.py -i $i/*-*.pdb -o $i/cluster.pdb > $i/cluster.log
    fi
done

for i in CORUM-clusters/cluster-*/
do
    if [ ! -f $i/cluster.pdb ]
    then
	j=`basename $i`
	bin/buildcomplex.py -i $i/*-*.pdb -o $i/cluster.pdb > $i/cluster.log
    fi
done
