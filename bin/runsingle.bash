#!/bin/bash -x
#SBATCH -A Berzelius-2022-106
#SBATCH --output=out/AFmono%j.out
#SBATCH --error=err/AFmono%j.err
#SBATCH --array=1-1
#SBATCH -N 1
#SBATCH --gpus=8
#SBATCH --export=ALL,CUDA_VISIBLE_DEVICES
#SBATCH -t 72:00:00
#SBATCH --exclusive

xport NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'


# Trying to automatize AF2 runs

#dir=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/
dir=./
fddir=/proj/berzelius-2021-29/users/x_arnel/git/FoldDock/
seq1=seq/$1.fasta
#seq2=seq/$2.fasta

name1=`basename $seq1 .fasta`
#name2=`basename $seq2 .fasta`

A3M1=a3m/${name1}.a3m
#A3M2=a3m/${name2}.a3m
MGF=0.9

# if both does not exist exit


#CB=`${dir}/bin/seqlen.bash seq/${name1}.fasta`
FASTAFILE=$seq1

MSAS="$A3M1" #Comma separated list of msa paths


AFHOME=${fddir}/src/alphafold/ # Path of alphafold directory in FoldDock 
SINGULARITY=/proj/berzelius-2021-29/singularity_images/alphafold.sif
PARAM=/proj/berzelius-2021-29/Database/af_params/  #Path to AF2 params \
OUTFOLDER=single/ # Path where AF2 generates its output folder structure

PRESET='full_dbs' #Choose preset model configuration - no ensembling (full_dbs) and (reduced_dbs) or 8 model ensemblings (casp14). \
MAX_RECYCLES=25 #max_recycles (default=3) \
for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_ptm"; done	# Uncomment to run all 5 ptm models.
#MODEL_NAME='model_1'
COMMON=/proj/

NAME=${name1}
#pushd $AFHOME
#cd $AFHOME
#NAME="./" # to not run this part

for MODEL_NAME in $MODEL_SET
do
    if [ ! -d $OUTFOLDER/$NAME/relaxed_${MODEL_NAME}_pred_0.pdb ]
    then
	mkdir -p $OUTFOLDER/$NAME/
	singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
		    python3 $AFHOME/run_alphafold.py \
                    --fasta_paths=$FASTAFILE \
                    --msas=$MSAS \
                    --output_dir=$OUTFOLDER \
                    --model_names=$MODEL_NAME \
                    --data_dir=$PARAM \
                    --fold_only \
                    --uniref90_database_path='' \
                    --mgnify_database_path='' \
                    --bfd_database_path='' \
                    --uniclust30_database_path='' \
                    --pdb70_database_path='' \
                    --template_mmcif_dir='' \
                    --obsolete_pdbs_path='' \
                    --preset=$PRESET \
                    --max_recycles=$MAX_RECYCLES
	#popd                 --chain_break_list= \
		    
    fi
done
