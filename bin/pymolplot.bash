#!/bin/bash -x
#PYMOL=/snap/bin/pymol-oss.pymol
PYMOL=pymol

name=`basename $1 .pdb`
dir=`dirname $1`
out=${dir}/${name}
d=${dir}/${name}.DockQ
p=${dir}/${name}.pLDDT
dockq=`gawk '{print $2}' $d`
plddt=`grep IF_pLDDT $p | gawk '{printf("%.1f",$2)}'`

echo $dockq
echo $plddt

$PYMOL -cq  -d   "load $1, AF2;
  load $2, PDBMODEL;
  hide all;
  bg white;
  show cartoon;
  set_color col4=[0.2,0.627451,0.172549];
  set_color col8=[1,0.4980392,0];
  color col4, model AF2 and chain A;
  color col8, model AF2 and chain B;
  color grey80, model PDBMODEL and chain A;
  color grey50, model PDBMODEL and chain B;
  extra_fit;
  orient organic; 
  ray 2048,2048;
  png $out"



convert  $out.png -pointsize 60 -gravity NorthWest -draw "text 10,60 DockQ:${dockq} " -draw "text 10,120 plDDT:${plddt}" ${out}_temp.png
mv ${out}_temp.png ${out}.png
