#!/bin/bash

dir=$1
input=$2

while I= read i j
do
	sed s/\>.*/\>$i/g seq/$i.fasta > ${dir}/$i-$j.fasta
	sed s/\>.*/\>$j/g seq/$j.fasta >> ${dir}/$i-$j.fasta
done < ${input}


for i in ${dir}/*.fasta
do
    python3 bin/build_folder_tree.py --fasta $i --sourcepath MSAs/ --targetpath ${dir}/
done
    
