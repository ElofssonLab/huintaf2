#!/bin/bash -x

dir=`dirname $1`;
name=`basename $dir`
model=`echo $1 | sed "s/.*model_//g" | sed "s/.*ranked_//g" | sed "s/_.*//g" | sed "s/.pdb$//g"`

if [ ! -f ${dir}/${name}_${model}.pdb ]
then
    ~/git/huintaf2/bin/splitchains.py -p $1 >  ${dir}/${name}_${model}.pdb
fi
