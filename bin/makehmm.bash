#!/bin/bash
cd pdb_seqres/
rm ../pdb_seqres_*
ffindex_build -s ../pdb_seqres_msa.ff{data,index} .
cd ..
ffindex_apply pdb_seqres_msa.ff{data,index}   -i pdb_seqres_a3m.ffindex -d pdb_seqres_a3m.ffdata -- hhconsensus -M 50 -maxres 65535 -i stdin -oa3m stdout -v 0
ffindex_apply pdb_seqres_a3m.ff{data,index}   -i pdb_seqres_hhm.ffindex -d pdb_seqres_hhm.ffdata -- hhmake -i stdin -o stdout -v 0
cstranslate -f -x 0.3 -c 4 -I a3m -i pdb_seqres_a3m -o pdb_seqres_cs219 
sort -k3 -n -r pdb_seqres_cs219.ffindex | cut -f1 > sorting.dat
ffindex_order sorting.dat pdb_seqres_hhm.ff{data,index} pdb_seqres_hhm_ordered.ff{data,index}
mv pdb_seqres_hhm_ordered.ffdata pdb_seqres_hhm.ffdata
mv pdb_seqres_hhm_ordered.ffindex pdb_seqres_hhm.ffindex
ffindex_order sorting.dat pdb_seqres_a3m.ff{data,index} pdb_seqres_a3m_ordered.ff{data,index}
mv pdb_seqres_a3m_ordered.ffindex pdb_seqres_a3m.ffindex
mv pdb_seqres_a3m_ordered.ffdata pdb_seqres_a3m.ffdata
