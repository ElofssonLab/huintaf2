#!/usr/bin/env python3
import pandas as pd
import os
import argparse
from pathlib import Path
# get input file from command line
arg_parser = argparse.ArgumentParser()
arg_parser.add_argument("-i", "--input", required=True, help="path to input file")
arg_parser.add_argument("-o", "--outdir", required=True, help="path to output directory")
arg_parser.add_argument("-s", "--seqdir", required=True, help="path to output directory")
arg_parser.add_argument('-n','--names', nargs='+', help='<Required> Set flag', required=True)
args = arg_parser.parse_args()

input=args.input
outdir=args.outdir
seqdir=args.seqdir

inpdir=os.path.dirname(input)
df=pd.read_json(input)

alphabet=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
NAME=''
KEYS=[]
SEQ=''
for i in df.keys():
    if (args.names.count(df[i].description)):
        NAME=NAME+df[i].description+"-"
        KEYS+=i
        SEQ+=">df"+df[i].description+"\n"+df[i].sequence+"\n"

print (SEQ,NAME,KEYS)
NAME=NAME[:-1]
open(seqdir+"/"+NAME+".fasta","w").write(SEQ)
output=Path(outdir+"/"+NAME+"/msas/")
output.mkdir(exist_ok=True,parents=True)
j=0
for i in KEYS:
    os.symlink(inpdir+"/"+i,outdir+"/"+NAME+"/msas/"+alphabet[j],)
    j+=1
print (NAME)
