#!/bin/bash -x
#SBATCH -A berzelius-2021-64
#SBATCH --output=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/out/runall-%j.out
#SBATCH --error=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/err/runall-%j.err
#SBATCH --array=1-1
#SBATCH -N 1
#SBATCH -t 48:00:00
#SBATCH --gpus=1


#export NVIDIA_VISIBLE_DEVICES='all'
#export TF_FORCE_UNIFIED_MEMORY='1'
#export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'

## #SBATCH --exclusive



#list=$1
#offset=$2

#pos=$(($SLURM_ARRAY_TASK_ID + $offset))
#id=`tail -n+$pos $list | head -n 1 | gawk '{print $0}'`
#id2=`tail -n+$pos $list | head -n 1 | gawk '{print $2}'`


#OUTFOLDER=pdb-multi/
#NAME=`echo ${id} | sed "s/\ /-/g"`
#pushd $AFHOME
#cd $AFHOME
#NAME="./" # to not run this part

cat $1 | parallel -j 32 /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/makemerge.bash {}


   
