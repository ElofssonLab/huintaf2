#!/bin/bash -x


export NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'
export CUDA_VISIBLE_DEVICES='all'
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64
#export TF_CPP_MIN_LOG_LEVEL=0


# Trying to automatize AF2 runs

#dir=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/
dir=./
fddir=/proj/berzelius-2021-29/users/x_arnel/git/FoldDock/
seq1=seq/$1.fasta
seq2=seq/$2.fasta

name1=`basename $seq1 .fasta`
name2=`basename $seq2 .fasta`

A3M1=a3m/${name1}.a3m
A3M2=a3m/${name2}.a3m
MGF=0.9

# if both does not exist exit

FUSEDMSA=merged/${name1}-${name2}_fused.a3m
PAIREDMSA=merged/${name1}-${name2}_paired.a3m

if [ -f $A3M1 ] && [ -f $A3M2  ] 
then
    if [ ! -f $PAIREDMSA ]
    then
	python3 ${dir}/bin/oxmatch.py --a3m1 $A3M1 --a3m2 $A3M2 --max_gap_fraction $MGF --outname $PAIREDMSA
    fi
    if [ ! -f $FUSEDMSA ]
    then
	python3 ${dir}/bin/fuse_msas.py --a3m1 $A3M1 --a3m2 $A3M2 --max_gap_fraction $MGF --outname $FUSEDMSA
    fi
else
    exit 0
fi

CB=`${dir}/bin/seqlen.bash seq/${name1}.fasta`
FASTAFILE=merged/${name1}-${name2}.fasta
if [ ! -f $FASTAFILE  ]
then
    ${dir}/bin/merge_fasta.bash seq/${name1}.fasta seq/${name2}.fasta > $FASTAFILE
fi

MSAS="$PAIREDMSA,$FUSEDMSA" #Comma separated list of msa paths


AFHOME=${fddir}/src/alphafold/ # Path of alphafold directory in FoldDock 
SINGULARITY=/proj/berzelius-2021-29/singularity_images/alphafold.sif
PARAM=/proj/berzelius-2021-29/Database/af_params/  #Path to AF2 params \
OUTFOLDER=pdb-multi/ # Path where AF2 generates its output folder structure

PRESET='full_dbs' #Choose preset model configuration - no ensembling (full_dbs) and (reduced_dbs) or 8 model ensemblings (casp14). \
MAX_RECYCLES=10 #max_recycles (default=3) \
MODEL_NAME='model_1'
COMMON=/proj/

NAME=${name1}-${name2}
#pushd $AFHOME
#cd $AFHOME
#NAME="./" # to not run this part

if [ ! -f $OUTFOLDER/$NAME/unrelaxed_model_1.pdb ]
then
    singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
        python3 $AFHOME/run_alphafold.py \
                --fasta_paths=$FASTAFILE \
                --msas=$MSAS \
                --chain_break_list=$CB \
                --output_dir=$OUTFOLDER \
                --model_names=$MODEL_NAME \
                --data_dir=$PARAM \
                --fold_only \
                --uniref90_database_path='' \
                --mgnify_database_path='' \
                --bfd_database_path='' \
                --uniclust30_database_path='' \
                --pdb70_database_path='' \
                --template_mmcif_dir='' \
                --obsolete_pdbs_path='' \
                --preset=$PRESET \
                --max_recycles=$MAX_RECYCLES
#popd
fi
