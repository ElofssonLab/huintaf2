#!/bin/bash -x
#SBATCH -A SNIC2021-5-297
#SBATCH --output=/proj/snic2019-35-62/users/x_gabpo/job_out/AF%j.out
#SBATCH --error=/proj/snic2019-35-62/users/x_gabpo/job_err/AF%j.err
#SBATCH --array=1-500
#SBATCH --gpus-per-task=1
#SBATCH -t 24:00:00

##### AF2 CONFIGURATION #####
COMMON='/proj/snic2019-35-62/' # Path prefix shared with all adopted paths
AFHOME=$COMMON'/AF2-multimer-mod/' # Path of AF2-multimer-mod directory
SINGULARITY=$AFHOME'/AF_data/alphafold-multimer.sif' # Path of singularity image
PARAM=$AFHOME'/AF_data/' # path of param folder containing AF2 Neural Net parameters.

### ADVANCED CONFIGURATION ###
MAX_RECYCLES= #max_recycles

### RUNTIME SPECIFICATIONS ###
LIST=$1
OFFSET=$2
POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
LINE=`tail -n+$POS $LIST | head -n 1`
FASTA=`echo $LINE | awk '{print $1}'`  # Path of input fasta files
FOLDER=$3 # Path where AF2 picks fasta files and generates its output folder structure
PRO=`echo $LINE | awk '{print $2}'` # true if folding a prokariotic multimer, false otherwise
echo $FASTA $PRO

for n in {1..5}; do
  singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
  	  python3 $AFHOME/alphafold/run_alphafold.py \
		  --fasta_paths=$FOLDER/${FASTA}.fasta \
                  --data_dir=$PARAM \
		  --output_dir=$FOLDER \
		  --model_preset=model_${n}_multimer
done
