#!/usr/bin/env python3

import pandas as pd
import pickle
import sys
import numpy as np
import argparse
import matplotlib.pyplot as plt
import re
from Bio.PDB import PDBIO
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.internal_coords import *

arg_parser = argparse.ArgumentParser(description="plot PAE from pickle file")
arg_parser.add_argument("-v","--verbose", action='store_true', required=False,help="Verbose output")
arg_parser.add_argument("-p","--pdb",type=argparse.FileType('r'),help="Input PDB file (to filter away non-contacts)")
arg_parser.add_argument("-i","--pickle",type=argparse.FileType('r'),help="Input PKL file")
arg_parser.add_argument("-o","--output",type=argparse.FileType('w'),help="ouput image")
arg_parser.add_argument("-d","--dist", type=float,required=False,default=8.0,help="Cutoff for defining distances")
args = arg_parser.parse_args()

print (args.pickle.name)
pkl=pd.read_pickle(args.pickle.name)

maxdist=args.dist


def structtonumpy(structure):
    chains={}
    chain_ids=[]
    STRUCTURE=[]
    for model in structure:
        for chain in model:
            ch=chain.get_id()
            chain_ids+=[ch]
            chains[ch] = []
            for residue in chain: # We try with CA here
                try:
                    x,y,z=residue['CA'].get_vector()
                    chains[ch].append([x,y,z])
                    STRUCTURE.append([x,y,z])
                except:
                    try:
                        x,y,z=residue['CB'].get_vector()
                        chains[ch].append([x,y,z])
                        STRUCTURE.append([x,y,z])
                    except:
                        try:
                            x,y,z=residue['N'].get_vector()
                            chains[ch].append([x,y,z])
                            STRUCTURE.append([x,y,z])
                        except:
                            i=0
    return (STRUCTURE,chains,chain_ids)
                
if (args.pdb):
    pdbp = PDBParser(QUIET=True)
    iopdb = PDBIO()
    structure = pdbp.get_structure('', args.pdb.name)

    STRUCTURE,chains,ids=structtonumpy(structure)
    coords = np.array(STRUCTURE)
    #Check total length
    #Calc 2-norm
    
    mat = np.append(coords, coords,axis=0)
    print (coords,mat)
    a_min_b = mat[:,np.newaxis,:] -mat[np.newaxis,:,:]

    ##--- EJ KLART
    dists_norm = np.sqrt(np.sum(a_min_b.T ** 2, axis=0)).T
    #l1 = len(coords)
    #contact_dists = dists[:l1,l1:]
    contacts = np.argwhere(dists<=maxdist)
    #M
    #print (contacts)
    f, ax = plt.subplots(figsize=(12., 12.))
    plt.imshow(pkl['predicted_aligned_error'].T,cmap='viridis',origin='lower')
    plt.colorbar()
    plt.savefig(args.output.name)

else:
    f, ax = plt.subplots(figsize=(12., 12.))
    plt.imshow(pkl['predicted_aligned_error'].T,cmap='viridis',origin='lower')
    plt.colorbar()
    plt.savefig(args.output.name)

