#!/bin/bash


MSAdir=$HOME/Downloads/MSAS/marks_paired/
seqdir=$HOME/Downloads/MSAS/marks/
bin=$HOME/git/huintaf2/bin/


name=`basename $1 _top.a3m`
first=${name:0:6}
len=`${bin}/seqlen.bash ${seqdir}/${first}.fasta`
echo -n $name " "
python3 ${bin}/inv_cov.py $1 $len 
