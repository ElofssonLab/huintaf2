#!/bin/bash -x

dir=`dirname $1`;
name=`basename $dir`
model=`echo $1 | sed "s/.*model_//g" | sed "s/.*ranked_//g" | sed "s/_.*//g" | sed "s/.pkl$//g"`

if [ ! -f ${dir}/${name}_${model}.pkl ]
then
    ~/git/huintaf2/bin/extractptm.py  $1 >  ${dir}/${name}_${model}.ptm
fi
