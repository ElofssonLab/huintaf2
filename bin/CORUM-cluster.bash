#!/bin/bash -x

sed "s/,/ /g" data/CORUM-complexes.csv > foo

while I= read -r i j k
do
    if [ -d CORUM/$j-$k/$j-$k.pdb ]
    then
	echo $j-$k >> CORUM-clusters/cluster-$i.txt
    elif [ -f CORUM/$k-$j/$k-$j.pdb ]
    then
	echo $k-$j  >> CORUM-clusters/cluster-$i.txt
    else
	echo $j-$k >> CORUM-missing.txt
    fi
done < foo

for c in  CORUM-clusters/cluster*.txt
do
    j=`basename $c .txt`
    rm -rf CORUM-clusters/$j/
    mkdir -p CORUM-clusters/$j
    unset line
    for i in `cat $c`
    do
	line=`echo -n $line " " $i `
	cp CORUM/${i}/${i}.pdb CORUM-clusters/$j/
    done
    echo $line > CORUM-clusters/$j/${j}-files.txt
    #ls
    bin/makecomplex.py --mintm 0.8 -i CORUM-clusters/$j/*pdb -o CORUM-clusters/$j/${j}_mintm08.pdb
    # Some cleaning up
    rm CORUM-clusters/$j/*_[A-Za-z].pdb
    rm CORUM-clusters/$j/*_[A-Za-z].rot
    rm CORUM-clusters/$j/*_[A-Za-z].MM
done
