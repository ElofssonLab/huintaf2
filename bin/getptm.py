#!/usr/bin/env python3
import pandas as pd
import pickle
import sys
import numpy as np
import argparse
import re

arg_parser = argparse.ArgumentParser(description="Extracts information from AlphaFold picke file")

group = arg_parser.add_mutually_exclusive_group(required=True)
group.add_argument("-a","--alphafold",type=argparse.FileType('r'),help="Input alphafold file")
group.add_argument("-m","--multimer",type=argparse.FileType('r'),help="Input alphafold multimer file")

args = arg_parser.parse_args()
if (args.alphafold):
    pkl=pd.read_pickle(args.alphafold.name)
    #print (pkl.keys())
    print ("Name,predicted_aligned_error,plddt,max_predicted_aligned_error,ptm")
    print (args.alphafold.name,",",np.mean(pkl['predicted_aligned_error']),",",np.mean(pkl['plddt']),",",pkl['max_predicted_aligned_error'],",",pkl[ 'ptm'])
    #print (args.alphafold.name,",",pkl['predicted_aligned_error'],",",np.mean(pkl['predicted_lddt']),",",np.mean(pkl['plddt']),",",pkl['max_predicted_aligned_error'],",",pkl[ 'ptm'])
else:
    pkl=pd.read_pickle(args.multimer.name)
    #print (pkl.keys())
    print ("name,ptm,iptm,max_predicted_aligned_error,pLDDT,maxPAE,rankconf")
    print (args.multimer.name,",",pkl["ptm"],",",pkl["iptm"],",",pkl["max_predicted_aligned_error"],",",np.mean(pkl["plddt"]),",",pkl["max_predicted_aligned_error"],",",pkl["ranking_confidence"])
    
