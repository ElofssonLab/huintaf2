#!/bin/bash -x
#SBATCH -A berzelius-2021-64
#SBATCH --output=/proj/berzelius-2021-29/users/x_adish/fd-multimer/src/logs/%A_%a.out
#SBATCH --error=/proj/berzelius-2021-29/users/x_adish/fd-multimer/src/logs/%A_%a.error
#SBATCH --array=1-1
#SBATCH -t 4:00:00
#SBATCH --gpus=1

export NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'

if [ -z $1 ]
then
        offset=0
else
        offset=$1
fi

LN=$(($SLURM_ARRAY_TASK_ID+$offset))

dir=/proj/berzelius-2021-29/users/x_adish/fd-multimer

###Get ID###
file=$dir/data/pdb_multimers/3mer_idlist
#file=/proj/berzelius-2021-29/users/x_adish/fd-multimer/output/missing_3mer_cyc3.txt
LN=$(($SLURM_ARRAY_TASK_ID+$offset))
id=$(sed -n ${LN}p $file)
echo $id

####Get fasta file####
FASTADIR1=$dir/data/pdb_multimers/fasta_files/3mer
FASTADIR2=$dir/data/pdb_multimers/merged_fasta/3mer

echo $FASTADIR1/${id}_*.fasta

if [ -f $FASTADIR1/${id}_B.fasta ]; then
	F1=$FASTADIR1/${id}_A.fasta
	F2=$FASTADIR1/${id}_B.fasta
	F3=$FASTADIR1/${id}_C.fasta
else
	F1=$FASTADIR1/${id}_A.fasta
	F2=$FASTADIR1/${id}_A.fasta
	F3=$FASTADIR1/${id}_A.fasta
fi

FASTAFILE=$FASTADIR2/${id}.fasta
${dir}/src/run_AF/merge_fasta_3.bash $F1 $F2 $F3 > $FASTAFILE

####Get chain break####
seqlen1=`$dir/src/run_AF/seqlen.bash $F1`
seqlen2=`$dir/src/run_AF/seqlen.bash $F2`
seqlen2=$(( $seqlen1 + $seqlen2 ))
CB="${seqlen1},${seqlen2}"

####Get MSAs####
MSA=/proj/berzelius-2021-29/users/x_adish/fd-multimer/data/pdb_multimers/hhblits_msa/3mer
FUSE_MSA=$MSA/${id}_fused.a3m

#SINGLE_MSA1=$MSA/${id}_single_A.a3m
#SINGLE_MSA2=$MSA/${id}_single_B.a3m
#SINGLE_MSA3=$MSA/${id}_single_C.a3m
#MSAS="$FUSE_MSA,$SINGLE_MSA1,$SINGLE_MSA2,$SINGLE_MSA3"

MSAS="$FUSE_MSA"

##### AF2 CONFIGURATION #####
### common portion to all configuration paths
COMMON='/proj/berzelius-2021-29/'
### Local path of alphafold directory
AFHOME=$COMMON'/users/x_patbr/FoldDock/src/alphafold/'

### Path of singularity image
SINGULARITY=$COMMON'/singularity_images/alphafold.sif'

### path of param folder containing AF2 Neural Net parameters.
### download from: https://storage.googleapis.com/alphafold/alphafold_params_2021-07-14.tar)
PARAM=$COMMON'/Database/af_params/'
### Path where AF2 generates its output folder structure
OUTFOLDER='/proj/berzelius-2021-29/users/x_adish/fd-multimer/output/fused_3_cyc3'

###### DATABASES ######
UNICL30=$COMMON'/alphafold/databases/uniclust30_2018_08/uniclust30_2018_08'
UNIREF=$COMMON'/alphafold/databases/uniref90/uniref90.fasta'
MGNIFY=$COMMON'/alphafold/databases/mgnify/mgy_clusters.fa'
PDB70=$COMMON'/alphafold/databases/pdb70/pdb70'
MMCIF=$COMMON'/alphafold/databases/pdb_mmcif/pdb_mmcif/raw/'
OBS=$COMMON'/alphafold/databases/pdb_mmcif/obsolete.dat'
BFD=$COMMON'/alphafold/databases/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt'

### Running options for obtaining a refines tructure ###
PRESET='full_dbs' #Choose preset model configuration - no ensembling (full_dbs) and (reduced_dbs) or 8 model ensemblings (casp14).
MAX_RECYCLES=3 #max_recycles (default=3)
MODEL_NAME='model_1_ptm'

#Regarding the run mode
#Options: Must specify a run mode: --full pipeline or --fold_only or --msa-only

pushd $AFHOME
cd $AFHOME
singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
	python3 $AFHOME/run_alphafold.py \
		--fasta_paths=$FASTAFILE \
		--msas=$MSAS \
	       	--chain_break_list=$CB \
		--output_dir=$OUTFOLDER \
		--model_names=$MODEL_NAME \
	       	--data_dir=$PARAM \
                --fold_only \
		--preset=$PRESET \
		--max_recycles=$MAX_RECYCLES
popd

