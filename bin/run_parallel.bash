#!/bin/bash -x
#SBATCH -A berzelius-2021-64
#SBATCH --output=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/out/runall-%j.out
#SBATCH --error=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/err/runall-%j.err
#SBATCH --array=1-20
#SBATCH -N 1
#SBATCH --gpus-per-task=1
#SBATCH --export=ALL,CUDA_VISIBLE_DEVICES
#SBATCH --gpus=8
#SBATCH -t 48:00:00
export NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'
export CUDA_VISIBLE_DEVICES='8'

## #SBATCH --exclusive



#list=$1
#offset=$2

#pos=$(($SLURM_ARRAY_TASK_ID + $offset))
#id=`tail -n+$pos $list | head -n 1 | gawk '{print $0}'`
#id2=`tail -n+$pos $list | head -n 1 | gawk '{print $2}'`


#OUTFOLDER=pdb-multi/
#NAME=`echo ${id} | sed "s/\ /-/g"`
#pushd $AFHOME
#cd $AFHOME
#NAME="./" # to not run this part

#cat $1 | parallel -j 8 /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/runall-multimer.bash {}
while I= read -r i j ; do /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/runall-multimer-conda.bash $i $j  ; done < $1 &
while I= read -r i j ; do /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/runall-multimer-conda.bash $i $j  ; done < $1 &
while I= read -r i j ; do /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/runall-multimer-conda.bash $i $j  ; done < $1 &
while I= read -r i j ; do /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/runall-multimer-conda.bash $i $j  ; done < $1 &
while I= read -r i j ; do /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/runall-multimer-conda.bash $i $j  ; done < $1 &
while I= read -r i j ; do /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/runall-multimer-conda.bash $i $j  ; done < $1 &
while I= read -r i j ; do /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/runall-multimer-conda.bash $i $j  ; done < $1 &
while I= read -r i j ; do /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/runall-multimer-conda.bash $i $j  ; done < $1 &


   
# /opt/conda/bin/python3 /proj/berzelius-2021-29//users/x_patbr/FoldDock/src/alphafold//run_alphafold.py --fasta_paths=merged/B2RXF5-Q99592.fasta --msas=merged/B2RXF5-Q99592_fused.a3m --chain_break_list=422 --output_dir=pdb-multi/ --model_names=model_1 --data_dir=/proj/berzelius-2021-29//Database/af_params/ --fold_only --preset=full_dbs --max_recycles=10
