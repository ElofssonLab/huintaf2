#!/usr/bin/env python3
import argparse
#from Bio.PDB.vectors import rotaxis, calc_angle, calc_dihedral
#from Bio.PDB.Polypeptide import is_aa
#from math import pi
import re
import math
import numpy  as np
from Bio.PDB import PDBIO
from Bio import SeqIO
from Bio.PDB import PDBParser
from Bio.PDB import MMCIFParser
#from Bio.PDB import Selection
from Bio.PDB import MMCIFIO
from Bio.PDB import PDBIO
from Bio.PDB import Select
from Bio.PDB.Chain import Chain
#from Bio.PDB.Polypeptide import PPBuilder
import subprocess
import sys
import os
import pandas as pd
#from Bio.PDB import Entity
import tempfile


def sigmoid(x, L ,x0, k, b):
    y = L / (1 + np.exp(-k*(x-x0)))+b
    return (y)

def pDockQ(structure,cutoff=10.0):
    i=0
    atoms=[]
    coords=[]
    residues=[]
    chains=[]
    for chain in structure.get_chains():
        chains+=[chain]
        i+=1
        #print (chains)
    interface_residues1=[]
    interface_residues2=[]
    for res1 in chains[0]:
        for res2 in chains[1]:
            #Atom-atom distance
            #print (res1,res2)
            test=False
            for i in res1:
                if test:break
                for j in res2:
                    dist = np.linalg.norm(i.coord - j.coord)
                    #scipy.spatial.distance.euclidian
                    #dist = distance.euclidean(coords[i], coords[len(ch1_res_nos)+j]) #Need to add l1 to get the right coords
                    if dist < cutoff:
                        #Save residues
                        #print ("Appending",res1,res2)
                        interface_residues1.append(res1.id[1])
                        interface_residues2.append(res2.id[1])
                        test=True
                        break
                    elif dist > 2*cutoff: # To speed up things
                        test=True
                        break

    #print (interface_residues1,interface_residues2)
    #print (np.unique(interface_residues1),np.unique(interface_residues2))
    #interface_res_num.append(np.unique(interface_residues).shape[0])
    #atoms, residue_numbers, coords = np.array(atoms), np.array(residue_numbers), np.array(coords)
    if1=np.unique(interface_residues1)
    if2=np.unique(interface_residues2)
    NumRes=if1.shape[0]+if2.shape[0]
    i=0
    b=0
    for res in chains[0]:
        if res.id[1] in if1:
            #print (res)
            b+=res['CA'].get_bfactor()
            i+=1
    for res in chains[1]:
        if res.id[1] in if2:
            b+=res['CA'].get_bfactor()
            i+=1
    IF_plDDT=b/i
    #Get res nos
    #Get chain cut
    #ch1_res_nos = np.argwhere(residue_numbers<=l1)[:,0] #All residue numbers
    #ch2_res_nos =  np.argwhere(residue_numbers>l1)[:,0]
    
    #print (NumRes,IF_plDDT)
    #return (NumRes,IF_plDDT)
    popt=[7.07140240e-01, 3.88062162e+02, 3.14767156e-02, 3.13182907e-02]
    tiny=1.e-20
    pdockq=sigmoid(np.log(NumRes+tiny)*IF_plDDT,*popt)
    return(pdockq)




def center_of_mass(entity, geometric=False):
    """
    Returns gravitic [default] or geometric center of mass of an Entity.
    Geometric assumes all masses are equal (geometric=True)
    """
    
    # Structure, Model, Chain, Residue
    if isinstance(entity, Entity.Entity):
        atom_list = entity.get_atoms()
    # List of Atoms
    elif hasattr(entity, '__iter__') and [x for x in entity if x.level == 'A']:
        atom_list = entity
    else: # Some other weirdo object
        raise ValueError("Center of Mass can only be calculated from the following objects:\n"
                            "Structure, Model, Chain, Residue, list of Atoms.")
    
    masses = []
    positions = [ [], [], [] ] # [ [X1, X2, ..] , [Y1, Y2, ...] , [Z1, Z2, ...] ]
    
    for atom in atom_list:
        
        masses.append(atom.mass)
        
        for i, coord in enumerate(atom.coord.tolist()):
            positions[i].append(coord)

    # If there is a single atom with undefined mass complain loudly.
    if 'ukn' in set(masses) and not geometric:
        raise ValueError("Some Atoms don't have an element assigned.\n"
                         "Try adding them manually or calculate the geometrical center of mass instead.")
    
    if geometric:
        return [sum(coord_list)/len(masses) for coord_list in positions]
    else:       
        w_pos = [ [], [], [] ]
        for atom_index, atom_mass in enumerate(masses):
            w_pos[0].append(positions[0][atom_index]*atom_mass)
            w_pos[1].append(positions[1][atom_index]*atom_mass)
            w_pos[2].append(positions[2][atom_index]*atom_mass)

        return [sum(coord_list)/sum(masses) for coord_list in w_pos]

class ChainSelect(Select):
    def __init__(self,chain):
        self.chain=chain
    def accept_chain(self,chain):
        if (re.search(chain.get_id(),self.chain)):
            return 1
        else:
            return 0


def overlap(structure,chain,cutoff=5,maxplDDT=70):
    overlap=np.zeros(len(chain))
    j=0
    for residue1 in chain:
        # Get some atoms
        ca1 = residue1["CA"]
        if ca1.get_bfactor()<maxplDDT:
            continue
        for c in structure[0]:
            for residue2 in c:
                #print ("Test",residue1,residue2)
                ca2 = residue2["CA"]
                if ca2.get_bfactor()<maxplDDT:
                    continue
                distance = ca1 - ca2
                if (distance < cutoff):
                    overlap[j]+=1
                    break
        j+=1
    return(overlap)
        


def rotate(file):    
    # Using readlines()
    rotfile = open(file, 'r')
    Lines = rotfile.readlines()
    translation_matrix=np.array((0,0,0),"f")
    rotation_matrix=np.array(((0,0,0),(0,0,0),(0,0,0)),"f")
    translation=[0,0,0]
    for line in Lines:
        if (line[0]=="0"):
            temp=line.split()
            #print (temp)
            translation_matrix[0]=1*float(temp[1])
            rotation_matrix[0,0]=1*float(temp[2])
            rotation_matrix[0,1]=1*float(temp[3])
            rotation_matrix[0,2]=1*float(temp[4])
        elif (line[0]=="1"):
            temp=line.split()
            translation_matrix[1]=1*float(temp[1])
            rotation_matrix[1,0]=1*float(temp[2])
            rotation_matrix[1,1]=1*float(temp[3])
            rotation_matrix[1,2]=1*float(temp[4])
        elif (line[0]=="2"):
            temp=line.split()
            translation_matrix[2]=1*float(temp[1])
            rotation_matrix[2,0]=1*float(temp[2])
            rotation_matrix[2,1]=1*float(temp[3])
            rotation_matrix[2,2]=1*float(temp[4])
            #rotation_matrix=rotation_matrix.T
    #print (rotation_matrix)
    #print (rotation_matrix[0,0])
    #print (translation_matrix)
    return(rotation_matrix,translation_matrix)


def addchain(complex,chain_complex,structure,chain_structure,minTM=0.8,maxoverlap=0.25,maxRMSD=9999,minSeqID=0,overlapdist=5):

    tmpfile1=tempfile.NamedTemporaryFile(suffix=".pdb")
    tmpfile2=tempfile.NamedTemporaryFile(suffix=".pdb")
    rotfile=tempfile.NamedTemporaryFile(suffix=".rot")
    outfile=tempfile.NamedTemporaryFile(suffix=".MM")

    io=PDBIO()
    io.set_structure(complex[0])
    #chain=str(chain_complex.id)
    #print ("files:",tmpfile1,tmpfile2)
    #print ("files:",tmpfile1.name,tmpfile2.name)
    io.save(tmpfile1.name,ChainSelect(chain_complex))
    
    io.set_structure(structure[0])
    io.save(tmpfile2.name,ChainSelect(chain_structure))

    MM=args.MM.name
    with open(outfile.name, "w+") as file:
        mm=subprocess.run([MM,"-m",rotfile.name,tmpfile2.name,tmpfile1.name],stdout=file)
    file.close()
    #print (mm)
    #sys.exit()
    file=open(outfile.name, "r")
    TM1=0
    TM2=0
    for line in file.readlines():
        #print (line)
        if re.match(r'TM-score=.*Chain_1',line):
            TM1=float(line.split()[1])
        elif re.match(r'TM-score=.*Chain_2',line):
            TM2=float(line.split()[1])
        elif re.match(r'Aligned.*RMSD',line):
            RMSD=float(re.sub(r'\,','',line.split()[4]))
            SeqID=float(line.split()[6])
            #print (tempdata)
            #MM = df_MM.append(tempdata, ignore_index=True)
    #print (TM1,TM2)
    TM=max(TM1,TM2)
    if (TM<minTM):
        print ("Skipping, mintm:",minTM,TM)
        return (complex,False,"-")
    if (RMSD>maxRMSD):
        print ("Skipping, maxRMSD:",maxRMSD,RMSD)
        return (complex,False,"-")
    if (SeqID<minSeqID):
        print ("Skipping, seqID:",minSeqID,SeqID)
        return (complex,False,"-")
    rotation_matrix,translation_matrix=rotate(rotfile.name)
    structure[0].transform(rotation_matrix.T, translation_matrix)
    added=False
    #print ("Complex",complex)
    complex_chains=[]
    for chain in complex[0]:
        complex_chains+=[chain.id]
    for chain in structure.get_chains():
        if chain.id==chain_structure:continue
        o=overlap(complex,chain,overlapdist)
        #print ("Overlap",i,o,np.sum(o)/len(o))
        if (np.sum(o)/len(o)<maxoverlap):
            while chain.id in complex_chains:
                j=1
                while chr(ord(chain.id)+j) in complex_chains:
                    j+=1
                chain.id=chr(ord(chain.id)+j)
            complex_chains+=[chain.id]
            print ("Adding", i,chain.id)
            complex[0].add(chain)
            added=True
        else:
            print ("Skipping overlap",len(o),maxoverlap,i,chain.id)
    #os.remove(tmpfile1.name) 
    #os.remove(tmpfile2.name) 
    #os.remove(rotfile.name) 
    #os.remove(outfile.name) 
    return (complex,added,chain.id)


if __name__ == "__main__":
    arg_parser = argparse.\
        ArgumentParser(                description="Translate and rotate a pdb file")
    group = arg_parser.add_mutually_exclusive_group(required=False)
    group.add_argument('-norank', help='No ranking (using file order)',action='store_true')
    group.add_argument('-rankpDockQ', help='ranking using PDockQ (default)',action='store_true')
    group.add_argument('-rankRandom', help='Random ranking ',action='store_true')
    group.add_argument('-rankNeigbours', help='Rank by number of neighbours',action='store_true')

    arg_parser.add_argument("-v","--verbose", action='store_true', required=False,help="Verbose output")

    arg_parser.add_argument('-i','--input', nargs='+', help='List of input pdb files (currenly only 2)', required=True)
    arg_parser.add_argument("-M",'--MM',type=argparse.FileType("r"),default=os.getenv("HOME")+"/git/huintaf2/bin/MMalign")
    arg_parser.add_argument("-o","--outfile",type=argparse.FileType("w"),required=False)
    arg_parser.add_argument("-l","--maxplDDT", help="Maxmimum pLDDT values (in bfactor column) to be incouded in overlap", default=70, type=float,required=False)
    arg_parser.add_argument("-m","--maxoverlap", help="Maxmimum fraction of overlap to include chain", default=0.5, type=float,required=False)
    arg_parser.add_argument("-O","--overlapdist", help="Distance used to calculate overlap", default=5, type=float,required=False)
    arg_parser.add_argument("-c","--cutoff",default=5,help="Distance cutoff to calculate overlap", type=float,required=False)
    arg_parser.add_argument("-tm","--minTM",default=0.80,help="Min TM score for overlapping chains", type=float,required=False)
    arg_parser.add_argument("-rmsd","--maxRMSD",default=9999,help="Max RMSD for overlapping chains", type=float,required=False)
    arg_parser.add_argument("-id","--minSeqID",default=0.,help="Min sequence identity for overlapping chains", type=float,required=False)
    arg_parser.add_argument("-p","--minpdockq",default=0.23,help="Min pDockQ score for oncluding a pair", type=float,required=False)    
    arg_parser.add_argument("-I","--maxiter",default=1,help="Maximum number of iterations (can not use) ", type=int,required=False)
    
    args = arg_parser.parse_args()
    std = 1
    cutoff=args.cutoff
    maxoverlap=args.maxoverlap
    mintm=args.minTM
    maxRMSD=args.maxRMSD
    minSeqID=args.minSeqID
    maxplDDT=args.maxplDDT
    #
    MM=args.MM
    
    #name1=sys.argv[1]
    #name2=sys.argv[2]
    i=0


    # Read in all data
    
    df=pd.DataFrame()
    cutoff=10
    
    for name in args.input:
        #names+=[name]
        tmpname=os.path.basename(name)
        tmpname=re.sub(".pdb$","",re.sub(".cif$","",tmpname))

        #names+=[tmpname]
        chain1=re.sub("-.*$","",tmpname)
        chain2=re.sub("^.*-","",tmpname)
        #if (p>minpDockQ) keep

        try:
            parser = MMCIFParser()
            tempstruct = parser.get_structure("protein-"+str(i), name)
            io=PDBIO()
            io.set_structure(tempstruct)
            io.save(tempname[-1]+".pdb")
        except:
            parser = PDBParser()
            tempstruct = parser.get_structure("protein-"+str(i), name)
        #structures+=[tempstruct]
        #print (name,tempstruct)
        p=pDockQ(tempstruct)
        #print (p)
        #print (structure)

        #print (chain1[i],chain2[i])
        if (p>args.minpdockq):
            print ("Adding file:",name)
            df=df.append({
                "ID":i,
                "File":name,
                'Structure':tempstruct,
                "Chain1":chain1,
                "Chain2":chain2,
                "pDockQ":p,
                "Neighbours":[]},ignore_index=True)
            i+=1
        else:
            print ("Skipping (pDockQ low):",name)
    chains=np.unique(df.Chain1.to_list()+df.Chain2.to_list())

    #print ("Test1:",chains)
    #print ("Test2:",df)
    
    
    df_complex=pd.DataFrame(columns=
                               ["Chain"])
    i=0

    # Analyse all structures
    #pDockQs={}
    #contacts={}
    
    #for c in chain1+chain2:
    #    contacts[c]+=1
    #for dimer in df.sort_values(by=["pDockQ"]["Structure"]).iterrows():
    #for dimer in df.iterrows():
    #    print (dimer)
    #    print (dimer[1])

    neighbours={}
    for c in chains:
        neighbours[c]=len(df[(df.Chain1==c)|(df.Chain2==c)])

    #df.apply(lambda x: pd.Series([1, 2], index=['foo', 'bar']), axis=1)
        
    df["neighbours"]=df.Chain1.apply(lambda x: neighbours[str(x)])+df.Chain2.apply(lambda x: neighbours[str(x)])
    # not implemented yed
    if (args.norank):
        df['StrRank'] = df['ID']
    elif (args.rankRandom):
        df['StrRank'] = df.rank(method="random",pct=False)        
    elif (args.rankNeigbours):
        df['StrRank'] = df['neighbours'].rank(ascending=False,pct=False)
    else:
        # Default rank by pDockq
        df['StrRank'] = df['pDockQ'].rank(ascending=False,pct=False)

    
    # we start with first ranked model
    complex=df[df.StrRank==1].iloc[0]["Structure"]
    complexchains={}
    num=0
    added=False

    # This is not necessary I thiink
    while (num<=args.maxiter) and (len(complexchains)<len(chains)): #&
        num+=1
        print ("Iteration Num:",num)
        # Let us check if we added something in the first round
        # If not we decrease minTM
        #if (num==2):
        #    if 
        chain1=df[df.StrRank==1.].iloc[0]["Chain1"]
        chain2=df[df.StrRank==1.].iloc[0]["Chain2"]
        complexchains={chain1:"A",chain2:"B"}
        i=1
        testadded=[]
        while i < len(df):
            print ("Testing with structure ",i)
            structure=df[df.StrRank==i].iloc[0]["Structure"]
            chainA=df[df.StrRank==i].iloc[0]["Chain1"]
            chainB=df[df.StrRank==i].iloc[0]["Chain2"]
            A=False
            B=False
            if (not i in testadded):
                print ("Trying to add:",chainA,chainB," on ",complexchains.items()) 
                A=False
                B=False
                for chain,id in complexchains.items():
                    if chainA==chain:
                        A=True
                        chain_complex=id
                    if chainB==chain:
                        B=True 
                        chain_complex=id
                if (A and not B):
                    structure_chain="A"
                    print ("Trying with:",chainB) 
                    complex,test,newchain=addchain(complex,chain_complex,structure,structure_chain)
                    if (test):
                        print ("Success:")
                        complexchains[chainB]=newchain
                        i=0
                        #break
                    else:
                        testadded+=[i]
                        
                elif (not A and  B):
                    structure_chain="B"
                    print ("Trying to add:",chainA) 
                    complex,test,newchain=addchain(complex,chain_complex,structure,structure_chain)
                    if (test):
                        print ("Success adding:")
                        complexchains[chainA]=newchain
                        i=0
                        #break
                    else:
                        testadded+=[i]

                elif A:
                    print("Both chain already added:",i,chainA+"-"+chainB)
                    testadded+=[i]
                else:
                    print("No chains to superimpose:",i,chainA+"-"+chainB)
                    #print (complex)
            i+=1
                    
    io=PDBIO()
    io.set_structure(complex)
    io.save(args.outfile.name)


    # Four things to add
    ## # 1) New iteration scheme (start over after one is added)
    # 2) Alternative methods to rank
    ### 3) Skip overlap of disordered residues
    # 3b) Do MMalign without disordered regions (How ?)
    # 4) Add possibility to use bigger complexes than dimers 
