#!/bin/bash -x

find ./pdb/ -size 0 -exec rm {} \;
for i in `find pdb/ -name "*DockQ" -exec grep -Hc already {} \; | grep -v :0 |sed s/:.*//g`
do
    rm $i
done

for i in pdb/*/*DockQ
do
    j=`basename $i .DockQ`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  > data/DockQ.csv

for i in `find pdb/ -name "*DockQ.reorder" -exec grep -Hc already {} \; | grep -v :0 |sed s/:.*//g`
do
    rm $i
done


for i in pdb/*/*DockQ.reorder
do
    j=`basename $i .DockQ.reorder`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  >  data/DockQreorder.csv

for i in pdb/*/*DockQall2
do
    j=`basename $i .DockQall2`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  >  data/DockQall.csv

for i in AF-MULTI/*/*DockQall2
do
    j=`basename $i .DockQall2`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  >  data/AFmulti-DockQall.csv

for i in AF-MULTI2/*/*DockQall2
do
    j=`basename $i .DockQall2`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  >  data/AFmulti2-DockQall.csv

for i in AF-MULTI-NOPAIR/*/*DockQall2
do
    j=`basename $i .DockQall2`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  >  data/AFmulti-nopair-DockQall.csv

for i in AF-MULTI-PROK/*/*DockQall2
do
    j=`basename $i .DockQall2`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  >  data/AFmulti-prok-DockQall.csv

find pdb/ AF-*/ HuRI*/ -name "*-*.pdb" -exec bin/pDockQ.bash {} \;




grep -h Name AF-MULTI/*/*csv  | head -1 > data/AFmulti-humap.csv
grep -vh Name AF-MULTI/*/*csv >> data/AFmulti-humap.csv

grep -h Name AF-MULTI2/*/*csv  | head -1 > data/AFmulti2-humap.csv
grep -vh Name AF-MULTI2/*/*csv >> data/AFmulti2-humap.csv

grep -h Name AF-MULTI-NOPAIR/*/*csv  | head -1 > data/AFmulti-nopair-humap.csv
grep -vh Name AF-MULTI-NOPAIR/*/*csv >> data/AFmulti-nopair-humap.csv

grep -h Name AF-MULTI-PROK/*/*csv  | head -1 > data/AFmulti-prok-humap.csv
grep -vh Name AF-MULTI-PROK/*/*csv >> data/AFmulti-prok-humap.csv

grep -h Name pdb/*/*csv  | head -1 > data/humap.csv
grep -vh Name pdb/*/*csv | grep -v reorder >> data/humap.csv



# A few MM files crashed (as they are only CA)
find pdb/ -name "*.MM" -size 1 -exec rm {} \;

for i in pdb/*/*MM
do
    j=`basename $i .MM`
    echo -n $j ","
    grep TM-score= $i | head -2 | sort -n -k 2 | tail -1 |gawk '{print $2}'
done  |sed "s/ //g" >   data/MM.csv

find pdb/ -name "*.MM.reorder" -size 1 -exec rm {} \;
for i in pdb/*/*MM.reorder
do
    j=`basename $i .MM.reorder`
    echo -n $j ","
    grep TM-score= $i | head -2 | sort -n -k 2 | tail -1 |gawk '{print $2} '
done  |sed "s/ //g" >   data/MMreorder.csv

find pdb/ -name "*.MMall" -size 1 -exec rm {} \;
find pdb/ -name "*.MMall2" -size 0 -exec rm {} \;
for i in pdb/*/*MMall2
do
    j=`basename $i .MMall2 `
    a=`grep TM-score= $i | sed "s/.*= //g" | sed "s/\ .*//g" | sort -n | tail -1 `
    #b=`grep TM-score= pdb/$j/$j.MM* | head -2 | sort -n -k 2 | tail -1 |gawk '{print $2}'`
    b=`grep TM-score= HuRI/$j/$j.MM* | head -2 | sort -n -k 2 | tail -1 | sed "s/.*TM-score=//g" |gawk '{print $1}'`
    echo -n $j ","
    echo $a $b | gawk '{ if($1>$2){print $1}else{print $2}}'
done  |sed "s/ //g" >  data/MMall.csv



# HuRI

find ./HuRI/ -size 0 -exec rm {} \;

for i in HuRI/*/*DockQ
do
    j=`basename $i .DockQ`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  > data/HuRI-DockQ.csv

for i in `find HuRI/ -name "*DockQ.reorder" -exec grep -Hc already {} \; | grep -v :0 |sed s/:.*//g`
do
    rm $i
done

for i in HuRI/*/*DockQ.reorder
do
    j=`basename $i .DockQ.reorder`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  > data/HuRI-DockQreorder.csv

for i in HuRI/*/*DockQall2
do
    j=`basename $i .DockQall2`
    echo -n $j ","
    gawk '{print $2}' $i
done | sed "s/ //g"  > data/HuRI-DockQall.csv


# A few MM files crashed (as they are only CA)
find HuRI/ -name "*.MM" -size 1 -exec rm {} \;

for i in HuRI/*/*MM
do
    j=`basename $i .MM`
    echo -n $j ","
    grep TM-score= $i | head -2 | sort -n -k 2 | tail -1 |gawk '{print $2}'
done  |sed "s/ //g" >  data/HuRI-MM.csv

find HuRI/ -name "*.MM.reorder" -size 1 -exec rm {} \;
for i in HuRI/*/*MM.reorder
do
    j=`basename $i .MM.reorder`
    echo -n $j ","
    grep TM-score= $i | head -2 | sort -n -k 2 | tail -1 |gawk '{print $2} '
done  |sed "s/ //g" >  data/HuRI-MMreorder.csv

find HuRI/ -name "*.MMall" -size 1 -exec rm {} \;
find HuRI/ -name "*.MMall2" -size 0 -exec rm {} \;
for i in HuRI/*/*MMall2
do
    j=`basename $i .MMall2 `
    a=`grep TM-score= $i | sed "s/.*= //g" | sed "s/\ .*//g" | sort -n | tail -1 `
    b=`grep TM-score= HuRI/$j/$j.MM* | head -2 | sort -n -k 2 | tail -1 | sed "s/.*TM-score=//g" |gawk '{print $1}'`
    echo -n $j ","
    echo $a $b | gawk '{ if($1>$2){print $1}else{print $2}}'
done  |sed "s/ //g" > data/HuRI-MMall.csv




# Now we start with plDDF

#for i in pdb/*/*MMall2
#do
#    #j=`basename $i .MMall | sed s/\-[0-9][A-Za-z0-9][A-Za-z0-9][A-Za-z0-9]//g`
#    j=`basename $i .MMall2 `
#    a=`grep TM-score= $i | head -2 | sort -n -k 5 | tail -1 |sed "s/.*= //g" | sed "s/ .*//g' `
#    b=`grep TM-score= pdb/$j/$j.MM* | head -2 | sort -n -k 2 | tail -1 |gawk '{print $2}'`
#    echo -n $j ","
#    echo $a $b | gawk '{ if($1>$2){print $1}else{print $3}}'
#    #grep TM-score= $i | head -2 | sort -n -k 5 | tail -1 |gawk '{print $2","$3","$4","$5}' 
#done  |sed "s/ //g" >  MMall.csv

for i in pdb/*/*.pLDDT
do
    k=`dirname $i`
    j=`basename $k`
    l=`echo $j  | sed "s/-[0-9]-/-/g" | sed "s/-10-/-/g" | sed "s/-[0-9]$//g" | sed "s/-10$//g"`
    echo -n $l ","
    #gawk '{if ($1=="IF_NumRes:") {N=$2}  else if ($1=="IF_pLDDT") {I=$2}   else {print N ","  I "," $2}}' $k/$j.pLDDT
    #grep Summary: $k/$j.pLDDT | sed "s/\[//g" | sed "s/\]//g" | sed "s/Summary://g"
    gawk '{if ($1=="IF_NumRes:") {N=$2}  
    	 else if ($1=="IF_pLDDT") {I=$2}   
    	 else if ($1=="pLDDT") {p=$2}   
    	 else if ($1=="Summary:") {print N ","  I ","  p  }}'    $k/$j.pLDDT 
done |sed "s/ //g" >  data/pLDDT.csv

echo "Name,NumRes,IF_plDDT,plDDT" >  data/negatome-pLDDT.csv
find ./negatome-pLDDT/ -size 0 -exec rm {} \;
for i in negatome-pLDDT/*.pLDDT
do
    j=`basename $i .pLDDT`
    l=`echo $j  | sed "s/-[0-9]-/-/g" | sed "s/-10-/-/g" | sed "s/-[0-9]$//g" | sed "s/-10$/_/g"`
    echo -n $l ","
    #gawk '{if ($1=="IF_NumRes:") {N=$2}  else if ($1=="IF_pLDDT") {I=$2}   else {print N ","  I "," $2}}' $k/$j.pLDDT
    #grep Summary: $k/$j.pLDDT | sed "s/\[//g" | sed "s/\]//g" | sed "s/Summary://g"
    gawk '{if ($1=="IF_NumRes:") {N=$2}  
    	 else if ($1=="IF_pLDDT") {I=$2}   
    	 else if ($1=="pLDDT") {p=$2}   
    	 else if ($1=="Summary:") {print N ","  I ","  p }}'    $i
done |sed "s/ //g" >>  data/negatome-pLDDT.csv

echo "Name,id1,id2,NumRes,IF_plDDT,plDDT" >  data/HuRI-pLDDT.csv
find ./HuRIT/ -size 0 -exec rm {} \;
for i in HuRI/*/*.pLDDT
do
    j=`basename $i .pLDDT`
    l=`echo $j  | sed "s/-/,/g"`    
    echo -n $j "," $l ","
    #gawk '{if ($1=="IF_NumRes:") {N=$2}  else if ($1=="IF_pLDDT") {I=$2}   else {print N ","  I "," $2}}' $k/$j.pLDDT
    #grep Summary: $k/$j.pLDDT | sed "s/\[//g" | sed "s/\]//g" | sed "s/Summary://g"
    gawk '{if ($1=="IF_NumRes:") {N=$2}  
    	 else if ($1=="IF_pLDDT") {I=$2}   
    	 else if ($1=="pLDDT") {p=$2}   
    	 else if ($1=="Summary:") {print N ","  I ","  p }}'    $i
done |sed "s/ //g" >> data/HuRI-pLDDT.csv

echo "Name,id1,id2,NumRes,IF_plDDT,plDDT" > data/Marks-pLDDT.csv
find ./marks/ -size 0 -exec rm {} \;
for i in marks/*/*.pLDDT
do
    j=`basename $i .pLDDT`
    l=`echo $j  | sed "s/-/,/g"`    
    echo -n $j "," $l ","
    #gawk '{if ($1=="IF_NumRes:") {N=$2}  else if ($1=="IF_pLDDT") {I=$2}   else {print N ","  I "," $2}}' $k/$j.pLDDT
    #grep Summary: $k/$j.pLDDT | sed "s/\[//g" | sed "s/\]//g" | sed "s/Summary://g"
    gawk '{if ($1=="IF_NumRes:") {N=$2}  
    	 else if ($1=="IF_pLDDT") {I=$2}   
    	 else if ($1=="pLDDT") {p=$2}   
    	 else if ($1=="Summary:") {print N ","  I ","  p }}'    $i
done |sed "s/ //g" >> data/Marks-pLDDT.csv

echo "Name,id1,id2,NumRes,IF_plDDT,plDDT" > data/Marks-overlap.csv
find ./marks/ -size 0 -exec rm {} \;
for i in marks/*/*.overlap
do
    j=`basename $i .overlap`
    l=`echo $j  | sed "s/-/,/g"`    
    echo -n $j "," $l ","
    gawk '{if ($1=="IF_NumRes:") {N=$2}  
    	 else if ($1=="IF_pLDDT") {I=$2}   
    	 else if ($1=="pLDDT") {p=$2}   
    	 else if ($1=="Summary:") {print N ","  I ","  p }}'    $i
done |sed "s/ //g" >> data/Marks-overlap.csv




echo "Name,id1,id2,NumRes,IF_plDDT,plDDT" > data/random-pLDDT.csv
find ./random/ -size 0 -exec rm {} \;
for i in random/*/*.pLDDT
do
    j=`basename $i .pLDDT`
    l=`echo $j  | sed "s/-/,/g"`    
    echo -n $j "," $l ","
    #gawk '{if ($1=="IF_NumRes:") {N=$2}  else if ($1=="IF_pLDDT") {I=$2}   else {print N ","  I "," $2}}' $k/$j.pLDDT
    #grep Summary: $k/$j.pLDDT | sed "s/\[//g" | sed "s/\]//g" | sed "s/Summary://g"
    gawk '{if ($1=="IF_NumRes:") {N=$2}  
    	 else if ($1=="IF_pLDDT") {I=$2}   
    	 else if ($1=="pLDDT") {p=$2}   
    	 else if ($1=="Summary:") {print N ","  I ","  p }}'    $i
done |sed "s/ //g" >> data/random-pLDDT.csv


for i in pdb/*/*.overlap
do
    k=`dirname $i`
    j=`basename $k`
    l=`echo $j  | sed "s/-[0-9]-/-/g" | sed "s/-10-/-/g" | sed "s/-[0-9]$//g" | sed "s/-10$//g"`
    echo -n $l ","
    #gawk '{if ($1=="IF_NumRes:") {N=$2}  else if ($1=="IF_pLDDT") {I=$2}   else {print N ","  I "," $2}}' $k/$j.pLDDT
    #grep Summary: $k/$j.pLDDT | sed "s/\[//g" | sed "s/\]//g" | sed "s/Summary://g"
    gawk '{if ($1=="IF_NumRes:") {N=$2}  
    	 else if ($1=="IF_pLDDT") {I=$2}   
    	 else if ($1=="pLDDT") {p=$2}   
    	 else if ($1=="Summary:") {print N ","  I ","  p  }}'    $k/$j.overlap 
done |sed "s/ //g" >  data/overlap.csv

echo "Name,NumRes,IF_plDDT,plDDT" >  data/negatome-overlap.csv
find ./negatome-pLDDT/ -size 0 -exec rm {} \;
for i in negatome-pLDDT/*.overlap
do
    j=`basename $i .overlap`
    l=`echo $j  | sed "s/-[0-9]-/-/g" | sed "s/-10-/-/g" | sed "s/-[0-9]$//g" | sed "s/-10$/_/g"`
    echo -n $l ","
    #gawk '{if ($1=="IF_NumRes:") {N=$2}  else if ($1=="IF_pLDDT") {I=$2}   else {print N ","  I "," $2}}' $k/$j.pLDDT
    #grep Summary: $k/$j.pLDDT | sed "s/\[//g" | sed "s/\]//g" | sed "s/Summary://g"
    gawk '{if ($1=="IF_NumRes:") {N=$2}  
    	 else if ($1=="IF_pLDDT") {I=$2}   
    	 else if ($1=="pLDDT") {p=$2}   
    	 else if ($1=="Summary:") {print N ","  I ","  p }}'    $i
done |sed "s/ //g" >>  data/negatome-overlap.csv

echo "Name,id1,id2,NumRes,IF_plDDT,plDDT" >  data/HuRI-overlap.csv
find ./HuRI/ -size 0 -exec rm {} \;
for i in HuRI/*/*.overlap
do
    j=`basename $i .overlap`
    l=`echo $j  | sed "s/-/,/g"`    
    echo -n $j "," $l ","
    #gawk '{if ($1=="IF_NumRes:") {N=$2}  else if ($1=="IF_pLDDT") {I=$2}   else {print N ","  I "," $2}}' $k/$j.pLDDT
    #grep Summary: $k/$j.pLDDT | sed "s/\[//g" | sed "s/\]//g" | sed "s/Summary://g"
    gawk '{if ($1=="IF_NumRes:") {N=$2}  
    	 else if ($1=="IF_pLDDT") {I=$2}   
    	 else if ($1=="pLDDT") {p=$2}   
    	 else if ($1=="Summary:") {print N ","  I ","  p }}'    $i
done |sed "s/ //g" >>  data/HuRI-overlap.csv


bin/mergecsv.py
