#!/bin/bash -x
#SBATCH -A SNIC2021-5-297
#SBATCH --output=out/jh%j.out
#SBATCH --error=err/jh%j.err
#SBATCH --array=1-1000
#SBATCH -c 8
#SBATCH -t 01:00:00

LIST=$1
OFFSET=$2
FOLDER=MSAs/

POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
ID=`tail -n+$POS $LIST | head -n 1`

COMMON=/proj/nobackup/snic2019-35-62/

UNIPROT=$COMMON/Database/uniprot/uniprot.fasta
UNIREF90=$COMMON/Database/uniref90/uniref90.fasta
MGNIFY=$COMMON/Database/mgnify/mgy_clusters.fa

echo 'Processing ' $ID '...'
ml singularity/3.7.1
singularity exec --bind $COMMON:$COMMON \
	$COMMON/gpozzati/AF2-multimer-mod/AF_data/alphafold-multimer.sif \
		python3 $COMMON/gpozzati/AF2-multimer-mod/alphafold/run_jackHMMer_mod.py \
			--fasta seq/${ID}.fasta \
			--outpath $FOLDER/ \
			--uniref_database_path $UNIREF90 \
			--mgnify_database_path $MGNIFY \
			--uniprot_database_path $UNIPROT
			
