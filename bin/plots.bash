#!/bin/bash -x


#First we add labels




convert -pointsize 200 -gravity NorthWest -annotate +50+50 "A" ~/git/huintaf2/plots/pDockQ-violin.png ~/git/huintaf2/plots/pDockQ-violin-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "B" ~/git/huintaf2/plots/FracDiso-violin.png ~/git/huintaf2/plots/FracDiso-violin-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "C" ~/git/huintaf2/plots/Length-violin.png ~/git/huintaf2/plots/Length-violin-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "D" ~/git/huintaf2/plots/FracDiso-pDockQ.png ~/git/huintaf2/plots/FracDiso-pDockQ-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "D" ~/git/huintaf2/plots/Meff-log-violin.png ~/git/huintaf2/plots/Meff-log-violin-annotate.png

convert -pointsize 200 -gravity NorthWest -annotate +50+50 "A" ~/git/huintaf2/plots/PdockQ-datasets.png ~/git/huintaf2/plots/pDockQ-datasets-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "B" ~/git/huintaf2/plots/PdockQ-datasets2.png ~/git/huintaf2/plots/pDockQ-datasets2-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "C" ~/git/huintaf2/plots/FracDiso-datasets.png ~/git/huintaf2/plots/FracDiso-datasets-annotate.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "C" ~/git/huintaf2/plots/FracDiso-pDockQ.png ~/git/huintaf2/plots/FracDiso-pDockQ-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "D" ~/git/huintaf2/plots/Length-datasets.png ~/git/huintaf2/plots/Length-datasets-annotate.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "D" ~/git/huintaf2/plots/Length-pDockQ.png ~/git/huintaf2/plots/Length-pDockQ-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "D" ~/git/huintaf2/plots/Meff-log-datasets.png ~/git/huintaf2/plots/Meff-log-datasets-annotate.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "D" ~/git/huintaf2/plots/Log-Meff-pDockQ.png ~/git/huintaf2/plots/Log-Meff-pDockQ-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "D" ~/git/huintaf2/plots/Meff-log-datasets.png ~/git/huintaf2/plots/Meff-log-datasets-annotate2.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "F" ~/git/huintaf2/plots/SubCell-datasets.png ~/git/huintaf2/plots/SubCell-datasets-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "F" ~/git/huintaf2/plots/SubCell-datasets.png ~/git/huintaf2/plots/SubCell-datasets-annotate2.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "E" ~/git/huintaf2/plots/Subcell-pDockQ.png ~/git/huintaf2/plots/Subcell-pDockQ-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/CoExpress-datasets.png ~/git/huintaf2/plots/CoExpress-datasets-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "F" ~/git/huintaf2/plots/CoExpress-datasets.png ~/git/huintaf2/plots/CoExpress-datasets-annotate2.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "F" ~/git/huintaf2/plots/coexpression-pDockQ.png ~/git/huintaf2/plots/coexpression-pDockQ-annotate.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/string-pDockQ.png ~/git/huintaf2/plots/string-pDockQ-annotate.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "H" ~/git/huintaf2/plots/string-pDockQ.png ~/git/huintaf2/plots/string-pDockQ-annotate2.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/gtex-pDockQ.png ~/git/huintaf2/plots/gtex-pDockQ-annotate.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/cooccurence-pDockQ.png ~/git/huintaf2/plots/cooccurence-pDockQ-annotate.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/string_experiments-pDockQ.png ~/git/huintaf2/plots/string_experiments-pDockQ-annotate.png
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/string_database-pDockQ.png ~/git/huintaf2/plots/string_database-pDockQ-annotate.png

convert -pointsize 200 -gravity NorthWest -annotate +50+50 "F" ~/git/huintaf2/plots/Subcell-violin.png ~/git/huintaf2/plots/Subcell-violin-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "F" ~/git/huintaf2/plots/Subcell-violin2.png ~/git/huintaf2/plots/Subcell-violin2-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/STRING-violin.png ~/git/huintaf2/plots/STRING-violin-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/string-violin.png ~/git/huintaf2/plots/string-violin-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/coExpress-violin.png ~/git/huintaf2/plots/coExpress-violin-annotate.png
convert -pointsize 200 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/coExpress2-violin.png ~/git/huintaf2/plots/coExpress2-violin-annotate.png


# Using "labels"

montage -tile 2x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -label "A" ~/git/huintaf2/plots/pDockQ-violin.png -label "B" ~/git/huintaf2/plots/FracDiso-violin.png -label "C" ~/git/huintaf2/plots/Length-violin.png -label "D" ~/git/huintaf2/plots/FracDiso-pDockQ.png ~/git/huintaf2/plots/Figure-comparison.png
montage -tile 2x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -label "A" ~/git/huintaf2/plots/pDockQ-violin.png -label "B" ~/git/huintaf2/plots/FracDiso-violin.png -label "C" ~/git/huintaf2/plots/Length-violin.png -label "D" ~/git/huintaf2/plots/Meff-log-violin.png ~/git/huintaf2/plots/Figure-comparison2.png
montage -tile 3x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 -label "A" ~/git/huintaf2/plots/pDockQ-violin.png -label "B" ~/git/huintaf2/plots/FracDiso-violin.png -label "C" ~/git/huintaf2/plots/Length-violin.png -label "D" ~/git/huintaf2/plots/Meff-log-violin.png -label "E" ~/git/huintaf2/plots/Subcell-violin.png -label "F" ~/git/huintaf2/plots/STRING-violin.png ~/git/huintaf2/plots/Figure-comparison3.png

# Using annotated figures

montage -tile 2x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+100+0  ~/git/huintaf2/plots/pDockQ-violin-annotate.png  ~/git/huintaf2/plots/FracDiso-violin-annotate.png  ~/git/huintaf2/plots/Length-violin-annotate.png  ~/git/huintaf2/plots/FracDiso-pDockQ-annotate.png ~/git/huintaf2/plots/Figure-comparison-annotate.png
montage -tile 2x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+100+0  ~/git/huintaf2/plots/pDockQ-violin-annotate.png  ~/git/huintaf2/plots/FracDiso-violin-annotate.png  ~/git/huintaf2/plots/Length-violin-annotate.png  ~/git/huintaf2/plots/Meff-log-violin-annotate.png ~/git/huintaf2/plots/Figure-comparison2-annotate.png
montage -tile 3x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+100+0 ~/git/huintaf2/plots/pDockQ-violin-annotate.png   ~/git/huintaf2/plots/FracDiso-violin-annotate.png  ~/git/huintaf2/plots/Length-violin-annotate.png  ~/git/huintaf2/plots/Meff-log-violin-annotate.png  ~/git/huintaf2/plots/Subcell-violin-annotate.png ~/git/huintaf2/plots/string-violin-annotate.png ~/git/huintaf2/plots/Figure-comparison3-annotate.png

# new
montage -tile 3x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 ~/git/huintaf2/plots/pDockQ-violin-annotate.png   ~/git/huintaf2/plots/FracDiso-violin-annotate.png ~/git/huintaf2/plots/Length-violin-annotate.png  ~/git/huintaf2/plots/Meff-log-violin-annotate.png  ~/git/huintaf2/plots/Subcell-violin2-annotate.png ~/git/huintaf2/plots/coExpress2-violin-annotate.png ~/git/huintaf2/plots/Figure-new-annotate.png


# new
montage -tile 3x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 ~/git/huintaf2/plots/pDockQ-datasets-annotate.png   ~/git/huintaf2/plots/FracDiso-datasets-annotate.png ~/git/huintaf2/plots/Length-datasets-annotate.png  ~/git/huintaf2/plots/Meff-log-datasets-annotate.png  ~/git/huintaf2/plots/SubCell-datasets-annotate.png ~/git/huintaf2/plots/CoExpress-datasets-annotate.png ~/git/huintaf2/plots/Figure-kde-annotate.png


montage -tile 3x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 ~/git/huintaf2/plots/pDockQ-datasets-annotate.png   ~/git/huintaf2/plots/FracDiso-datasets-annotate.png ~/git/huintaf2/plots/Meff-log-datasets-annotate2.png  ~/git/huintaf2/plots/SubCell-datasets-annotate2.png ~/git/huintaf2/plots/CoExpress-datasets-annotate2.png  ~/git/huintaf2/plots/string-pDockQ-annotate.png  ~/git/huintaf2/plots/Figure-kde-annotate2.png

montage -tile 3x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 ~/git/huintaf2/plots/pDockQ-datasets-annotate.png   ~/git/huintaf2/plots/FracDiso-datasets-annotate.png ~/git/huintaf2/plots/Meff-log-datasets-annotate2.png  ~/git/huintaf2/plots/Subcell-pDockQ-annotate.png ~/git/huintaf2/plots/coexpression-pDockQ-annotate.png  ~/git/huintaf2/plots/string-pDockQ-annotate.png  ~/git/huintaf2/plots/Figure-kde-annotate3.png


montage -tile 4x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 ~/git/huintaf2/plots/pDockQ-datasets-annotate.png   ~/git/huintaf2/plots/FracDiso-pDockQ-annotate.png ~/git/huintaf2/plots/Log-Meff-pDockQ-annotate.png  ~/git/huintaf2/plots/Subcell-pDockQ-annotate.png ~/git/huintaf2/plots/coexpression-pDockQ-annotate.png  ~/git/huintaf2/plots/gtex-pDockQ-annotate.png  ~/git/huintaf2/plots/string_database-pDockQ-annotate.png  ~/git/huintaf2/plots/string-pDockQ-annotate2.png   ~/git/huintaf2/plots/Figure-kde-annotate4.png

montage -tile 4x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 ~/git/huintaf2/plots/pDockQ-datasets-annotate.png  ~/git/huintaf2/plots/pDockQ-datasets2-annotate.png   ~/git/huintaf2/plots/FracDiso-pDockQ-annotate.png ~/git/huintaf2/plots/Log-Meff-pDockQ-annotate.png  ~/git/huintaf2/plots/Subcell-pDockQ-annotate.png ~/git/huintaf2/plots/coexpression-pDockQ-annotate.png  ~/git/huintaf2/plots/gtex-pDockQ-annotate.png   ~/git/huintaf2/plots/string-pDockQ-annotate2.png   ~/git/huintaf2/plots/Figure-kde-annotate5.png



# SVG

convert -pointsize 100 -gravity NorthWest -annotate +50+50 "A" ~/git/huintaf2/plots/PdockQ-datasets.svg ~/git/huintaf2/plots/pDockQ-datasets-annotate.eps
convert -pointsize 100 -gravity NorthWest -annotate +50+50 "B" ~/git/huintaf2/plots/PdockQ-datasets2.svg ~/git/huintaf2/plots/pDockQ-datasets2-annotate.eps
convert -pointsize 50 -gravity NorthWest -annotate +50+50 "C" ~/git/huintaf2/plots/FracDiso-pDockQ.svg ~/git/huintaf2/plots/FracDiso-pDockQ-annotate.eps
convert -pointsize 50 -gravity NorthWest -annotate +50+50 "D" ~/git/huintaf2/plots/Log-Meff-pDockQ.svg ~/git/huintaf2/plots/Log-Meff-pDockQ-annotate.eps
convert -pointsize 50 -gravity NorthWest -annotate +50+50 "E" ~/git/huintaf2/plots/Subcell-pDockQ.svg ~/git/huintaf2/plots/Subcell-pDockQ-annotate.eps
convert -pointsize 50 -gravity NorthWest -annotate +50+50 "F" ~/git/huintaf2/plots/coexpression-pDockQ.svg ~/git/huintaf2/plots/coexpression-pDockQ-annotate.eps
convert -pointsize 50 -gravity NorthWest -annotate +50+50 "G" ~/git/huintaf2/plots/gtex-pDockQ.svg ~/git/huintaf2/plots/gtex-pDockQ-annotate.eps
convert -pointsize 50 -gravity NorthWest -annotate +50+50 "H" ~/git/huintaf2/plots/string-pDockQ.svg ~/git/huintaf2/plots/string-pDockQ-annotate2.eps


montage -tile 4x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 ~/git/huintaf2/plots/pDockQ-datasets-annotate.eps  ~/git/huintaf2/plots/pDockQ-datasets2-annotate.eps   ~/git/huintaf2/plots/FracDiso-pDockQ-annotate.eps ~/git/huintaf2/plots/Log-Meff-pDockQ-annotate.eps  ~/git/huintaf2/plots/Subcell-pDockQ-annotate.eps ~/git/huintaf2/plots/coexpression-pDockQ-annotate.eps  ~/git/huintaf2/plots/gtex-pDockQ-annotate.eps   ~/git/huintaf2/plots/string-pDockQ-annotate2.eps   ~/git/huintaf2/plots/Figure2-4x2.eps

montage -tile 3x2 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 ~/git/huintaf2/plots/pDockQ-datasets-annotate.eps  ~/git/huintaf2/plots/pDockQ-datasets2-annotate.eps   ~/git/huintaf2/plots/FracDiso-pDockQ-annotate.eps ~/git/huintaf2/plots/Log-Meff-pDockQ-annotate.eps  ~/git/huintaf2/plots/Subcell-pDockQ-annotate.eps ~/git/huintaf2/plots/coexpression-pDockQ-annotate.eps    ~/git/huintaf2/plots/Figure2.eps



# supplementare venn.diagram
convert -pointsize 150 -gravity NorthWest -annotate +150+150 "A pDockQ > 0.23"  ../plots/venn-huri-humap-pDockQ-023.png   ../plots/venn-huri-humap-pDockQ-023-annotate.png 
convert -pointsize 150 -gravity NorthWest -annotate +150+150 "B pDockQ > 0.5 "  ../plots/venn-huri-humap-pDockQ-05.png   ../plots/venn-huri-humap-pDockQ-05-annotate.png 
convert -pointsize 150 -gravity NorthWest -annotate +150+150 "C Structure "  ../plots/venn-huri-humap-struct.png   ../plots/venn-huri-humap-struct-annotate.png 

montage  -tile 3x1 -density 600 -compress lzw -pointsize 120 -geometry 2048x2048+200+0 ../plots/venn-huri-humap-pDockQ-023-annotate.png ../plots/venn-huri-humap-pDockQ-05-annotate.png ../plots/venn-huri-humap-struct-annotate.png ../plots/venn-supplementary.png
