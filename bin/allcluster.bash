#!/bin/bash -x


for i in `gawk -F"," '{print $1}' data/edges-pred.csv | sort -u | grep -v Clust`
do
    grep -w $i data/edges-pred.csv | gawk -F "," '{print $2}' > cluster-$i.txt
done

wc cluster-*.txt | grep -vE " 1.*1 " | gawk '{print $4}' | grep -v total  > bigclusters.txt


for c in `cat bigclusters.txt`
do
    j=`basename $c .txt`
    mkdir -p $j
    unset line
    for i in `cat $c`
    do
	line=`echo -n $line " " $i `
	cp pdb/${i}/${i}.pdb $j/
    done
    echo $line > $j/${j}-files.txt
    ls 
    bin/makecomplex.py --mintm 0.8 -i $j/*pdb -o $j/${j}_mintm08.pdb
    # Some cleaning up
    rm $j/*_[A-Za-z].pdb
    rm $j/*_[A-Za-z].rot
    rm $j/*_[A-Za-z].MM
done
