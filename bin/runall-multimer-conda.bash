#!/bin/bash -x


export NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'
export CUDA_VISIBLE_DEVICES='all'
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda/lib64
#export TF_CPP_MIN_LOG_LEVEL=0
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/software/sse/manual/CUDA/11.4.2_470.57.02/lib64/
#export XLA_FLAGS=--xla_gpu_cuda_data_dir=$CUDA_PATH


# Trying to automatize AF2 runs

#dir=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/
dir=./
fddir=/proj/berzelius-2021-29/users/x_arnel/git/fd-multimer/


if [ $# == 1 ]
then
    list=`echo "$@" | tr " " "\n" ` 
else
    list=$@
fi
echo $list
    
#name1=`basename $seq1 .fasta`
#name2=`basename $seq2 .fasta`

FUSEDMSA=merged/
PAIREDMSA=merged/
FASTAFILE=merged/
OUTFOLDER=pdb-multi/ # Path where AF2 generates its output folder structure

length=0
for var in $list
do
    SEQ=${SEQ}" "seq/${var}.fasta
    A3M=${A3M},a3m/${var}.a3m
    NAME=${NAME}${var}-
    FUSEDMSA=${FUSEDMSA}${var}-
    PAIREDMSA=${PAIREDMSA}${var}-
    FASTAFILE=${FASTAFILE}${var}-
    length=$((${length} + `${dir}/bin/seqlen.bash seq/${var}.fasta` ))
    CB=${CB},$length
    FASTANAME=${FASTANAME}\ seq/${var}.fasta 
done

DIR=`echo $NAME | sed "s/\-$//g"`

CB=`echo $CB | sed "s/^\,//g"`
CB=`echo $CB | sed "s/\,[0-9]*$//g"`
A3M=`echo $A3M | sed "s/^\,//g"`
#NAME=`echo $NAME | sed "s/\-$/.fasta/g"`
NAME=`echo $NAME | sed "s/\-$//g"`
FASTAFILE=`echo $FASTAFILE | sed "s/\-$/.fasta/g"`
FUSEDMSA=`echo $FUSEDMSA | sed "s/\-$/_fused.a3m/g"`
PAIREDMSA=`echo $PAIREDMSA | sed "s/\-$/_paired.a3m/g"`

echo $A3M
echo $FUSEDMSA
echo $PAIREDMSA
echo $CB
echo $FASTANAME
echo $FASTAFILE

MGF=0.9

# if both does not exist exit

if [ ! -f $FASTAFILE  ]
then
    ${dir}/bin/merge_fasta.bash ${SEQ} > $FASTAFILE
fi


if [ ! -f $PAIREDMSA ]
then
	python3 ${fddir}/src/pair_msas.py --a3m_files $A3M --max_gap_fraction $MGF --outname $PAIREDMSA
fi
if [ ! -f $FUSEDMSA ]
then
    python3 ${fddir}/src/fuse_msas.py --a3m_files $A3M --max_gap_fraction $MGF --outname $FUSEDMSA
fi


#CB=`${dir}/bin/seqlen.bash seq/${name1}.fasta`

if [ ! -f $FASTAFILE  ]
then
    ${dir}/bin/merge_fasta.bash $FASTANAME > $FASTAFILE
fi

MSAS="$FUSEDMSA,$PAIREDMSA" #Comma separated list of msa paths


COMMON='/proj/berzelius-2021-29/'
AFHOME=$COMMON'/users/x_arnel/git/FoldDock/src/alphafold/'
#AFHOME=${fddir}/src/alphafold/ # Path of alphafold directory in FoldDock \
#SINGULARITY=/proj/berzelius-2021-29/singularity_images/alphafold-multimer.sif
SINGULARITY=$COMMON'/singularity_images/alphafold.sif'
PARAM=$COMMON'/Database/af_params/'  # PARAM=/proj/berzelius-2021-29/Database/af_params/  #Path to AF2 params \


PRESET='full_dbs' #Choose preset model configuration - no ensembling (full_dbs) and (reduced_dbs) or 8 model ensemblings (casp14). \
MAX_RECYCLES=10 #max_recycles (default=3) \
MODEL_NAME='model_1'
#MODEL_NAME='model_1_ptm,model_2_ptm,model_3_ptm,model_4_ptm,model_5_ptm'
COMMON=/proj/

#NAME=${name1}-${name2}
#pushd $AFHOME
#cd $AFHOME
#NAME="./" # to not run this part

if [ ! -d $OUTFOLDER/$NAME ]
then
    mkdir $OUTFOLDER/$NAME
    module load Anaconda/2021.05-nsc1
    # conda init bash
    conda activate /proj/berzelius-2021-29/users/x_arnel/.conda/envs/af_server/ 
    #singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
    python3 $AFHOME/run_alphafold.py \
	    --fasta_paths=$FASTAFILE \
	    --msas=$MSAS \
	    --chain_break_list=$CB \
	    --output_dir=$OUTFOLDER \
	    --model_names=$MODEL_NAME \
	    --data_dir=$PARAM \
            --fold_only \
	    --preset=$PRESET \
	    --max_recycles=$MAX_RECYCLES
    #popd

    #--model_names=$MODEL_NAME \

fi
