#!/bin/bash


#  [ -s ${dir}/msas/A/pdb_hits.sto ] &&

complete=$(mktemp complete.XXXXXX.txt)
missing=missing.txt
incomplete=$(mktemp incomplete.XXXXXX.txt)
rm -f ${missing}
touch ${missing}
for dir in "$@"
do
    if [ -s ${dir}/msas/A/bfd_uniref_hits.a3m ] && 	     [ -s ${dir}/msas/A/mgnify_hits.sto ] &&	         [ -s ${dir}/msas/A/uniprot_hits.sto ] &&	     [ -s ${dir}/msas/A/uniref90_hits.sto ]
    then
	echo $dir >>  ${complete}
    else
	echo $dir >>  ${incomplete}
	if [ ! -s ${dir}/msas/A/bfd_uniref_hits.a3m ]
	then
	    echo  ${dir}/msas/A/bfd_uniref_hits.a3m >> ${missing}
	fi
	if [ ! -s ${dir}/msas/A/mgnify_hits.sto ]
	then
	    echo  ${dir}/msas/A/mgnify_hits.sto >> ${missing}
	fi
	if [ ! -s ${dir}/msas/A/pdb_hits.sto ]
	then
	    echo  ${dir}/msas/A/pdb_hits.sto >> ${missing}
	fi
	if [ ! -s ${dir}/msas/A/uniprot_hits.sto ] 
	then
	    echo  ${dir}/msas/A/uniprot_hits.sto >> ${missing}
	fi
	if [ ! -s ${dir}/msas/A/uniref90_hits.sto ]	
	then
	    echo [ -s ${dir}/msas/A/uniref90_hits.sto   >> ${missing}
	fi

    fi
done

gawk -F "/" '{print $2}' ${complete} | sort -u > complete.txt
gawk -F "/" '{print $2}' ${incomplete} | sort -u > incomplete.txt

rm ${complete}
rm ${incomplete}
