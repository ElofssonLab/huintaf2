#!/bin/bash -x

dir=`dirname $1`;
name=`basename $1 .pdb`


for i in 2 3 4 
do
    if [ ! -f ${dir}/${name}-$i.csv ]
    then
	~/git/huintaf2/bin/pDockQ.py -v -p $1 -C $i >   ${dir}/${name}-$i.csv
    fi
done

    
