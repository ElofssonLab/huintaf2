#!/bin/bash -lx
#SBATCH -A NAISS2023-5-249
#SBATCH -J MSA
#SBATCH -p shared
#SBATCH -t 4:00:00
#SBATCH -c 2 # Some jobs run out of memory
##SBATCH --mem=[XXXX]MB
#SBATCH --output=log/msa_jackhmmer_%A_%a.out
#SBATCH --error=log/msa_jackhmmer_%A_%a.error
#SBATCH --array=1-500

#ml PDC/21.11
#ml parallel/20210922

export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

if [ -z $2 ]
then
        offset=0
else
        offset=$2
fi

abspath=`pwd`

#ID_FILE=$abspath/git/ab-benchmark/data/summary/ids429_split_TEST.tsv
ID_FILE=$1
POS=$(($SLURM_ARRAY_TASK_ID + $offset))
ID=$(sed -n ${POS}p $ID_FILE)

echo $ID # example: 7bdc_A

#protID=${ID::-2}  # everything but last two letters, example: 7bdc
#chainID=${ID:0-1} # select last letter, example: A 
protID=`basename ${ID} .fasta`
chainID="A"



## run jackhmmer
SECONDS=0
ncores=2
echo "number of cores: ${ncores}"
bash ${abspath}/bin/dardel_jackhmmer.sh ${protID} ${chainID} ${ncores}
duration=$SECONDS
echo "Elapsed Time: $SECONDS seconds"
echo "$(($SECONDS/3600))h $((($SECONDS/60)%60))m $(($SECONDS%60))s"

