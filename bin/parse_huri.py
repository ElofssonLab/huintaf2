#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


df_map=pd.read_csv("../data/ensgenemap.tsv",sep="\t")

    
# In[3]:


df_huri=pd.read_csv("../data/HI-union.tsv",names=["id1","id2"],sep="\t")


# In[4]:


df_interactions=pd.read_csv("../data/evaluation.csv")


# In[5]:


tempdf=pd.merge(df_map,df_interactions,how="inner",right_on=["id1"],left_on=["proteinID"])
newdf=pd.merge(df_map,tempdf,how="inner",right_on=["id2"],left_on=["proteinID"])


# In[6]:


#newdf.to_csv("merged-evaluation.csv")


# In[7]:


newdf


# In[8]:


newdf["key1"]=newdf.name2_x+"-"+newdf.name2_y
newdf["key2"]=newdf.name2_y+"-"+newdf.name2_x


# In[15]:


df_huri["key1"]=df_huri.id1+"-"+df_huri.id2
df_huri["key2"]=df_huri.id2+"-"+df_huri.id1


# In[10]:


df_match1=pd.merge(df_huri,newdf,on="key1",how="inner")
df_match2=pd.merge(df_huri,newdf,on="key2",how="inner")
df_match=pd.concat([df_match1,df_match2])
df_match


# In[19]:


newdf=df_match[["Name","id1_y","id2_y","id1_x","id2_x"]].drop_duplicates()


# In[20]:


newdf.to_csv("../data/merged_evaluation.csv")


# In[ ]:




