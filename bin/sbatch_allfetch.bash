#!/bin/bash -x
#SBATCH -A berzelius-2021-29
#SBATCH --output=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/out/%j.out
#SBATCH --error=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/err/%j.err
#SBATCH --array=1-50
#SBATCH -c 1
#SBATCH --gpus-per-task=0  
#SBATCH -t 08:00:00

cd /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/

#bin/run_fetch_random.bash

#bin/run_fetch.bash
#bin/run_fetch_negatome.bash
#bin/run_overlap.bash
#bin/run_overlap_negatome.bash
#
#
#bin/run_fetch_CORUM.bash
#bin/run_overlap_CORUM.bash
#
#
#
cd /proj/berzelius-2021-29/users/x_arnel/
bin/run_fetch_HuRI.bash
bin/run_overlap_HuRI.bash

