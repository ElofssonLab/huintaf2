#!/bin/bash -x

rsync --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/pdb/ /scratch5/arnee/HuMap2/pdb/


rsync --exclude  "out/*" --exclude "*msas" --exclude "*.CD*" --exclude "*.cdhit" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/AF-MULTI/ /scratch5/arnee/HuMap2/AF-MULTI/

rsync --exclude  "out/*" --exclude "*msas" --exclude "*.CD*" --exclude "*.cdhit" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/AF-MULTI2/ /scratch5/arnee/HuMap2/AF-MULTI2/

rsync --exclude  "out/*" --exclude "*msas" --exclude "*.CD*" --exclude "*.cdhit" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/AF-MULTI-NOPAIR/ /scratch5/arnee/HuMap2/AF-MULTI-NOPAIR/

rsync --exclude  "out/*" --exclude "*msas" --exclude "*.CD*" --exclude "*.cdhit" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/AF-MULTI-PROK/ /scratch5/arnee/HuMap2/AF-MULTI-PROK/


rsync  --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/negatome/ /scratch5/arnee/HuMap2/negatome/

rsync - --exclude "out/*" --exclude "error/*" -exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/negatome-pLDDT/ /scratch5/arnee/HuMap2/negatome-pLDDT/


rsync  --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/HuRI/ /scratch5/arnee/HuMap2/HuRI/ --exclude "out*"

#rsync  --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/HuRI-plDDT/ /scratch5/arnee/HuMap2/HuRI-plDDT/


rsync  --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/CORUM-pdb/ /scratch5/arnee/HuMap2/CORUM/ 


rsync  --exclude "rewritten*" --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/marks/ /scratch5/arnee/HuMap2/marks/ 


rsync  --exclude "rewritten*" --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/random/ /scratch5/arnee/HuMap2/random/ 

rsync  --exclude "rewritten*" --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/6RGQ// /scratch5/arnee/HuMap2/6RGQ/


rsync --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/single/ /scratch5/arnee/HuMap2/HuMap-single/

rsync --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/single/ /scratch5/arnee/HuMap2/HuRI-single/

rsync --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/humap/ /scratch5/arnee/HuMap2/humap/

rsync --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/gpcrdock/dimers/ /scratch5/arnee/gpcrdock/dimers/

rsync --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Drew/dimers/ /scratch5/arnee/Drew/dimers/

rsync --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Tox-AntiTox//dimers/ /scratch5/arnee/Tox-AntiTox//dimers/


rsync --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/Tox-AntiTox//AF-MULTI/ /scratch5/arnee/Tox-AntiTox//AF-MULTI/


rsync --exclude "out/*" --exclude "error/*" --exclude "*.pkl" -arv x_arnel@berzelius1.nsc.liu.se:/proj/berzelius-2021-29/users/x_arnel/pairs//dimers/ /scratch5/arnee/pairs/dimers/

