#!/usr/bin/env python3


import sys
import numpy as np
import argparse
import re
from Bio.PDB import *
from Bio.PDB.MMCIFParser import MMCIFParser
from Bio.PDB.PDBParser import PDBParser


class NotDisordered(Select):
    def accept_atom(self, atom):
        # return not atom.is_disordered() or atom.get_altloc() == "A"
        return not atom.get_bfactor() < 70


arg_parser = argparse.ArgumentParser(description="Calculates pDockQ from NumRes and IF_plDDT")
group = arg_parser.add_mutually_exclusive_group(required=True)
group.add_argument("-p","--pdb",type=argparse.FileType('r'),help="Input pdb file pLddt values in bfactor columns")
group.add_argument("-c","--cif",type=argparse.FileType('r'),help="Input cif file plddt values in bfactor columns")
arg_parser.add_argument("-C","--cutoff", type=float,required=False,default=70.0,help="pLDDT cutoff to be included in output")
arg_parser.add_argument("-o","--output", type=str,required=True,help="Output CIF file (as alphafold requires CIF files)")
args = arg_parser.parse_args()
cutoff=args.cutoff


if (args.cif):
    name=args.cif.name
    bio_parser = MMCIFParser()
    structure_file = args.cif
    structure_id = args.cif.name[:-4]
    structure = bio_parser.get_structure(structure_id, structure_file)
        
elif (args.pdb):
    Name=args.pdb.name
    bio_parser = PDBParser()
    structure_file = args.pdb
    structure_id = args.pdb.name[:-4]
    structure = bio_parser.get_structure(structure_id, structure_file)


io = MMCIFIO()
io.set_structure(structure)
io.save(args.output, select=NotDisordered())    
    
