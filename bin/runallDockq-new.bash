#!/bin/bash -x

PDB=pdb/
MODELS=$1
IN=$2

#PDB=pdb/
#MODELS=AF-MULTI-PROK/

while I= read -r i j k l n o
do
    if [ ! -f ${MODELS}/${i}/${i}.DockQall2 ]
    then
	touch ${MODELS}/${i}/${i}.DockQall2
	m=`basename $j .pdb`
	bin/matchpdb.py ${MODELS}/${i}/${i}.pdb ${PDB}/${i}/${m}.pdb
	cp  ${PDB}/${i}/${m}.pdb ${PDB}/${i}/${m}_extra.pdb
	bin/matchpdb.py ${PDB}/${i}/${i}_reorder.pdb ${PDB}/${i}/${m}_extra.pdb
	pdb_reres ${PDB}/${i}/${m}_matching.pdb  > ${PDB}/${i}/${m}_renum.pdb 
	pdb_reres ${PDB}/${i}/${m}_extra_matching.pdb > ${PDB}/${i}/${m}_extra_matching_renum.pdb
	pdb_reres ${PDB}/${i}/${i}_matching.pdb >${PDB}/${i}/${i}_matching_renum.pdb
	pdb_reres ${PDB}/${i}/${i}_reorder_matching.pdb >${PDB}/${i}/${i}_reorder_matching_renum.pdb
	#pdb_reres $2 > $B
	#
	if [ -f ${MODELS}/${i}/${i}*.MMall2 ]
	then
	    for p in ${MODELS}/${i}/${i}*.MMall2
	    do
		pdb=`gawk '{print $2}' $p `
		chain=`gawk '{print $3}' $p `
		if [ -f cif/${pdb}_${chain}_rechain.pdb ]
		then
		    cp cif/${pdb}_${chain}_rechain.pdb ${MODELS}/${i}/${m}_mmall.pdb 
		fi
	    done
	fi
	if [ -f cif/${k}_${l}${n}_rechain.pdb ]
	then
	    cp cif/${k}_${l}${n}_rechain.pdb ${MODELS}/${i}/${m}_pdborg.pdb
	fi
	
	for a in ${MODELS}/${i}/${i}*.pdb
	do
	    for b in  ${PDB}/${i}/${m}*.pdb
	    do
		A=`basename $a`
		B=`basename $b`
		python3 ~/git/DockQ/DockQ-mod.py -short -useCA  $a $b > ${MODELS}/${i}/${A}-${B}.DockQall
	    done
	done
	python3 ~/git/DockQ/DockQ-mod.py -short -useCA ${MODELS}/${i}/${i}.pdb ${PDB}/${i}/${m}.pdb > ${MODELS}/${i}/${i}.DockQ 
	python3 ~/git/DockQ/DockQ-mod.py -short -useCA ${MODELS}/${i}/${i}_reorder.pdb ${PDB}/${i}/${m}.pdb > ${MODELS}/${i}/${i}.DockQ.reorder
	cp ${MODELS}/${i}/${i}.DockQ ${MODELS}/${i}/${i}-${m}.DockQall
	cp ${MODELS}/${i}/${i}.DockQ.reorder ${MODELS}/${i}/${i}-${m}_reorder.DockQall
	grep -H DockQ ${MODELS}/${i}/${i}*.DockQall ${MODELS}/${i}/${i}.DockQ ${MODELS}/${i}/${i}.DockQ.reorder | sort -nk2 |tail -1 >  ${MODELS}/${i}/${i}.DockQall2
	
    
    
	# Original and different chain order
	

	~/git/bioinfo-toolbox/trRosetta/MMalign ${MODELS}/${i}/${i}.pdb ${PDB}/${i}/${m}.pdb > ${MODELS}/${i}/${i}.MM
	~/git/bioinfo-toolbox/trRosetta/MMalign ${MODELS}/${i}/${i}_reorder.pdb ${PDB}/${i}/${m}.pdb > ${MODELS}/${i}/${i}.MM.reorder
    fi
    
done <  ${IN}

