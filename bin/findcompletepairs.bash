#!/bin/bash

#  #   [ -s ${dir}/msas/A/pdb_hits.sto ] &&	 [ -s ${dir}/msas/B/pdb_hits.sto ] &&

finished=$(mktemp finished.XXXXXX.txt)
complete=$(mktemp complete.XXXXXX.txt)
incomplete=$(mktemp incomplete.XXXXXX.txt)
for dir in `ls $@`
do
    #echo $dir
    if [ -s ${dir}/msas/A/bfd_uniref_hits.a3m ] && 	     [ -s ${dir}/msas/A/mgnify_hits.sto ] &&	        [ -s ${dir}/msas/A/uniprot_hits.sto ] &&	     [ -s ${dir}/msas/A/uniref90_hits.sto ] && [ -s ${dir}/msas/B/bfd_uniref_hits.a3m ] && 	     [ -s ${dir}/msas/B/mgnify_hits.sto ] &&	         [ -s ${dir}/msas/B/uniprot_hits.sto ] &&	     [ -s ${dir}/msas/B/uniref90_hits.sto ]
    then
	if [ -s ${dir}/ranked_0.pdb ]
	then
	    echo $dir >>  ${finished}
	else
	    echo $dir >>  ${complete}
	fi
    else
	echo $dir >>  ${incomplete}
    fi
done

gawk -F "/" '{print $2}' ${finished} | sort -u > finishedpairs.txt
gawk -F "/" '{print $2}' ${complete} | sort -u > completepairs.txt
gawk -F "/" '{print $2}' ${incomplete} | sort -u > incompletepairs.txt

#rm ${finished}
#rm ${complete}
#rm ${incomplete}
