import os
import sys
import string
import shutil
import argparse
from itertools import combinations

def gen_foldertree(input_fasta, source, target, complex_id):
    msa_dic = {}
    uniq_seqs = []
    letters = list(string.ascii_uppercase)
    for code, seq in input_fasta.items():
        if seq not in uniq_seqs:
            chain_id = letters.pop(0)
            uniq_seqs.append(seq)

        #if code[-1] != chain_id: code = code[:-1]+chain_id
        msapath = source+code+'/msas/A/'
        #print ("TEST",source,code,msapath)
        if  os.path.exists(msapath+'bfd_uniclust_hits.a3m') \
        and os.path.exists(msapath+'uniref90_hits.sto') \
        and os.path.exists(msapath+'uniprot_hits.sto') \
        and os.path.exists(msapath+'mgnify_hits.sto'):
            msa_dic[chain_id] = msapath

        else:
            print (msapath+'bfd_uniclust_hits.a3m',os.path.exists(msapath+'bfd_uniclust_hits.a3m'))
            print (msapath+'uniref90_hits.sto',os.path.exists(msapath+'uniref90_hits.sto'))
            print(msapath+'uniprot_hits.sto',os.path.exists(msapath+'uniprot_hits.sto'))
            print(msapath+'mgnify_hits.sto',os.path.exists(msapath+'mgnify_hits.sto'))
            raise RuntimeError ('Single chain MSAs for chain {}'\
                ' are missing.'.format(source+code))

    print ('{} has {} unique chains in total'.format(complex_id, len(msa_dic)))
    target_path = target+complex_id+'/msas/'
    os.makedirs(target_path, exist_ok=True)
    for msa_id, path in msa_dic.items():
        shutil.copytree(path, target_path+msa_id)
    
    #print (input_fasta)
    #with open(target+complex_id+'.fasta', 'w') as out:
    #    for code, seq in input_fasta.items():
    #        out.write('>{}\n{}\n'.format(code,seq))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description =
    'AF2 multimer folder tree generation, \
        starting from single chain MSAS')
    parser.add_argument('--fasta', required=True,
        help='multi-sequence fasta file')
    parser.add_argument('--sourcepath', required=True,
        help='output folder where AF2 folder structure \
        for single chains have been generated')
    parser.add_argument('--targetpath', required=True,
        help='output folder where AF2 folder structure \
        for multi-chain complexes have to be generated')
    parser.add_argument('--split', required=False, default=None,
        help='specify an integer N; composes one folder \
        substructure for each possible combination of N chains')
    ns = parser.parse_args()

    full_fasta = ''.join(['\n'+line.rstrip()+'\t\t\t' \
        if line.startswith('>') else line.rstrip() \
        for line in open(ns.fasta)])

    full_fasta = {line.split('\t\t\t')[0][1:]:line.split('\t\t\t')[1] \
        for line in full_fasta.split('\n')[1:]}

    source = ns.sourcepath
    target = ns.targetpath
    if source[-1] != '/': source+='/'
    if target[-1] != '/': target+='/'
    fastafile = ns.fasta.rstrip('fasta').rstrip('.').split('/')[-1]


    if ns.split:
        codes = list(full_fasta.keys())
        subcomplexes = list(combinations(codes, int(ns.split)))
        print ('{} subcomplexes generated:'.format(len(subcomplexes)), subcomplexes)
        for group in subcomplexes:
            print ('Setting up ', group, '...')
            complex_id = '-'.join(group)
            sub_fasta = {code:seq for code, seq in full_fasta.items() \
                if code in group}
            gen_foldertree(sub_fasta, source, target, complex_id)

            with open(target+'id_mapping.csv','a') as out:
                out.write(fastafile+','+complex_id+'\n')
    else:
        gen_foldertree(full_fasta, source, target, fastafile)

