#!/usr/bin/env python3

import pandas as pd
import sys
import numpy as np
import argparse
import re


def findclustercenter (x):
    try:
        y=df_merge[df_merge["ch2"]==x]["ch1"].to_list()[0]
    except:
        y=x
    return y

arg_parser = argparse.ArgumentParser(description="Finds conntacts between chains, using two csv files.")
arg_parser.add_argument("-c","--cluster", type=argparse.FileType('r'), required=False,help="Cluster file from MMseqs")
arg_parser.add_argument("-p","--pairs", type=argparse.FileType('r'), required=True,help="CSV file with number of contacts between all chain pairs")
arg_parser.add_argument("-C","--cutoff", type=float,required=False,default=10.0,help="Min Number of contacts between chains")
args = arg_parser.parse_args()
cutoff=args.cutoff


if (args.cluster):
    df_merge=pd.read_csv(args.cluster,sep="\t",names=["Center","Member"])
    df_merge["ch1"]=df_merge["Center"].str.replace("...._","", regex = True)
    df_merge["ch2"]=df_merge["Member"].str.replace("...._","", regex = True)

df_pair=pd.read_csv(args.pairs)

Name=args.pairs.name
Name=re.sub(r'.*/','',Name)
Name=re.sub(r'.contacts.csv$','',Name)

df_pair["Ch1New"]=df_pair["Ch1"].apply(findclustercenter)
df_pair["Ch2New"]=df_pair["Ch2"].apply(findclustercenter)
df_pair["combo"]=Name+"_"+df_pair["Ch1New"]+","+Name+"_"+df_pair["Ch2New"]
pairs=df_pair.groupby("combo",sort=False)["NumContacts"].max()
print ("Chain1,Chain2,Contacts")
for i in pairs.keys():
    if pairs[i]>cutoff:
        print (i+","+str(pairs[i]))
    else:
        print (i+","+str(pairs[i]))
