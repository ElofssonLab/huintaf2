#!/usr/bin/env python3
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import sklearn.metrics as metrics


# In[2]:


from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from matplotlib_venn import venn3,venn2,venn3_circles
import venn

sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
# In[102]:


df=pd.read_csv("../data/evaluation.csv")
df_random=pd.read_csv("../data/random-pLDDT.csv")
df_negpLDDT=pd.read_csv("../data/negatome-pLDDT.csv")
df_negseqlen=pd.read_csv("../data/negatome-seqlen.csv",sep=",",names=["Name","SeqLen1","SeqLen2"])
df_negseqlen["SeqLen"]=df_negseqlen.SeqLen1+df_negseqlen.SeqLen2
df_evidence=pd.read_csv("../data/evidence.csv",sep="\s") #,engine=python)

df_negatome=pd.merge(df_negpLDDT,df_negseqlen,on=["Name"],how="inner")

# Huei
#df_HuRI=pd.read_csv("../data/HuRI-pLDDT.csv")
#df_HuRI_dockq=pd.read_csv("../data/HuRI-DockQ.csv",sep=",",names=["Name","DockQ"])
#print (len(df_HuRI),len(df_HuRI_dockq))
##df_HuRI=pd.merge(df_HuRI,df_HuRI_dockq,on=["Name"],how="outer")
#print (len(df_HuRI),len(df_HuRI_dockq))

#sys.exit

df_HuRI=pd.read_csv("../data/Hurimapping.csv")
#df_hurimap=pd.read_csv("../data/merged_evaluation.csv")
df_hurimap=pd.read_csv("../data/humap_ensembl_id.csv")
df_hurimap["Name"]=df_hurimap.Ensembl_id1+"-"+df_hurimap.Ensembl_id2
a=df_hurimap[["Id1","Ensembl_id1"]]
b=df_hurimap[["Id2","Ensembl_id2"]]
a=a.rename(columns={"Id1":"UniProt","Ensembl_id1":"Ensembl"})
b=b.rename(columns={"Id2":"UniProt","Ensembl_id2":"Ensembl"})
#df_ensmap=pd.concat([a,b]).drop_duplicates()
df_ensmap=pd.read_csv("../data/HUMAN_9606_idmapping_subset.tsv",sep="\s+",names=["UniProt","temp","Ensembl"])
df_ensmap_full=pd.read_csv("../data/HUMAN_9606_idmapping.tsv",sep="\s+",names=["UniProt","temp","Ensembl"])
df_ensmap_full = df_ensmap_full.drop_duplicates(subset=['Ensembl'], keep='first')
df_ensmap=pd.concat([df_ensmap,df_ensmap_full]).drop_duplicates(subset=['Ensembl'], keep='first')
#print (df_ensmap)
#sys.exit()
#df_temp=pd.merge(df_HuRI,df_ensmap,left_on=["id1"],right_on=["Ensembl"],how="outer")
#df_HuRI_all=pd.merge(df_temp,df_ensmap,left_on=["id2"],right_on=["Ensembl"],how="outer")
#df_HuRI=df_HuRI_all.dropna()
#df_HuRI["Name1"]=df_HuRI["UniProt_x"]+"-"+df_HuRI["UniProt_y"]
#df_HuRI["Name2"]=df_HuRI["UniProt_y"]+"-"+df_HuRI["UniProt_x"]
#df_HuRI["SortedName"]=['-'.join(sorted(tup)) for tup in zip(df_HuRI['UniProt_x'], df_HuRI['UniProt_y'])]
df_HuRI["SortedName"]=df_HuRI["Name"]

#df_HuRI_all.to_csv("../data/Hurimapping_full.csv")
#df_HuRI.to_csv("../data/Hurimapping.csv")

#sys.exit()
#
df["SortedName"]=['-'.join(sorted(tup)) for tup in zip(df['id1'], df['id2'])]
df_negatome[["id1","id2"]]=df_negatome["Name"].str.split("-",n=2,expand=True)
# In[109]:
df_negatome["SortedName"]=['-'.join(sorted(tup)) for tup in zip(df_negatome['id1'], df_negatome['id2'])]
df_negatome["SortedName"]=['-'.join(sorted(tup)) for tup in zip(df_negatome['id1'], df_negatome['id2'])]
df_random[["id1","id2"]]=df_random["Name"].str.split("-",n=2,expand=True)
# In[109]:
df_random["SortedName"]=['-'.join(sorted(tup)) for tup in zip(df_random['id1'], df_random['id2'])]
df_random["SortedName"]=['-'.join(sorted(tup)) for tup in zip(df_random['id1'], df_random['id2'])]



#df_negatome


# In[110]:


df["SumLogID_pLDDT"]=df.IF_plDDT*np.log(df.NumRes)
df["SumIF"]=df.NumRes*df.IF_plDDT/100
df_negatome["SumIF"]=df_negatome.NumRes*df_negatome.IF_plDDT/100
df_random["SumIF"]=df_random.NumRes*df_negatome.IF_plDDT/100



# In[111]:


minreses=[1,2,5,10,20,30,50]
minrescols=[]
minIFcols=[]
for i in minreses:
    df["minres-"+str(i)]= np.where(df.NumRes>i, 1, 0)
    df_negatome["minres-"+str(i)]= np.where(df_negatome.NumRes>i, 1, 0)
    minrescols+=["minres-"+str(i)]
    df["IFmin-"+str(i)]=df["IF_plDDT"]*df["minres-"+str(i)]
    df_negatome["IFmin-"+str(i)]=df_negatome["IF_plDDT"]*df_negatome["minres-"+str(i)]
    minIFcols+=["IFmin-"+str(i)]


# In[112]:


df["Struct"]= np.where(df.DockQ.notna(), True, False)
df_struct=df[df.DockQ.notna()]
df_nostruct=df[df.DockQ.isna()]
#df_struct["Good"]= np.where(df_struct.DockQ>0.23, True, False)
#df_struct["TMGood"]= np.where(df_struct.MMall>0.7, True, False)
df_struct["GoodAll"]= np.where(df_struct.DockQ>0.23, True, False)
df_struct["TMGoodAll"]= np.where(df_struct.MMall>0.7, True, False)
df_corr=df_struct[df_struct.GoodAll==True]
df_incorr=df_struct[df_struct.GoodAll==False]



df_ensmap=pd.concat([a,b]).drop_duplicates()
df_ensmap
#df_merged=pd.merge(df,df_hurimap,on=["SortedName"],how="outer")
#df_merged=df_merged.rename(columns={
#    "id1_x":"Gene1",
#    "id2_x":"Gene2",
#    "id1_y":"id1",
#    "id2_y":"id2",
#    "id1_x_x":"Gene1",
#    "id2_x_x":"Gene2",
#    "id1_y_y":"id1",
#    "id2_y_y":"id2",
#})
#df=df_merged


df_evidence=df_evidence.rename(columns={"pairs":"Name"})
df_evidence[["id1","id2"]]=df_evidence["Name"].str.split("-",n=2,expand=True)
df_evidence["SortedName"]=['-'.join(sorted(tup)) for tup in zip(df_evidence['id1'], df_evidence['id2'])]
#print (df_evidence)
df_merged=pd.merge(df,df_evidence,on=["SortedName"],how="inner")
df_merged=df_merged.rename(columns={
    "Name_x":"Name",
    "id1_x":"Gene1",
    "id2_x":"Gene2",
    "id1_y":"id1",
    "id2_y":"id2",
    "id1_x_x":"Gene1",
    "id2_x_x":"Gene2",
    "id1_y_y":"id1",
    "id2_y_y":"id2",
})
#df=df_merged



df=df_merged
# finding homodimers
df["HomoDimer"]=np.where(df.id1==df.id2, True, False)
df_HuRI["HomoDimer"]=np.where(df_HuRI.id1==df_HuRI.id2, True, False)

#df.to_csv("merged.csv")
#print (df)
tiny=1.e-20

# This is from fitting it to a linear model on Marks data
def sigmoid(x, L ,x0, k, b):
    y = L / (1 + np.exp(-k*(x-x0)))+b
    return (y)

popt=[7.07140240e-01, 3.88062162e+02, 3.14767156e-02, 3.13182907e-02]

df["MULT"]=np.log(df.NumRes+tiny)*df.IF_plDDT
df_HuRI["MULT"]=np.log(df_HuRI.NumRes+tiny)*df_HuRI.IF_plDDT
df_negatome["MULT"]=np.log(df_negatome.NumRes+tiny)*df_negatome.IF_plDDT
df_random["MULT"]=np.log(df_random.NumRes+tiny)*df_random.IF_plDDT


# Parameters are from scaling on the marks set
#df["pDockQ"]=sigmoid(df["MULT"],*popt) # Alread in csv
#df_HuRI["pDockQ"]=sigmoid(df_HuRI["MULT"],*popt)
df_negatome["pDockQ"]=sigmoid(df_negatome["MULT"],*popt)
df_random["pDockQ"]=sigmoid(df_random["MULT"],*popt)

# Parameters are from scaling on the marks set
df["pDockQ-lin"]=np.log(df.NumRes+tiny)*df.IF_plDDT*0.0024/2
df_HuRI["pDockQ-lin"]=np.log(df_HuRI.NumRes+tiny)*df_HuRI.IF_plDDT*0.0024/2
df_negatome["pDockQ-lin"]=np.log(df_negatome.NumRes+tiny)*df_negatome.IF_plDDT*0.0024/2
df_random["pDockQ-lin"]=np.log(df_random.NumRes+tiny)*df_random.IF_plDDT*0.0024/2

df["GoodpDockQ"]= np.where(df.pDockQ>0.23, True, False)
df_HuRI["GoodpDockQ"]= np.where(df_HuRI.pDockQ>0.23, True, False)
df_negatome["GoodpDockQ"]= np.where(df_negatome.pDockQ>0.23, True, False)
df_random["GoodpDockQ"]= np.where(df_random.pDockQ>0.23, True, False)


# This is a linear regression model with three paramers - but it does not make it look nicer
#df["pDockQ"]=np.log(df.NumRes+tiny)*df.IF_plDDT*0.00220395+np.log(df.NumRes+tiny)*(-0.02319845)+df.IF_plDDT*0.0083746
#df_HuRI["pDockQ"]=np.log(df_HuRI.NumRes+tiny)*df_HuRI.IF_plDDT*0.00220395+np.log(df_HuRI.NumRes+tiny)*(-0.02319845)+df_HuRI.IF_plDDT*0.0083746
#df_negatome["pDockQ"]=np.log(df_negatome.NumRes+tiny)*df_negatome.IF_plDDT*0.00220395+np.log(df_negatome.NumRes+tiny)*(-0.02319845)+df_negatome.IF_plDDT*0.0083746




#sys.exit()



#df["Gene"]= np.where(df.Gene1.notna(), True, False)
#df_merged
df["Int-Stru"]=np.where(df.interactome=="Structure", True, False)
df["Int-Dom"]=np.where(df.interactome=="Dom_dom_model", True, False)
df["Int-Model"]=np.where(df.interactome=="Model", True, False)
df["Int-NoStr"]=np.where(df.interactome=="0", True, False)
#

#  we need to sort make sure the names of IDs are in correct order




jplot=sns.jointplot(data=df_struct, x="NumRes", y="NumResOverlap",kind="scatter",hue="DockQ")
sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})

#jplot.plot_joint(sns.kdeplot, color="r", zorder=0, levels=6,legend=False)
#sns_plot=sns.jointplot(data=newdf, x=k, y=l,hue="good",kind="kde")
#jplot.ax_joint.set_xticks([0,1,2,3,4])
#jplot.ax_joint.set_xticklabels([1,10,100,1000,10000])
#jplot.ax_joint.set_xlabel("Meff")
jplot.ax_joint.set_xlim([1,1000])
jplot.ax_joint.set_ylim([1,125])
jplot.ax_joint.set_xscale("log")
jplot.ax_joint.set_yscale("log")
jplot.savefig("../plots/NumRes-overlap.png",bbox_inches="tight",dpi=300)
jplot.savefig("../plots/NumRes-overlap.svg",bbox_inches="tight",dpi=300)
plt.close(fig="all")
#sys.exit()

# In[113]:


f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.distplot(df_corr.IF_plDDT,label="PDB-correct", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df_incorr.IF_plDDT,label="PDB-incorrect", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df_nostruct.IF_plDDT,label="HuMap - no PDB", kde_kws=dict(linewidth=3),hist=False)
#sns.distplot(df_negatome.IF_plDDT,label="Negatome", kde_kws=dict(linewidth=3),hist=False)
plt.legend()

plt.savefig("../plots/pLDDT.png",dpi=1200)
plt.savefig("../plots/pLDDT.svg",dpi=1200)


f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.distplot(df_corr.IF_plDDT,label="PDB-correct", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df_incorr.IF_plDDT,label="PDB-incorrect", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df_nostruct.IF_plDDT,label="HuMap - no PDB", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df_HuRI.IF_plDDT,label="HuRI", kde_kws=dict(linewidth=3),hist=False)
#sns.distplot(df_HuRI_all.IF_plDDT,label="HuRI - all", kde_kws=dict(linewidth=3),hist=False)
#sns.distplot(df_negatome.IF_plDDT,label="Negatome", kde_kws=dict(linewidth=3),hist=False)
plt.legend()

plt.savefig("../plots/pLDDT-Huri.png",dpi=1200)
plt.savefig("../plots/pLDDT-Huri.svg",dpi=1200)

f, ax = plt.subplots(figsize=(12., 12.))
sns.distplot(df_HuRI[df_HuRI.HomoDimer].IF_plDDT,label="HomoDimers", kde_kws=dict(linewidth=3),hist=False)
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.distplot(df_HuRI[df_HuRI.HomoDimer==False].IF_plDDT,label="HeteroDimers", kde_kws=dict(linewidth=3),hist=False)
plt.legend()
plt.savefig("../plots/pLDDT-Homodimers.png",dpi=1200)
plt.savefig("../plots/pLDDT-Homodimers.svg",dpi=1200)


f, ax = plt.subplots(figsize=(12., 12.))
sns.distplot(df.IF_plDDT,label="HuMap2.0", kde_kws=dict(linewidth=3),hist=False,color="black")
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
#sns.distplot(df_nogene.IF_plDDT,label="NO-Y2H", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df[df.DockQ.notna()].IF_plDDT,label="Structure", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="red")
sns.distplot(df[df.interactome!="0"].IF_plDDT,label="3Dinteractome", kde_kws=dict(linewidth=3,linestyle="--"),hist=False,color="darkred")
sns.distplot(df[(df.intact_y2h>0)|(df.biogridy2h>0)].IF_plDDT,label="Y2H", kde_kws=dict(linewidth=3,linestyle="-."),hist=False,color="crimson")
sns.distplot(df[(df.cross32>0)|(df.crosstotal>0)].IF_plDDT,label="Cross Linking", kde_kws=dict(linewidth=3,linestyle="dotted"),hist=False,color="tomato")
sns.distplot(df_HuRI.IF_plDDT,label="HuRI", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="green") 
#sns.distplot(df_nostruct.IF_plDDT,label="Interacting", kde_kws=dict(linewidth=3),hist=False)
#sns.distplot(df_negatome.IF_plDDT,label="Negatome", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="blue")
sns.distplot(df_random.IF_plDDT,label="Random", kde_kws=dict(linewidth=5,linestyle="-"),hist=False,color="blue")
plt.legend()
plt.ylim([0,0.05])
plt.xlabel('Interface plDDT')
plt.gcf().subplots_adjust(left=0.18)
plt.savefig("../plots/plDDT-evidence.png",dpi=300)
plt.savefig("../plots/plDDT-evidence.svg",dpi=300)



f, ax = plt.subplots(figsize=(12., 12.))
sns.distplot(df.pDockQ,label="HuMap2.0", kde_kws=dict(linewidth=3),hist=False,color="black")
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
#sns.distplot(df_nogene.pDockQ,label="NO-Y2H", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df[df.DockQ.notna()].pDockQ,label="Structure", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="red")
sns.distplot(df[df.interactome!="0"].pDockQ,label="3Dinteractome", kde_kws=dict(linewidth=3,linestyle="--"),hist=False,color="darkred")
sns.distplot(df[(df.intact_y2h>0)|(df.biogridy2h>0)].pDockQ,label="Y2H", kde_kws=dict(linewidth=3,linestyle="-."),hist=False,color="crimson")
sns.distplot(df[(df.cross32>0)|(df.crosstotal>0)].pDockQ,label="Cross Linking", kde_kws=dict(linewidth=3,linestyle="dotted"),hist=False,color="tomato")
sns.distplot(df_HuRI.pDockQ,label="HuRI", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="green") 
#sns.distplot(df_nostruct.pDockQ,label="Interacting", kde_kws=dict(linewidth=3),hist=False)
#sns.distplot(df_negatome.pDockQ,label="Negatome", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="blue")
sns.distplot(df_random.pDockQ,label="Random", kde_kws=dict(linewidth=5,linestyle="-"),hist=False,color="blue")
plt.legend()
#plt.ylim([0,0.05])
plt.xlabel('pDockQ')
plt.gcf().subplots_adjust(left=0.18)
plt.savefig("../plots/pDockQ-evidence.png",dpi=300)
plt.savefig("../plots/pDockQ-evidence.svg",dpi=300)


tempdf1=df.dropna(subset=["id1","id2","IF_plDDT","NumRes"])[["SortedName","id1","id2","IF_plDDT","NumRes","GoodpDockQ","DockQ","interactome","biogridy2h","intact_y2h","cross32","pDockQ","crosstotal"]]
tempdf2=df_HuRI.dropna(subset=["id1","id2","IF_plDDT","NumRes"])[["SortedName","id1","id2","IF_plDDT","NumRes","GoodpDockQ","DockQ","pDockQ"]]
tempdf2["interactome"]="0"
tempdf2["biogridy2h"]=0
tempdf2["intact_y2h"]=0
tempdf2["cross32"]=0
tempdf2["crosstotal"]=0
tempdf1["Method"]="HuMap"
tempdf2["Method"]="HuRI"
tempdf=pd.concat([tempdf1,tempdf2])


f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.distplot(tempdf[tempdf.Method=="HuMap"].pDockQ,label="HuMap2.0", kde_kws=dict(linewidth=3),hist=False,color="black")
sns.distplot(tempdf[tempdf.Method=="HuRI"].pDockQ,label="HuRI", kde_kws=dict(linewidth=3,linestyle="dotted"),hist=False,color="black") 
#sns.distplot(tempdf_nogene.pDockQ,label="NO-Y2H", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(tempdf[(tempdf.DockQ.notna() )|(tempdf.interactome!="0")].pDockQ,label="Structure", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="red")
#sns.distplot(tempdf[(tempdf.DockQ.notna() )].pDockQ,label="Petras", kde_kws=dict(linewidth=3,linestyle="--"),hist=False,color="red")
#sns.distplot(tempdf[(tempdf.interactome!="0")].pDockQ,label="Interactions", kde_kws=dict(linewidth=3,linestyle=":"),hist=False,color="black")
#sns.distplot(tempdf[tempdf.interactome!="0"].pDockQ,label="3Dinteractome", kde_kws=dict(linewidth=3,linestyle="--"),hist=False,color="darkred")
sns.distplot(tempdf[(tempdf.intact_y2h>0)|(tempdf.biogridy2h>0)].pDockQ,label="Y2H", kde_kws=dict(linewidth=3,linestyle="-."),hist=False,color="red")
sns.distplot(tempdf[(tempdf.cross32>0)|(tempdf.crosstotal>0)].pDockQ,label="Cross Linking", kde_kws=dict(linewidth=3,linestyle="dotted"),hist=False,color="red")
#sns.distplot(tempdf_nostruct.pDockQ,label="Interacting", kde_kws=dict(linewidth=3),hist=False)
#sns.distplot(tempdf_negatome.pDockQ,label="Negatome", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="blue")
sns.distplot(df_random.pDockQ,label="Random", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="blue")
plt.legend()
#plt.ylim([0,0.05])
plt.xlabel('pDockQ')
plt.gcf().subplots_adjust(left=0.18)
plt.savefig("../plots/pDockQ-final.png",dpi=300)
plt.savefig("../plots/pDockQ-final.svg",dpi=300)





f, ax = plt.subplots(figsize=(12., 12.))
sns.distplot(df.NumRes,label="HuMap2.0", kde_kws=dict(linewidth=3),hist=False,color="black")
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
#sns.distplot(df_nogene.NumRes,label="NO-Y2H", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df[df.DockQ.notna()].NumRes,label="Structure", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="red")
sns.distplot(df[df.interactome!="0"].NumRes,label="3Dinteractome", kde_kws=dict(linewidth=3,linestyle="--"),hist=False,color="darkred")
sns.distplot(df[(df.intact_y2h>0)|(df.biogridy2h>0)].NumRes,label="Y2H", kde_kws=dict(linewidth=3,linestyle="-."),hist=False,color="crimson")
sns.distplot(df[(df.cross32>0)|(df.crosstotal>0)].NumRes,label="Cross Linking", kde_kws=dict(linewidth=3,linestyle="dotted"),hist=False,color="tomato")
 
#sns.distplot(df_nostruct.NumRes,label="Interacting", kde_kws=dict(linewidth=3),hist=False)
#sns.distplot(df_negatome.NumRes,label="Negatome", kde_kws=dict(linewidth=3,linestyle="-"),hist=False,color="blue")
sns.distplot(df_random.NumRes,label="Random", kde_kws=dict(linewidth=5,linestyle="-"),hist=False,color="blue")
plt.legend()
plt.xlabel('Interface NumRes')
plt.gcf().subplots_adjust(left=0.18)
plt.savefig("../plots/NumRes-evidence.png",dpi=300)
plt.savefig("../plots/NumRes-evidence.svg",dpi=300)

#sys.exit()

# In[114]:

f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.distplot(df_corr.NumRes,label="PDB-correct", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df_incorr.NumRes,label="PDB-incorrect", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df_nostruct.NumRes,label="NoPDB", kde_kws=dict(linewidth=3),hist=False)
#sns.distplot(df_negatome.NumRes,label="Negatome", kde_kws=dict(linewidth=3),hist=False)
ax.set_xlim([0,500])
plt.legend()
#plt.show()
plt.savefig("../plots/NumRes.png",dpi=1200)
plt.savefig("../plots/NumRes.svg",dpi=1200)


# In[115]:

f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.distplot(df_corr.SumIF,label="PDB-correct", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df_incorr.SumIF,label="PDB-incorrect", kde_kws=dict(linewidth=3),hist=False)
sns.distplot(df_nostruct.SumIF,label="NoPDB", kde_kws=dict(linewidth=3),hist=False)
#sns.distplot(df_negatome.SumIF,label="Negatome", kde_kws=dict(linewidth=3),hist=False)
#ax.set_xscale('log')
ax.set_xlim(0,300)
plt.legend()
#plt.show()
plt.savefig("../plots/SumIF.png",dpi=1200)
plt.savefig("../plots/SumIF.svg",dpi=1200)


# In[129]:


f, ax = plt.subplots(figsize=(12., 12.))
reg = LinearRegression()
cutoff=0.23
tempdf=df_struct.dropna(subset=["IF_plDDT","SumIF","NumRes","SeqLen"])
X=tempdf[["IF_plDDT","SumIF","NumRes","SeqLen"]]
Y=tempdf.DockQall
R=reg.fit(X,Y)
pred=reg.predict(X)
plt.scatter(Y,pred)
err=mean_squared_error(pred,Y)
correct=Y>cutoff            
#fig2.show()
fpr, tpr, threshold = metrics.roc_curve(correct, pred)
roc_auc = metrics.auc(fpr, tpr)
corr=np.corrcoef(Y,pred)[0,0]
ax.set_title('pDockQ, Cx:'+str(round(corr,2))+" ROC: "+str(round(roc_auc,2)))
    
plt.legend(loc = 'upper left')
plt.plot([0, 1], [0, 1],'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('pDockQ')
plt.xlabel('DockQ')
plt.savefig("../plots/pDockQ.png",dpi=1200)
plt.savefig("../plots/pDockQ.svg",dpi=1200)
#print (corr,roc_auc)


# In[130]:


f, ax = plt.subplots(figsize=(12., 12.))
tempdf=df_struct.dropna(subset=["IF_plDDT","SumIF","NumRes","SeqLen"])
correct=tempdf["GoodAll"]
X=tempdf[["IF_plDDT","SumIF","NumRes","SeqLen"]]
tempdf["pred"]=reg.predict(X)
for d in ["IF_plDDT","SumIF","NumRes","IFmin-50","pred"]:
    values=tempdf[d]
    fpr, tpr, threshold = metrics.roc_curve(correct, values)
    roc_auc = metrics.auc(fpr, tpr)
    plt.plot(fpr, tpr, label = d+': AUC = %0.2f' % roc_auc)
    ax.set_title('ROC: Correct vs Wrong Structure')
    
    #plt.legend(loc = 'lower right')
    plt.legend(loc = 'upper left')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
plt.savefig("../plots/ROC-error.png",dpi=1200)
plt.savefig("../plots/ROC-error.svg",dpi=1200)


# In[131]:


#df_struct[df_struct.MMall>0.9].dropna(subset=["IF_plDDT","SumIF","NumRes"]).sort_values("DockQall")


# In[132]:


df_negatome["GoodAll"]=False
df_negatome["Struct"]=False
df_negatome
df_concat=pd.concat([df_negatome,df_struct])[["SeqLen","SortedName","IF_plDDT","plDDT","NumRes","SumIF","GoodAll","Struct"]+minIFcols]
df_concat2=pd.concat([df_negatome,df_struct[df_struct.DockQ>0.23]])[["SeqLen","SortedName","IF_plDDT","plDDT","NumRes","SumIF","GoodAll","Struct"]+minIFcols]

df_concat


# In[133]:


f, ax = plt.subplots(figsize=(12., 12.))
tempdf=df_concat.dropna()
correct=tempdf["Struct"]
X=tempdf[["IF_plDDT","SumIF","NumRes","SeqLen"]]
tempdf["pred"]=reg.predict(X)

for d in ["IF_plDDT","SumIF","NumRes","pred","IFmin-50"]:
    values=tempdf[d]
    fpr, tpr, threshold = metrics.roc_curve(correct, values)
    roc_auc = metrics.auc(fpr, tpr)
    plt.plot(fpr, tpr, label = d+': AUC = %0.2f' % roc_auc)
    ax.set_title('ROC: Interaction vs Negatome')
    
    #plt.legend(loc = 'lower right')
    plt.legend(loc = 'upper left')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
plt.savefig("../plots/ROC-negatome.png",dpi=1200)
plt.savefig("../plots/ROC-negatome.svg",dpi=1200)



for d in ["IF_plDDT","SumIF","NumRes","pred","IFmin-50"]:
    values=tempdf[d]
    fpr, tpr, threshold = metrics.roc_curve(correct, values)
    roc_auc = metrics.auc(fpr, tpr)
    plt.plot(fpr, tpr, label = d+': AUC = %0.2f' % roc_auc)
    ax.set_title('ROC: Interaction vs Negatome')
    
    plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, .1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
plt.savefig("../plots/ROC-negatome-zoom.png",dpi=1200)
plt.savefig("../plots/ROC-negatome-zoom.svg",dpi=1200)


# In[134]:


f, ax = plt.subplots(figsize=(12., 12.))
tempdf=df_concat2.dropna()
correct=tempdf["Struct"]
X=tempdf[["IF_plDDT","SumIF","NumRes","SeqLen"]]
tempdf["pred"]=reg.predict(X)

for d in ["IF_plDDT","SumIF","NumRes",'IFmin-50',"pred"]:
    values=tempdf[d]
    fpr, tpr, threshold = metrics.roc_curve(correct, values)
    roc_auc = metrics.auc(fpr, tpr)
    plt.plot(fpr, tpr, label = d+': AUC = %0.2f' % roc_auc)
    ax.set_title('Receiver Operating Characteristic')
    
    plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, .1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')

plt.close()
# Making some venn diagrams
minIF=70
minNum=50


f, ax = plt.subplots(figsize=(12., 12.))
set1=set(df_struct["SortedName"].to_list())
set2=set(df_struct["SortedName"].to_list()+df_nostruct["SortedName"].to_list())
set3=set(df[(df.IF_plDDT>minIF)&(df.NumRes>minNum)]["SortedName"].to_list())

f=venn3([set1, set2, set3], ('Structure', 'HuMap', 'Predicted'))
c=venn3_circles([set1, set2, set3], linestyle='dashed', linewidth=1, color="grey")
plt.savefig("../plots/venn-humap.png",dpi=300)
plt.savefig("../plots/venn-humap.svg",dpi=300)
plt.close()


f, ax = plt.subplots(figsize=(12., 12.))
set1=set(df_struct["SortedName"].to_list())
set2=set(df_struct["SortedName"].to_list()+df_nostruct["SortedName"].to_list())
set3=set(df[(df.GoodpDockQ)]["SortedName"].to_list())

f=venn3([set1, set2, set3], ('Structure', 'HuMap', 'Predicted'))
c=venn3_circles([set1, set2, set3], linestyle='dashed', linewidth=1, color="grey")
plt.savefig("../plots/venn-humap-pdockq.png",dpi=300)
plt.savefig("../plots/venn-humap-pdockq.svg",dpi=300)
plt.close()



f, ax = plt.subplots(figsize=(12., 12.))
set1=set(df_struct["SortedName"].to_list())
set2=set(df[df.GoodpDockQ]["SortedName"].to_list())
set3=set(df[(df.IF_plDDT>minIF)&(df.NumRes>minNum)]["SortedName"].to_list())

f=venn3([set1, set2, set3], ('Structure', 'pDockQ', 'Interface+MinRes'))
c=venn3_circles([set1, set2, set3], linestyle='dashed', linewidth=1, color="grey")
plt.savefig("../plots/venn-test.png",dpi=300)
plt.savefig("../plots/venn-test.svg",dpi=300)
plt.close()



# Making some venn diagrams
f, ax = plt.subplots(figsize=(12., 12.))
set1=set(df_struct[df_struct.GoodAll]["SortedName"].to_list())
set2=set(df_struct["SortedName"].to_list())
set3=set(df_struct[(df_struct.IF_plDDT>minIF)&(df_struct.NumRes>minNum)]["SortedName"].to_list())
f=venn3([set1, set2, set3], ('DockQ>0.23', 'Structure', 'IF_plDDT >'+str(minIF)))
c=venn3_circles([set1, set2, set3], linestyle='dashed', linewidth=1, color="grey")

plt.savefig("../plots/venn-structure.png",dpi=300)
plt.savefig("../plots/venn-structure.svg",dpi=300)
    


# Huri mappgin

#print ("TEST",df.keys(),df)
tempdf1=df.dropna(subset=["id1","id2","IF_plDDT","NumRes"])[["SortedName","id1","id2","IF_plDDT","NumRes","GoodpDockQ","DockQ","pDockQ"]]
tempdf2=df_HuRI.dropna(subset=["id1","id2","IF_plDDT","NumRes"])[["SortedName","id1","id2","IF_plDDT","NumRes","GoodpDockQ","DockQ","pDockQ"]]
tempdf1["Method"]="HuMap"
tempdf2["Method"]="HuRI"
tempdf=pd.concat([tempdf1,tempdf2])

#
f, ax = plt.subplots(figsize=(12., 12.))
set1=set(tempdf[tempdf.Method=="HuRI"]["SortedName"].to_list())
set2=set(tempdf[tempdf.Method=="HuMap"]["SortedName"].to_list())
set3=set(tempdf[(tempdf.IF_plDDT>minIF)&(tempdf.NumRes>minNum)]["SortedName"].to_list())
f=venn3([set1, set2, set3], ('HuRI', 'HuMap', 'Predicted'))
c=venn3_circles([set1, set2, set3], linestyle='dashed', linewidth=1, color="grey")
plt.savefig("../plots/venn-huri.png",dpi=300)
plt.savefig("../plots/venn-huri.svg",dpi=300)


#
f, ax = plt.subplots(figsize=(12., 12.))
set1=set(tempdf[tempdf.Method=="HuRI"]["SortedName"].to_list())
set2=set(tempdf[tempdf.Method=="HuMap"]["SortedName"].to_list())
set3=set(tempdf[tempdf.pDockQ>0.23]["SortedName"].to_list())
 
f=venn3([set1, set2, set3], ('HuRI', 'HuMap', 'Predicted'))
c=venn3_circles([set1, set2, set3], linestyle='dashed', linewidth=1, color="grey")
for text in f.set_labels:
    text.set_fontsize(24)
for text in f.subset_labels:
    text.set_fontsize(24)

plt.savefig("../plots/venn-huri-humap-pDockQ-023.png",dpi=300)
plt.savefig("../plots/venn-huri-humap-pDockQ-023.svg",dpi=300)



#
f, ax = plt.subplots(figsize=(12., 12.))
set1=set(tempdf[tempdf.Method=="HuRI"]["SortedName"].to_list())
set2=set(tempdf[tempdf.Method=="HuMap"]["SortedName"].to_list())
set3=set(tempdf[tempdf.pDockQ>0.5]["SortedName"].to_list())
 
f=venn3([set1, set2, set3], ('HuRI', 'HuMap', 'Predicted'))
c=venn3_circles([set1, set2, set3], linestyle='dashed', linewidth=1, color="grey")
for text in f.set_labels:
    text.set_fontsize(24)
for text in f.subset_labels:
    text.set_fontsize(24)

plt.savefig("../plots/venn-huri-humap-pDockQ-05.png",dpi=300)
plt.savefig("../plots/venn-huri-humap-pDockQ-05.svg",dpi=300)


f, ax = plt.subplots(figsize=(12., 12.))
set1=set(tempdf[tempdf.Method=="HuRI"]["SortedName"].to_list())
set2=set(tempdf[tempdf.Method=="HuMap"]["SortedName"].to_list())
set3=set(tempdf[tempdf.DockQ.notna()]["SortedName"].to_list())
 
f=venn3([set1, set2, set3], ('HuRI', 'HuMap', 'Structure'))
c=venn3_circles([set1, set2, set3], linestyle='dashed', linewidth=1, color="grey")

for text in f.set_labels:
    text.set_fontsize(24)
for text in f.subset_labels:
    text.set_fontsize(24)
    
plt.savefig("../plots/venn-huri-humap-struct.png",dpi=300)
plt.savefig("../plots/venn-huri-humap-struct.svg",dpi=300)




#tempdf.to_csv("../data/huri-humap.csv")

#
#f, ax = plt.subplots(figsize=(12., 12.))
set1=set(tempdf[tempdf.Method=="HuRI"]["SortedName"].to_list())
set2=set(tempdf[tempdf.Method=="HuMap"]["SortedName"].to_list())
set3=set(tempdf[(tempdf.IF_plDDT>minIF)&(tempdf.NumRes>minNum)]["SortedName"].to_list())
set4=set(df_struct["SortedName"].to_list())

#venn4([set1, set2, set3], ('HuRI', 'HuMap', 'Predicted',"Structure"))

#plt.savefig("../plots/venn-huri4.png",dpi=300)
#plt.savefig("../plots/venn-huri4.svg",dpi=300)




# For Venn4 we need to use another packaqge

labels = venn.get_labels([set1,set2,set3,set4], fill=['number', 'logic'])
fig, ax = venn.venn4(labels, names=['HuRI', 'HuMap', 'Predicted',"Structure"])
fig.savefig("../plots/venn-huri4.png",dpi=300, bbox_inches='tight')
fig.savefig("../plots/venn-huri4.svg",dpi=300, bbox_inches='tight')
plt.close()
