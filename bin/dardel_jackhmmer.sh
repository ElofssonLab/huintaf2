#!/bin/bash -lx

#ml PDC/21.11
#ml parallel/20210922


ID=$1
CHAIN=$2 # last letter in ID

ncores=$3

abspath=`pwd`
MSAS=$abspath"/multimer/"
FASTAPATH=$abspath"/seq/"



echo "Processing ${ID}..."

FASTA=$FASTAPATH/${ID}.fasta
echo "FASTA: ${FASTA} CHAIN: ${CHAIN}"

TMPfile=`mktemp`
echo ">chain_A" > ${TMPfile}
tail -n +2  ${FASTA} >> ${TMPfile}

## create individual folder
mkdir -p $MSAS/$ID
mkdir -p $MSAS/$ID"/msas"
mkdir -p $MSAS/$ID"/msas/${CHAIN}"

# DATABASES
DATABASE_DIR=/cfs/klemming/projects/snic/snic2019-35-62/databases/
#uniprot_database_path=$DATABASE_DIR/uniprot/uniprot.fasta
uniprot_database_path=$DATABASE_DIR/uniprot_v23.fasta
uniref90_database_path=$DATABASE_DIR/uniref90/uniref90.fasta
mgnify_database_path=$DATABASE_DIR/mgnify/mgy_clusters_2018_12.fa # mgy_clusters_2022_05.fa #mgy_clusters_2018_12.fa
bfd_database_path=$DATABASE_DIR/bfd/bfd_metaclust_clu_complete_id30_c90_final_seq.sorted_opt 
#pdb_seqres_database_path=$DATABASE_DIR/pdb_seqres/pdb_seqres.txt
pdb_seqres_database_path=$DATABASE_DIR/pdb_seqres_v23.txt
#uniclust30_database_path=$DATABASE_DIR/uniclust30/uniclust30_2018_08/
small_bfd=$DATABASE_DIR/small_bfd/bfd-first_non_consensus_sequences.fasta
uniclust30_database_path=$DOWNLOAD_DIR/uniclust30/UniRef30_2021_03/UniRef30_2021_03
obsolete_pdbs_path=$DATABASE_DIR/pdb_mmcif/obsolete.dat

#JACKHMMER
jackhmmer_path=/cfs/klemming/projects/snic/snic2019-35-62/programs/hmmer-3.3.2/bin/jackhmmer

OUTPUT_REF90=$MSAS/$ID/msas/${CHAIN}/uniref90_hits.sto
OUTPUT_MGF=$MSAS/$ID/msas/${CHAIN}/mgnify_hits.sto
OUTPUT_UNIPROT=$MSAS/$ID/msas/${CHAIN}/uniprot_hits.sto
OUTPUT_PDB=$MSAS/$ID/msas/${CHAIN}/pdb_hits.sto
OUTPUT_BFD=$MSAS/$ID/msas/${CHAIN}/pdb_hits.sto
OUTPUT_SMALLBFD=$MSAS/$ID/msas/${CHAIN}/small_bfd_hits.sto

echo "${OUTPUT_REF90}, ${OUTPUT_MGF}, ${OUTPUT_SMALLBFD}" #${OUTPUT_BFD}"

FASTA=${TMPfile}

if [ ! -s ${OUTPUT_REF90} ]
then
    srun -p shared ${jackhmmer_path} -o /dev/null -A ${OUTPUT_REF90} --noali --F1 0.0005 --F2 5e-05 --F3 5e-07 --incE 0.0001 -E 0.0001 -N 1 --cpu ${ncores} $FASTA ${uniref90_database_path}
echo $ID uniref90 done
fi

if [ ! -s ${OUTPUT_MGF} ]
then
    srun -p shared ${jackhmmer_path} -o /dev/null -A ${OUTPUT_MGF} --noali --F1 0.0005 --F2 5e-05 --F3 5e-07 --incE 0.0001 -E 0.0001 -N 1 --cpu ${ncores} $FASTA ${mgnify_database_path}
    echo $ID mgnify done
fi
if [ ! -s ${OUTPUT_PDB} ]
then
    srun -p shared ${jackhmmer_path} -o /dev/null -A ${OUTPUT_PDB} --noali --F1 0.0005 --F2 5e-05 --F3 5e-07 --incE 0.0001 -E 0.0001 -N 1 --cpu ${ncores} $FASTA ${pdb_seqres_database_path} 
    echo $ID mgnify done
fi
    
if [ ! -s ${OUTPUT_UNIPROT} ]
then
    # srun -p shared
    ${jackhmmer_path} -o /dev/null -A ${OUTPUT_UNIPROT} --noali --F1 0.0005 --F2 5e-05 --F3 5e-07 --incE 0.0001 -E 0.0001 -N 1 --cpu ${ncores} $FASTA ${uniprot_database_path}
    echo $ID bfd done
fi

#if [ ! -s ${OUTPUT_SMALLBFD} ]
#then
#    srun -p shared ${jackhmmer_path} -o /dev/null -A ${OUTPUT_SMALLBFD} --noali --F1 0.0005 --F2 5e-05 --F3 5e-07 --incE 0.0001 -E 0.0001 -N 1 --cpu ${ncores} $FASTA ${small_bfd}
#    echo $ID small_bfd done
#fi

#srun -p shared ${jackhmmer_path} -o /dev/null -A ${OUTPUT_BFD} --noali --F1 0.0005 --F2 5e-05 --F3 5e-07 --incE 0.0001 -E 0.0001 -N 1 --cpu 8 $FASTA ${bfd_database_path}
#echo $ID bfd done
 
