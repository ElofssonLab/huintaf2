#!/usr/bin/env python3
import pandas as pd
import sys
import numpy as np
import argparse
import re
import matplotlib.pyplot as plt
import seaborn as sns

from pathlib import Path
home = str(Path.home())


#import GOntoSim
#from goatools.obo_parser import GODag
#from goatools.base import get_godag
#go = get_godag("go-basic.obo", optional_attrs={'relationship'})
#from Similarity import Similarity_of_Two_GOTerms, Similarity_of_Set_of_GOTerms

df=pd.read_csv(home+"/git/huintaf2/data/evaluation.csv")

df_HURI=pd.read_csv(home+"/git/huintaf2/data/HuRI.csv")
df_humap=pd.read_csv(home+"/git/huintaf2/data/humap.csv")
df_humap_uniprot=pd.read_csv(home+"/git/huintaf2/data/HuMap-uniprot.tab",sep="\t")
df_HuRI_uniprot=pd.read_csv(home+"/git/huintaf2/data/HuRI-uniprot.tab",sep="\t")
df_hurimapping=pd.read_csv(home+"/git/huintaf2/data/HuRI-uniprot-mapping.csv",sep=",",names=["ENSGENE","UniProt"])
#df_structure=pd.read_csv(home+"/git/huintaf2/data/petras-pdb-humap.csv")

df_gtex_huri=pd.read_csv(home+"/git/huintaf2/data/gtex-huri.tsv",sep="\s")
df_gtex_humap=pd.read_csv(home+"/git/huintaf2/data/gtex-humap.tsv",sep="\s")
df_gtex_huri["Dataset"]="HuRI"
df_gtex_humap["Dataset"]="HuMap"
df_gtex_huri["pDockQ"]=df_gtex_huri["pdockq"]
df_gtex_humap["pDockQ"]=df_gtex_humap["pdockq"]
df_gtex=pd.concat([df_gtex_huri,df_gtex_humap])
#df_gtex

def getname(id1,id2):
    if (id1>id2):
        Name=id2+"-"+id1
    else:
        Name=id1+"-"+id2
    return (Name)


def splitsubcell(string):
    #print (string)
    try:
        return(re.sub('{;*}','',re.sub('[:,]',';',re.sub(r'ECO:[0-9]+[\|A-Za-z0-9:]*','',re.sub(r'\s','',re.sub(r'\.',',',string))))))
    except:
        return(string)
    
# Subcellular localistaions

df_humap[["id1","id2"]]=df_humap["Name"].str.split("-",n=1,expand=True)
df_humap=pd.merge(df_humap,df_humap_uniprot,left_on="id1",right_on="Entry")
df_humap=pd.merge(df_humap,df_humap_uniprot,left_on="id2",right_on="Entry")

df_humap["Subcell_x"]=df_humap["Subcellular location [CC]_x"].apply(splitsubcell)
df_humap[["temp1","subcell1a","subcell1b","subcell1c","subcell1d"]]=df_humap["Subcell_x"].str.split(r'[;]',n=4,expand=True)
df_humap["Subcell_y"]=df_humap["Subcellular location [CC]_y"].apply(splitsubcell)
df_humap[["temp2","subcell2a","subcell2b","subcell2c","subcell2d"]]=df_humap["Subcell_y"].str.split(r'[;]',n=4,expand=True)


df_humapsubcell=df_humap.loc[(df_humap.temp1!="nan")&(df_humap.temp2!="nan")]
df_humapsubcell["Localisation"]=np.where(df_humapsubcell["subcell1a"]==df_humapsubcell["subcell2a"],
                                   np.where(df_humapsubcell["subcell1b"]==df_humapsubcell["subcell2b"],
                                            np.where(df_humapsubcell["subcell1c"]==df_humapsubcell["subcell2c"],
                                                     "SubSubOrg","SubOrg"),"Organell"),"Different")
df_humapsubcell["SubcellularDegree"]=np.where(df_humapsubcell["subcell1a"]==df_humapsubcell["subcell2a"],
                                   np.where(df_humapsubcell["subcell1b"]==df_humapsubcell["subcell2b"],
                                            np.where(df_humapsubcell["subcell1c"]==df_humapsubcell["subcell2c"],
                                                     4,3),2),0)

my_pal = {"CORUM-dimers": "orange", "CORUM-multimers": "darkorange", "CORUM": "orange",
          "HuRI": "lightgreen" ,"HuMap":"grey" ,"HuMap2":"black" ,"COMPLEXES-direct":"royalblue" ,
          "Direct":"royalblue" ,"Indirect":"mediumblue" ,
          "COMPLEXES-indirect":"mediumblue" ,"COMPLEXES":"blue" ,"PDB":"red",
          "Different": "red",
          "Organell": "blue",
          "SubOrganell": "royalblue",
          "SubOrg": "royalblue",
          "SubSubOrg": "darkblue",
          "HuRI: Different": "lightgreen",
          "HuRI: Organell": "darkgreen",
          "HuRI: SubOrganell": "green",
          "HuRI: SubOrg": "green",
          "HuRI: SubSubOrg": "darkgreen",
          "HuMap: Different": "lightgrey",
          "HuMap: Organell": "darkgrey",
          "HuMap: SubOrganell": "grey",
          "HuMap: SubOrg": "grey",
          "HuMap: SubSubOrg": "dimgrey",
          "HuRI: 0": "palegreen",
          "HuRI: <250": "lightgreen",
          "HuRI: <500": "limegreen",
          "HuRI: <750": "green",
          "HuRI: >750": "darkgreen",
          "HuMap: 0": "gainsboro",
          "HuMap: <250": "lightgrey",
          "HuMap: <500": "silver",
          "HuMap: <750": "grey",
          "HuMap: >750": "darkgrey",
          "HuRI: NoCoExpress": "lightgreen",
          "HuMap: NoCoExpress": "lightgrey",
          "HuRI: CoExpress": "green",
          "HuMap: CoExpress": "grey",
          "PDB: Different": "pink",
          "PDB: Organell": "red",
          "PDB: SubOrganell": "tomato",
          "PDB: SubOrg": "tomato",
          "PDB: SubSubOrg": "firebrick",
          "PDB: 0": "pink",
          "PDB: <250": "lightsalmon",
          "PDB: <500": "tomato",
          "PDB: <750": "red",
          "PDB: >750": "firebrick",
          "PDB: NoCoExpress": "pink",
          "PDB: CoExpress": "red"
}
          

sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 60) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_humapsubcell,x="pDockQ",y="Localisation",kind="kde",scale="width",cut=0,
               showmeans=True, showextrema=True, showmedians=False ,palette=my_pal)
plt.savefig(home+"/git/huintaf2/plots/Subcell-humap-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Subcell-humap-violin.svg",dpi=600)




df_HURI[["temp1","temp2"]]=df_HURI["Name"].str.split("-",n=2,expand=True)
df_HURI=pd.merge(df_HURI,df_hurimapping,left_on="temp1",right_on="ENSGENE")
df_HURI=pd.merge(df_HURI,df_hurimapping,left_on="temp2",right_on="ENSGENE")
df_HURI["id1"]=df_HURI["UniProt_x"]
df_HURI["id2"]=df_HURI["UniProt_y"]
df_HURI["Name"] = df_HURI.apply(lambda x: getname(x["id1"],x["id2"]),axis=1)

df_HURI=pd.merge(df_HURI,df_HuRI_uniprot,left_on="id1",right_on="Entry")
df_HURI=pd.merge(df_HURI,df_HuRI_uniprot,left_on="id2",right_on="Entry")


df_HURI["Subcell_x"]=df_HURI["Subcellular location [CC]_x"].apply(splitsubcell)
df_HURI[["temp1","subcell1a","subcell1b","subcell1c","subcell1d"]]=df_HURI["Subcell_x"].str.split(r'[;]',n=4,expand=True)
df_HURI["Subcell_y"]=df_HURI["Subcellular location [CC]_y"].apply(splitsubcell)
df_HURI[["temp2","subcell2a","subcell2b","subcell2c","subcell2d"]]=df_HURI["Subcell_y"].str.split(r'[;]',n=4,expand=True)




df_HURIsubcell=df_HURI.loc[(df_HURI.temp1!="nan")&(df_HURI.temp2!="nan")]

df_HURIsubcell["Localisation"]=np.where(df_HURIsubcell["subcell1a"]==df_HURIsubcell["subcell2a"],
                                   np.where(df_HURIsubcell["subcell1b"]==df_HURIsubcell["subcell2b"],
                                            np.where(df_HURIsubcell["subcell1c"]==df_HURIsubcell["subcell2c"],
                                                     "SubSubOrg","SubOrg"),"Organell"),"Different")
df_HURIsubcell["SubcellularDegree"]=np.where(df_HURIsubcell["subcell1a"]==df_HURIsubcell["subcell2a"],
                                   np.where(df_HURIsubcell["subcell1b"]==df_HURIsubcell["subcell2b"],
                                            np.where(df_HURIsubcell["subcell1c"]==df_HURIsubcell["subcell2c"],
                                                     4,3),2),0)

df_temp=pd.concat([df_humap,df_HURI])
df_structure=df[df.DockQ.notna()]
df_structure=pd.merge(df_structure,df_temp,on="Name")
df_structure["pDockQ"]=df_structure["pDockQ_x"]


df_structuresubcell=df_structure.loc[(df_structure.temp1!="nan")&(df_structure.temp2!="nan")]

df_structuresubcell["Localisation"]=np.where(df_structuresubcell["subcell1a"]==df_structuresubcell["subcell2a"],
                                   np.where(df_structuresubcell["subcell1b"]==df_structuresubcell["subcell2b"],
                                            np.where(df_structuresubcell["subcell1c"]==df_structuresubcell["subcell2c"],
                                                     "SubSubOrg","SubOrg"),"Organell"),"Different")
df_structuresubcell["SubcellularDegree"]=np.where(df_structuresubcell["subcell1a"]==df_structuresubcell["subcell2a"],
                                   np.where(df_structuresubcell["subcell1b"]==df_structuresubcell["subcell2b"],
                                            np.where(df_structuresubcell["subcell1c"]==df_structuresubcell["subcell2c"],
                                                     4,3),2),0)


#print 

sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 60) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_HURIsubcell,x="pDockQ",y="Localisation",kind="kde",scale="width",cut=0,
               showmeans=True, showextrema=True, showmedians=False ,palette=my_pal)
plt.savefig(home+"/git/huintaf2/plots/Subcell-HuRI-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Subcell-HuRI-violin.svg",dpi=600)





df_test=pd.DataFrame({'Name' : [] ,"pDockQ":[] ,"Localisation":[],"SubcellularDegree":[]})
df_temp=df_humapsubcell[["Name","pDockQ","Localisation","SubcellularDegree"]]
df_temp["Localisation"]="HuMap: "+df_temp["Localisation"]
df_temp["Dataset"]="HuMap"
df_temp["subcell"]=np.where(df_temp["SubcellularDegree"]>0,"Organell","Different")
df_temp["Subcell"]="HuMap: "+df_temp["subcell"]
df_test=pd.concat([df_test,df_temp])
df_temp=df_HURIsubcell[["Name","pDockQ","Localisation","SubcellularDegree"]]
df_temp["Localisation"]="HuRI: "+df_temp["Localisation"]
df_temp["Dataset"]="HuRI"
df_temp["subcell"]=np.where(df_temp["SubcellularDegree"]>0,"Organell","Different")
df_temp["Subcell"]="HuRI: "+df_temp["subcell"]
df_test=pd.concat([df_test,df_temp])
df_temp=df_structuresubcell[["Name","pDockQ","Localisation","SubcellularDegree"]]
df_temp["Localisation"]="PDB: "+df_temp["Localisation"]
df_temp["Dataset"]="PDB"
df_temp["subcell"]=np.where(df_temp["SubcellularDegree"]>0,"Organell","Different")
df_temp["Subcell"]="PDB: "+df_temp["subcell"]
df_test=pd.concat([df_test,df_temp])




my_order = ["HuMap: Different","HuMap: Organell","HuMap: SubOrg","HuMap: SubSubOrg",
            "HuRI: Different","HuRI: Organell","HuRI: SubOrg","HuRI: SubSubOrg",
            "PDB: Different","PDB: Organell","PDB: SubOrg","PDB: SubSubOrg"]

sns.set(font_scale=1.5,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 75) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_test,x="pDockQ",y="Localisation",kind="kde",cut=0,
               showmeans=True, showextrema=True, showmedians=False ,palette=my_pal,order=my_order)
plt.savefig(home+"/git/huintaf2/plots/Subcell-violin.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Subcell-violin.svg",dpi=600) #,scale="width"



f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
sns.lineplot(df_test[df_test.Dataset=="HuRI"].sort_values(by=["SubcellularDegree"]).SubcellularDegree,df_test[df_test.Dataset=="HuRI"].sort_values(by=["SubcellularDegree"])["pDockQ"].rolling(200,min_periods=200, center=True).mean(),color="green",label="HuRI")
sns.lineplot(df_test[df_test.Dataset=="HuMap"].sort_values(by=["SubcellularDegree"]).SubcellularDegree,df_test[df_test.Dataset=="HuMap"].sort_values(by=["SubcellularDegree"])["pDockQ"].rolling(200,min_periods=200, center=True).mean(),color="grey",label="HuMap")
sns.lineplot(df_test[df_test.Dataset=="PDB"].sort_values(by=["SubcellularDegree"]).SubcellularDegree,df_test[df_test.Dataset=="PDB"].sort_values(by=["SubcellularDegree"])["pDockQ"].rolling(200,min_periods=200, center=True).mean(),color="red",label="PDB")
plt.legend()
plt.savefig(home+"/git/huintaf2/plots/SubCell-running.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/SubCell-running.svg",dpi=600)






f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
g=sns.kdeplot(data=df_test[df_test.Subcell=="HuRI: Organell"], x="pDockQ",common_norm=False,label="HuRI: Organell",linewidth=3,linestyle="-",color="lightgreen")
g=sns.kdeplot(data=df_test[df_test.Subcell=="HuRI: Different"], x="pDockQ",common_norm=False,label="HuRI: Different",linewidth=3,linestyle="--",color="green")
g=sns.kdeplot(data=df_test[df_test.Subcell=="HuMap: Organell"], x="pDockQ",common_norm=False,label="HuMap: Organell",linewidth=3,linestyle="-",color="lightgrey")
g=sns.kdeplot(data=df_test[df_test.Subcell=="HuMap: Different"], x="pDockQ",common_norm=False,label="HuMap: Different",linewidth=3,linestyle="--",color="grey")
g=sns.kdeplot(data=df_test[df_test.Subcell=="PDB: Organell"], x="pDockQ",common_norm=False,label="PDB: Organell",linewidth=3,linestyle="-",color="pink")
sns.kdeplot(data=df_test[df_test.Subcell=="PDB: Different"], x="pDockQ",common_norm=False,label="PDB: Different",linewidth=3,linestyle="--",color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/SubCell-datasets.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/SubCell-datasets.svg",dpi=600)




f, ax = plt.subplots(figsize=(12., 12.))
sns.set(font_scale=2,style="whitegrid",rc={'figure.figsize':(12,12)})
g=sns.kdeplot(data=df_test[df_test.Dataset=="HuRI"], x="SubcellularDegree",common_norm=False,label="HuRI",linewidth=3,linestyle="--",color="green")
g=sns.kdeplot(data=df_test[df_test.Dataset=="HuMap"], x="SubcellularDegree",common_norm=False,label="HuMap",linewidth=3,linestyle="--",color="grey")
sns.kdeplot(data=df_test[df_test.Dataset=="PDB"], x="SubcellularDegree",common_norm=False,label="PDB",linewidth=3,linestyle="--",color="red")
plt.legend()
sns.set(font_scale=3,style="whitegrid",rc={'figure.figsize':(12,12)})
plt.savefig(home+"/git/huintaf2/plots/SubCell-Location.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/SubCell-Location.svg",dpi=600)




my_order = ["HuMap: Different","HuMap: Organell",
            "HuRI: Different","HuRI: Organell",
            "PDB: Different","PDB: Organell"]

sns.set(font_scale=1.5,style="whitegrid",rc={'figure.figsize':(12,12)})
f, ax = plt.subplots(figsize=(12., 12.))
plt.yticks(rotation = 75) # Rotates X-Axis Ticks by 45-degrees
sns.violinplot(data=df_test,x="pDockQ",y="Subcell",kind="kde",cut=0,
               showmeans=True, showextrema=True, showmedians=False ,palette=my_pal,order=my_order)
plt.savefig(home+"/git/huintaf2/plots/Subcell-violin2.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Subcell-violin2.svg",dpi=600) #,scale="width"



sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
#f, ax = plt.subplots(figsize=(12., 12.))
#df_HURIsubcell["Disorder"]=np.where(df_HURIsubcell["FracDiso"]>0.10,"Disorder","Order")

sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
#f, ax = plt.subplots(figsize=(12., 12.))
#df_HURIsubcell["Disorder"]=np.where(df_HURIsubcell["FracDiso"]>0.10,"Disorder","Order")
g=sns.jointplot(data=df_test[df_test.Dataset!="PDB"],x="SubcellularDegree",y="pDockQ",
                hue="Dataset" ,kind="kde",palette=my_pal,common_norm=False)
g.ax_joint.set_xticklabels(['','Different','','Organell','SubOrg','SubSub','SubSubSub'],rotation=45)
plt.savefig(home+"/git/huintaf2/plots/Subcell-pDockQ.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/Subcell-pDockQ.svg",dpi=600)


# GO similairyt
#methods=['GOntoSim','Baseline','Baseline_LCA','Baseline_Desc','wang']


sns.set(font_scale=1.2,style="whitegrid",rc={'figure.figsize':(12,12)})
#f, ax = plt.subplots(figsize=(12., 12.))
#df_HURIsubcell["Disorder"]=np.where(df_HURIsubcell["FracDiso"]>0.10,"Disorder","Order")
sns.jointplot(data=df_gtex,x="gtex",y="pDockQ",
              hue="Dataset" ,kind="kde",palette=my_pal,common_norm=False)
plt.savefig(home+"/git/huintaf2/plots/gtex-pDockQ.png",dpi=600)
plt.savefig(home+"/git/huintaf2/plots/gtex-pDockQ.svg",dpi=600)
