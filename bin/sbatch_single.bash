#!/bin/bash -x
#SBATCH -A berzelius-2021-64
#SBATCH --output=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/out/single-%j.out
#SBATCH --error=/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/err/single-%j.err
#SBATCH --array=1-5
#SBATCH -N 1
#SBATCH --gpus-per-task=8  
#SBATCH --export=ALL,CUDA_VISIBLE_DEVICES
#SBATCH --gpus=8
#SBATCH -t 24:00:00
#SBATCH --exclusive
export NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'



list=$1
offset=$2

pos=$(($SLURM_ARRAY_TASK_ID + $offset))
id1=`tail -n+$pos $list | head -n 1 | gawk '{print $1}'`
#id2=`tail -n+$pos $list | head -n 1 | gawk '{print $2}'`


OUTFOLDER=single/
NAME=${id1}
#pushd $AFHOME
#cd $AFHOME
#NAME="./" # to not run this part

if [ ! -d $OUTFOLDER/$NAME ]
then
    echo 'Processing ' $id1 $id2 '...'
    mkdir -p $OUTFOLDER/$NAME/
    /proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/runsingle.bash ${id1}  > out/${id1}.log 2> err/${id1}.err
fi

