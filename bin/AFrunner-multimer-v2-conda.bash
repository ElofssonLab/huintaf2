#!/bin/bash -x
#SBATCH -A berzelius-2022-106
#SBATCH --output=out/AFmultiV2cuda-%j.out
#SBATCH --error=err/AFmultiV2cuda-%j.err
#SBATCH --array=1-1
#SBATCH -N 1
#SBATCH --export=ALL,CUDA_VISIBLE_DEVICES
#SBATCH --gpus=8
#SBATCH --exclusive
#SBATCH -t 48:00:00


#export CUDA_VISIBLE_DEVICES='all'
export NVIDIA_VISIBLE_DEVICES='all'
export TF_FORCE_UNIFIED_MEMORY='1'
export XLA_PYTHON_CLIENT_MEM_FRACTION='4.0'
#export TF_CPP_MIN_LOG_LEVEL=0
##


##### AF2 CONFIGURATION #####
COMMON="/proj/berzelius-2021-29/"
AFHOME="/proj/berzelius-2021-29/users/x_arnel/git/AF2-V2.2.0/"
#AFHOME=$COMMON"/af2-v2.2.0//alphafold/"
#AFHOME=$COMMON"/AF2-multimer-mod/" 			# Path of AF2-multimer-mod directory.
#SINGULARITY=$COMMON"/af2-v2.2.0//alphafold/AF_data_v220/alphafold_v220.sif" 	# Path of singularity image.
PARAM=$COMMON"/af2-v2.2.0//alphafold/AF_data_v220/" 				# path of param folder containing AF2 Neural Net parameters.
MAX_RECYCLES=10
BIN="/proj/berzelius-2021-29/users/x_arnel/Hu.Map-2.0/bin/"



#### ONLY ONE OF THE FOLLOWING SHOULD BE UNCOMMENTED ####################
#MODEL_SET="model_1"							# Uncomment to run single standard model.
#MODEL_SET="model_1_ptm"						# Uncomment to run single ptm model.
MODEL_SET="multimer" 						# Uncomment to run single multimer model.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}"; done		# Uncomment to run all 5 standard models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_ptm"; done	# Uncomment to run all 5 ptm models.
#for n in {1..5}; do MODEL_SET="$MODEL_SET model_${n}_multimer"; done	# Uncomment to run all 5 multimers models.
#########################################################################

### RUNTIME SPECIFICATIONS ###
LIST=$1 	# List with each line containing: fastaID(no format) [true or false] set true if protein is prokariote, false otherwise.
OFFSET=$2 	# ignore first $OFFSET lines of list; to set according to SLURM array size, in order to run all the list.
#FOLDER=$3 	# Path where AF2 picks fasta files and generates its output folder structure.
FOLDER="complexes-v2/"
PRO=false
POS=$(($SLURM_ARRAY_TASK_ID + $OFFSET))
LINE=`tail -n+$POS $LIST | head -n 1`
id=`echo $LINE | sed "s/\ /-/g" `	# path of input fasta file (without .fasta suffix)
#id2=`echo $LINE | awk '{print $2}'`	# path of input fasta file (without .fasta suffix)
FASTA=${FOLDER}"/${id}.fasta"
#PRO=`echo $LINE | awk '{print $2}'`	# true if folding a prokariotic multimer, false otherwise
PRO=false,false
flagfile=/proj/beyondfold/apps/alphafold/flagfiles/multimer_full_dbs.flag

for PRESET in $MODEL_SET
do
    if [[ ! -f  ${FOLDER}/${id}/ranked_5.pdb ]]
    then
        ##### TO JUST FOLD, GIVEN AN AF DEFAULT FOLDER STRUCTURE WITH MSAS #####
        ##### ALREADY EXIST AT $FOLDER/$FASTA                              ##### 
	module load Anaconda/2021.05-nsc1
	# conda init bash
	conda activate /proj/berzelius-2021-29/users/x_arnel/.conda/envs/af_server/ 
	#singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
	#singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
        #python3 $AFHOME/alphafold/run_alphafold.py \
	python3 /proj/beyondfold/apps/alphafold/run_alphafold.py \
		--flagfile $flagfile --output_dir $FOLDER --fasta_paths ${FASTA} 
 
        #--is_prokaryote_list=$PRO \

#	##### TO JUST FOLD, PROVIDING CUSTOM MSAS #####
#	singularity exec --nv --bind $COMMON:$COMMON $SINGULARITY \
#		python3 $AFHOME/alphafold/run_alphafold.py \
#			--fasta_paths=$FOLDER/${FASTA}.fasta \
#			--model_preset=$PRESET \
#			--output_dir=$FOLDER \
#			--is_prokaryote_list=$PRO \
#			--data_dir=$PARAM \
#			--custom_msas=$MSAS
	# Now we shoudl extract the pTM etc

    fi
    #for i in  ${FOLDER}/${id}/*pred*.pkl
    #do
    #	j=`basename $i .pkl`
    #${BIN}/getptm.py -m  ${FOLDER}/${id}/$j.pkl > ${FOLDER}/${id}/$j.ptm
    #done
    #rm  ${FOLDER}/${id}/*pred*.pkl

done


#pip install --upgrade "jax[cuda]" -f https://storage.googleapis.com/jax-releases/jax_releases.html
#export TF_FORCE_UNIFIED_MEMORY=1
#export XLA_PYTHON_CLIENT_MEM_FRACTION=10.0

