#!/usr/bin/env python
# coding: utf-8

# In[1]:


import seaborn as sns

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import sklearn.metrics as metrics
#import csv


# In[2]:



from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline


# In[4]:


#import jgraph as ig
from pyvis.network import Network
import networkx as nx
import plotly.graph_objects as go


# In[5]:



df=pd.read_csv("../data/evaluation.csv")
df["HuMap"]=True
df["HuRI"]=False
#df_negpLDDT=pd.read_csv("../data/negatome-pLDDT.csv")
#df_negseqlen=pd.read_csv("../data/negatome-seqlen.csv",sep=",",names=["Name","SeqLen1","SeqLen2"])
#df_negseqlen["SeqLen"]=df_negseqlen.SeqLen1+df_negseqlen.SeqLen2
#df_negatome=pd.merge(df_negpLDDT,df_negseqlen,on=["Name"],how="inner")

df["SortedName"]=['-'.join(sorted(tup)) for tup in zip(df['id1'], df['id2'])]
df_HuRI=pd.read_csv("../data/Hurimapping.csv")
df_HuRI=df_HuRI.rename(columns={"id1":"EnsembleId1","id2":"EnsembleId2"})
df_HuRI=df_HuRI.rename(columns={"UniProt_x":"id1","UniProt_y":"id2"})
df_HuRI["HuMap"]=False
df_HuRI["HuRI"]=True

df_combo=pd.concat([df,df_HuRI])

#print (df_all.SortedName)
#sys.exit()

print ("INFO> Finished reading in CSVs")
# In[6]:


minres=50
minIF=70
minplDDT=80

# HuRI is too big, so let's skip all edges that do not have a good hit
df_HuRI=df_HuRI[(df_HuRI.NumRes>minres)&(df_HuRI.IF_plDDT>minIF)]


df["Good"]=np.where((df.NumRes>minres)&(df.IF_plDDT>minIF),True,False)
df["VeryGood"]=np.where((df.NumRes>minres)&(df.IF_plDDT>minIF)&(df.plDDT>minplDDT),True,False)

#df_negatome["Good"]=np.where((df_negatome.NumRes>minres)&(df_negatome.IF_plDDT>minIF),True,False)
df["Struct"]= np.where(df.DockQ.notna(), True, False)


df_HuRI["Good"]=np.where((df_HuRI.NumRes>minres)&(df_HuRI.IF_plDDT>minIF),True,False)
df_HuRI["VeryGood"]=np.where((df_HuRI.NumRes>minres)&(df_HuRI.IF_plDDT>minIF)&(df_HuRI.plDDT>minplDDT),True,False)
df_HuRI["Struct"]= False

df_combo["Good"]=np.where((df_combo.NumRes>minres)&(df_combo.IF_plDDT>minIF),True,False)
df_combo["VeryGood"]=np.where((df_combo.NumRes>minres)&(df_combo.IF_plDDT>minIF)&(df_combo.plDDT>minplDDT),True,False)
df_combo["Struct"]= np.where(df_combo.DockQ.notna(), True, False)


df_struct=df[df.DockQ.notna()]
df_nostruct=df[df.DockQ.isna()]
#df_struct["Good"]= np.where(df_struct.DockQ>0.23, True, False)
#df_struct["TMGood"]= np.where(df_struct.MMall>0.7, True, False)
#df_struct["GoodAll"]= np.where(df_struct.DockQ>0.23, True, False)
#df_struct["TMGoodAll"]= np.where(df_struct.MMall>0.7, True, False)
#df_corr=df_struct[df_struct.GoodAll==True]
#df_incorr=df_struct[df_struct.GoodAll==False]


# In[7]:

print ("INFO> Finished merging")

#df_str=df_struct # [df_struct.Good==True]
Gstruct=nx.from_pandas_edgelist(df_struct,source="id1",target="id2",edge_attr="IF_plDDT")
netStruct=Network(notebook=True,height='1200px', width='100%',)
netStruct.from_nx(Gstruct)

options = {
    'node_color': 'black',
    'node_size': 10,
    'width': 10,
}
#nx.draw(Gstruct, with_labels=False, font_weight='bold',**options)
#plt.savefig("../plots/network-draw.svg")
#plt.close()
#nx.draw_random(Gstruct, with_labels=False, font_weight='bold',**options)
#plt.savefig("../plots/network-random.svg")
#plt.close()
#nx.draw_circular(Gstruct, with_labels=False, font_weight='bold',**options)
#plt.savefig("../plots/network-circular.svg")
#plt.close()
#nx.draw_spectral(Gstruct, with_labels=False, font_weight='bold',**options)
#plt.savefig("../plots/network-spectral.svg")
#plt.close()
#nx.draw_shell(Gstruct, with_labels=False, font_weight='bold',**options)
#plt.savefig("../plots/network-shell.svg")
#plt.close()
#sys.exit()


# In[ ]:





# In[9]:


df_pred=df[(df.Good)]
Gpred=nx.from_pandas_edgelist(df_pred,source="id1",target="id2",edge_attr="IF_plDDT")
#len(Gpred)
df_pred2=df[(df.VeryGood)]
Gpred2=nx.from_pandas_edgelist(df_pred2,source="id1",target="id2",edge_attr="IF_plDDT")


# In[10]:


netPred=Network(notebook=True,height='1200px', width='100%',)
netPred.from_nx(Gpred)
netPred2=Network(notebook=True,height='1200px', width='100%',)
netPred2.from_nx(Gpred2)

# SPecific networks
# This is taken from
# for i in `grep P25789 ../data/edges-pred2.csv | sed "s/7,//g" | sed "s/-/ /g" `; do echo $i ; done | sort -u > P25789.txt

codes=["P25789","O14818","P20618","P25786","P25787","P25788","P25789","P25789","P28062","P28065","P28066","P28070","P28072","P28074","P49721","P60900","Q8TAA3","Q99436","Q9Y244"]

df_P25789=df_pred[(df_pred.id1.isin(codes))|(df_pred.id2.isin(codes))]
G25789=nx.from_pandas_edgelist(df_P25789,source="id1",target="id2",edge_attr="IF_plDDT")

#print (len(df_pred))
#print (len(df_P25789))
#sys.exit()

# This is taken from
# for i in `grep P31150 ../data/edges-pred2.csv | sed "s/46,//g" | sed "s/-/ /g" `; do echo $i ; done | sort -u > P31150.txt


codes=["P20338","P31150","P51149","P51151","P51153","P61006","P61018","P61020","P61026","P61106","Q13636","Q15286","Q6IQ22","Q969Q5","Q9BZG1","Q9UL26"]

df_P31150=df_pred[(df_pred.id1.isin(codes))|(df_pred.id2.isin(codes))]
G31150=nx.from_pandas_edgelist(df_P31150,source="id1",target="id2",edge_attr="IF_plDDT")
netP25789=Network(notebook=True,height='1200px', width='100%',)
netP25789.from_nx(G25789)
netP31150=Network(notebook=True,height='1200px', width='100%',)
netP31150.from_nx(G31150)



# In[11]:


Gall=nx.from_pandas_edgelist(df,source="id1",target="id2",edge_attr="IF_plDDT")
netALL=Network(notebook=True,height='1200px', width='100%',)
netALL.from_nx(Gall)


# Hur is too big so we only include good 
GHuRI=nx.from_pandas_edgelist(df_HuRI,source="id1",target="id2",edge_attr="IF_plDDT")
netHuRI=Network(notebook=True,height='1200px', width='100%',)
netHuRI.from_nx(GHuRI)

#Gcombo=nx.from_pandas_edgelist(df_combo,source="id1",target="id2",edge_attr="IF_plDDT")
#netcombo=Network(notebook=True,height='1200px', width='100%',)
#netcombo.from_nx(Gcombo)

print ("INFO> FInished building networks")

# In[13]:


#netALL.nodes


# In[14]:


# Is this used ?
temp=df[["id1","Struct"]]
temp=temp.rename(columns={"id1":"id2"})
nodes=pd.concat([temp,df[["id2","Struct"]]])
nodes=nodes.rename(columns={"id2":"id"})
nodes=nodes.drop_duplicates()
#nodes[nodes.id=="Q86VD1"]["Struct"].any()

df_names=pd.read_csv("../data/HuMap-names.csv",sep=",",names=["id","UniProtID","Description","Organism","OrgId","xid","SN","GN"])


# In[15]:

print ("INFO> Finished building nodes")

for n in netALL.nodes:
    n["title"]=str(n["id"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    n["font"] = "14px Arial black Bold"
    n["borderWidth"]=1
    n["opacity"]=50
    #print (n["id"],nodes[nodes.id==n["id"]].any()[0])
    #print (n["id"],nodes[nodes.id==n["id"]])
    if nodes[nodes.id==n["id"]]["Struct"].any():
        n["size"]=22
        n["shape"]="star"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="red"
    else:
        #print (n["id"])
        n["size"]=12
        n["shape"]="dot"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="blue"        

#for n in netcombo.nodes:
#    n["title"]=str(n["id"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
#    n["title"]=str(df_names[df_names.id==n["id"]]["Description"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
#    n["title"]=str(df_names[df_names.id==n["id"]]["Description"])
#    n["title"]=df_names[df_names.id==n["id"]][["id","Description"]]
#    n["title"]=n["id"]
#    n["font"] = "14px Arial black Bold"
#    n["borderWidth"]=1
#    n["opacity"]=50
#    #print (n["id"],nodes[nodes.id==n["id"]].any()[0])
#    #print (n["id"],nodes[nodes.id==n["id"]])
#    if nodes[nodes.id==n["id"]]["Struct"].any():
#        n["size"]=22
#        n["shape"]="star"
#        n["opacity"]=50
#        n["Shadow"]=True
#        n["color"]="red"
#    else:
#        #print (n["id"])
#        n["size"]=12
#        n["shape"]="dot"
#        n["opacity"]=50
#        n["Shadow"]=True
#        n["color"]="blue"        

for n in netHuRI.nodes:
    n["title"]=str(n["id"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=df_names[df_names.id==n["id"]][["id","Description"]]
    #n["title"]=n["id"]
    n["font"] = "14px Arial black Bold"
    n["borderWidth"]=1
    n["opacity"]=50
    #print (n["id"],nodes[nodes.id==n["id"]].any()[0])
    #print (n["id"],nodes[nodes.id==n["id"]])
    if nodes[nodes.id==n["id"]]["Struct"].any():
        n["size"]=22
        n["shape"]="star"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="red"
    else:
        #print (n["id"])
        n["size"]=12
        n["shape"]="dot"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="blue"        


# In[16]:


for n in netPred.nodes:
    n["title"]=str(n["id"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=df_names[df_names.id==n["id"]][["id","Description"]]
    #n["title"]=n["id"]
    n["font"] = "14px Arial black Bold"
    n["borderWidth"]=1
    n["opacity"]=50
    #print (n["id"],nodes[nodes.id==n["id"]].any()[0])
    #print (n["id"],nodes[nodes.id==n["id"]])
    if nodes[nodes.id==n["id"]]["Struct"].any():
        n["size"]=22
        n["shape"]="star"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="red"
    else:
        #print (n["id"])
        n["size"]=12
        n["shape"]="dot"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="blue"        


for n in netP25789.nodes:
    n["title"]=str(n["id"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=df_names[df_names.id==n["id"]][["id","Description"]]
    #n["title"]=n["id"]
    n["font"] = "14px Arial black Bold"
    n["borderWidth"]=1
    n["opacity"]=50
    #print (n["id"],nodes[nodes.id==n["id"]].any()[0])
    #print (n["id"],nodes[nodes.id==n["id"]])
    if nodes[nodes.id==n["id"]]["Struct"].any():
        n["size"]=22
        n["shape"]="star"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="red"
    else:
        #print (n["id"])
        n["size"]=12
        n["shape"]="dot"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="blue"        
        

for n in netP31150.nodes:
    n["title"]=str(n["id"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=df_names[df_names.id==n["id"]][["id","Description"]]
    #n["title"]=n["id"]
    n["font"] = "14px Arial black Bold"
    n["borderWidth"]=1
    n["opacity"]=50
    #print (n["id"],nodes[nodes.id==n["id"]].any()[0])
    #print (n["id"],nodes[nodes.id==n["id"]])
    if nodes[nodes.id==n["id"]]["Struct"].any():
        n["size"]=22
        n["shape"]="star"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="red"
    else:
        #print (n["id"])
        n["size"]=12
        n["shape"]="dot"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="blue"        
        
for n in netPred2.nodes:
    n["title"]=str(n["id"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=df_names[df_names.id==n["id"]][["id","Description"]]
    #n["title"]=n["id"]
    n["font"] = "14px Arial black Bold"
    n["borderWidth"]=1
    n["opacity"]=50
    #print (n["id"],nodes[nodes.id==n["id"]].any()[0])
    #print (n["id"],nodes[nodes.id==n["id"]])
    if nodes[nodes.id==n["id"]]["Struct"].any():
        n["size"]=22
        n["shape"]="star"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="red"
    else:
        #print (n["id"])
        n["size"]=12
        n["shape"]="dot"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="blue"        


# In[16]:


for n in netStruct.nodes:
    n["title"]=str(n["id"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])+":"+str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=str(df_names[df_names.id==n["id"]]["Description"])
    #n["title"]=df_names[df_names.id==n["id"]][["id","Description"]]
    #n["title"]=n["id"]
    n["font"] = "14px Arial black Bold"
    n["borderWidth"]=1
    n["opacity"]=50
    #print (n["id"],nodes[nodes.id==n["id"]].any()[0])
    #print (n["id"],nodes[nodes.id==n["id"]])
    if nodes[nodes.id==n["id"]]["Struct"].any():
        n["size"]=22
        n["shape"]="star"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="red"
    else:
        #print (n["id"])
        n["size"]=12
        n["shape"]="dot"
        n["opacity"]=50
        n["Shadow"]=True
        n["color"]="blue"        


# In[18]:

print ("INFO> Finished mapping nodes")

for e in netALL.edges:
    #print (e)
    #e["length"]=np.min(100,)
    e["opacity"]=50
    id=e["from"]+"-"+e["to"]
    if e["from"]==e["to"]:
        e["value"]=0
        e["hidden"]=True
    else:
        if df[df.SortedName==id]["VeryGood"].max():
            e["value"]=1
            e["color"]="red"
            #e["length"]=100
        elif df[df.SortedName==id]["Good"].max():
            e["value"]=.2
            e["color"]="red"
            #e["length"]=100
        elif df[df.SortedName==id]["Struct"].max():
            e["value"]=1
            e["color"]="Green"
            #e["length"]=100
        else: 
            e["value"]=0.1
            e["color"]="blue"
            
            #e["length"]=200

for e in netHuRI.edges:
    #print (e)
    #e["length"]=np.min(100,)
    e["opacity"]=50
    id=e["from"]+"-"+e["to"]
    if e["from"]==e["to"]:
        e["value"]=0
        e["hidden"]=True
    else:
        if df_HuRI[df_HuRI.SortedName==id]["VeryGood"].max():
            e["value"]=1
            e["color"]="red"
            #e["length"]=100
        if df_HuRI[df_HuRI.SortedName==id]["Good"].max():
            e["value"]=.2
            e["color"]="red"
            #e["length"]=100
        #elif df[df.SortedName==id]["Struct"].max():
        #    e["value"]=1
        #    e["color"]="Green"
        #    #e["length"]=100
        #elif df[df.SortedName==id]["HuMap"].max(): 
        #    e["value"]=0.2
        #    e["color"]="black"
        else: 
            e["value"]=0.1
            e["color"]="blue"
            
            #e["length"]=200

#for e in netcombo.edges:
#    #print (e)
#    #e["length"]=np.min(100,)
#    e["opacity"]=50
#    id=e["from"]+"-"+e["to"]
#    if e["from"]==e["to"]:
#        e["value"]=0
#        e["hidden"]=True
#    else:
#        if df_combo[df_combo.SortedName==id]["VeryGood"].max():
#            e["value"]=1
#            e["color"]="red"
#            #e["length"]=100
#        elif df_combo[df_combo.SortedName==id]["VeryGood"].max():
#            e["value"]=.2
#            e["color"]="red"
#            #e["length"]=100
#        elif df_combo[df_combo.SortedName==id]["Struct"].max():
#            e["value"]=1
#            e["color"]="Green"
#            #e["length"]=100
#        elif df_combo[df_combo.SortedName==id]["HuMap"].max(): 
#            e["value"]=0.1
#            e["color"]="black"
#        else: 
#            e["value"]=0.1
#            e["color"]="blue"
            
            #e["length"]=200


# In[19]:


#for e in netStruct.edges:
#    #print (e)
#    #e["length"]=np.min(100,)
#    e["opacity"]=50
#    id=e["from"]+"-"+e["to"]
#    if e["from"]==e["to"]:
#        e["value"]=0
#        e["hidden"]=True
#    else:
#        if df[df.SortedName==id]["VeryGood"].max():
#            e["value"]=1
#            e["color"]="red"
#            #e["length"]=100
#        elif df[df.SortedName==id]["Good"].max():
#            e["value"]=.1
#            e["color"]="red"
#            #e["length"]=100
#        else: 
#            e["value"]=0.1
#            e["color"]="blue"
#            #e["length"]=200


# In[20]:


#for e in netPred.edges:
#    #print (e)
#    #e["length"]=np.min(100,)
#    e["opacity"]=50
#    id=e["from"]+"-"+e["to"]
#    if e["from"]==e["to"]:
#        e["value"]=0
#        e["hidden"]=True
#    else:
#        if df[df.SortedName==id]["VeryGood"].max():
#            e["value"]=.1
#            e["color"]="red"
#            #e["length"]=100
#        elif df[df.SortedName==id]["Good"].max():
#            e["value"]=.1
#            e["color"]="red"
#            #e["length"]=100
#        elif df[df.SortedName==id]["Struct"].max():
#            e["value"]=.1
#            e["color"]="Green"
#            #e["length"]=100
#        else: 
#            e["value"]=0.1
#            e["color"]="blue"
#            #e["length"]=200

#for e in netP25789.edges:
#    #print (e)
#    #e["length"]=np.min(100,)
#    e["opacity"]=50
#    id=e["from"]+"-"+e["to"]
#    if e["from"]==e["to"]:
#        e["value"]=0
#        e["hidden"]=True
#    else:
#        if df[df.SortedName==id]["VeryGood"].max():
#            e["value"]=.0001
#            e["color"]="red"
#            #e["length"]=100
#        elif df[df.SortedName==id]["Good"].max():
#            e["value"]=.0001
#            e["color"]="red"
#            #e["length"]=100
#        elif df[df.SortedName==id]["Struct"].max():
#            e["value"]=.0001
#            e["color"]="Green"
#            #e["length"]=100
#        else: 
#            e["value"]=0.0001
#            e["color"]="blue"
#            #e["length"]=200


#for e in netP31150.edges:
#    print (e)
#    #e["length"]=np.min(100,)
#    e["opacity"]=50
#    id=e["from"]+"-"+e["to"]
#    if e["from"]==e["to"]:
#        e["value"]=0
#        e["hidden"]=True
#    else:
#        e["weight"]=0.01
#        if df[df.SortedName==id]["VeryGood"].max():
#            e["value"]=.01
#            e["color"]="red"
#            #e["length"]=100
#        elif df[df.SortedName==id]["Good"].max():
#            e["value"]=.01
#            e["color"]="red"
#            #e["length"]=100
#        elif df[df.SortedName==id]["Struct"].max():
#            e["value"]=.01
#            e["color"]="Green"
#            #e["length"]=100
#        else: 
#            e["value"]=0.01
#            e["color"]="blue"

#            #e["length"]=200

#for e in netPred2.edges:
#    #print (e)
#    #e["length"]=np.min(100,)
#    e["opacity"]=50
#    id=e["from"]+"-"+e["to"]
#    if e["from"]==e["to"]:
#        e["value"]=0
#        e["hidden"]=True
#    else:
#        if df[df.SortedName==id]["VeryGood"].max():
#            e["value"]=.1
#            e["color"]="red"
#            #e["length"]=100
#        elif df[df.SortedName==id]["Good"].max():
#            e["value"]=.1
#            e["color"]="red"
#            #e["length"]=100
#        elif df[df.SortedName==id]["Struct"].max():
#            e["value"]=.1
#            e["color"]="Green"
#            #e["length"]=100
#        else: 
#            e["value"]=0.1
#            e["color"]="blue"
#            #e["length"]=200


# In[22]:

print ("INFO> Finished mapping edges")

#net_full.show_buttons(filter_=['physics'])
netStruct.force_atlas_2based()
netStruct.show("../data/network-struct.html")




# In[23]:


#net_full.show_buttons(filter_=['physics'])
netALL.force_atlas_2based()
netALL.show("../data/network-all.html")

#net_full.show_buttons(filter_=['physics'])
netHuRI.force_atlas_2based()
netHuRI.show("../data/network-HuRI.html")

#net_full.show_buttons(filter_=['physics'])
#netcombo.force_atlas_2based()
#netcombo.show("../data/network-combo.html")






#net_full.show_buttons(filter_=['physics'])
netPred.force_atlas_2based()
netPred.show("../data/network-pred.html")

#net_full.show_buttons(filter_=['physics'])
netPred2.force_atlas_2based()
netPred2.show("../data/network-pred2.html")


netP25789.force_atlas_2based()
netP25789.show("../data/network-P25789.html")

netP31150.force_atlas_2based()
netP31150.show("../data/network-P31150.html")



print ("INFO> finished making html network files.")

# In[24]:

# In[23]:
sys.exit()

# Analysis


# In[25]:

print ("INFO> Starting analyzing Gstruct")

DegreeStruct=sorted(d for n, d in Gstruct.degree())
ClustersStruct=nx.clustering(Gstruct)
ConnectedStruct=list(nx.connected_components(Gstruct))


# In[27]:


# Find all edges of connected components.
df_Struct_csv = pd.DataFrame.from_dict(ConnectedStruct) 
# We ignore all clus
df_Struct_csv.to_csv("../data/connected-struct.csv")
alldf=df #[(df.Good)]
k=0
edges={}
for index, row in df_Struct_csv.iterrows():
    #print (row.dropna().to_list())
    edges[k]=[]
    for i in row.dropna().to_list():
        for j in row.dropna().to_list():
            if i==j: continue
            name=alldf[((alldf.id1==i )& (alldf.id2==j))|((alldf.id1==j) & (alldf.id2==i))]["SortedName"].to_list()
            if (len(name)):
                if (not name[0] in edges[k]): 
                    #print (i,j,name)
                    edges[k]+=[name[0]]
    #print (k,edges[k])            
    k+=1

#edges
    


# In[28]:


f = open("../data/edges-struct.csv", "w+")
f.write("Cluster,Edge\n")
for i in edges:
    for j in edges[i]:
        f.write(str(i)+","+j)
        f.write("\n")
f.close()


# In[29]:
print ("INFO> Starting analyzing Gpred")



DegreePred=sorted(d for n, d in Gpred.degree())
ClustersPred=nx.clustering(Gpred)
ConnectedPred=list(nx.connected_components(Gpred))



DegreePred2=sorted(d for n, d in Gpred2.degree())
ClustersPred2=nx.clustering(Gpred2)
ConnectedPred2=list(nx.connected_components(Gpred2))


# In[30]:


# Find all edges of connected components.
df_pred_csv = pd.DataFrame.from_dict(ConnectedPred) 
# We ignore all clus
df_pred_csv.to_csv("../data/connected-pred.csv")
alldf=df # [(df.Good)]
k=0
edges={}
for index, row in df_pred_csv.iterrows():
    #print (row.dropna().to_list())
    edges[k]=[]
    for i in row.dropna().to_list():
        for j in row.dropna().to_list():
            if i==j: continue
            name=alldf[((alldf.id1==i )& (alldf.id2==j))|((alldf.id1==j) & (alldf.id2==i))]["SortedName"].to_list()
            if (len(name)):
                if (not name[0] in edges[k]): 
                    #print (i,j,name)
                    edges[k]+=[name[0]]
    #print (k,edges[k])            
    k+=1

#edges
    
f = open("../data/edges-pred.csv", "w+")
f.write("Cluster,Edge\n")
for i in edges:
    for j in edges[i]:
        f.write(str(i)+","+j)
        f.write("\n")
f.close()



# Find all edges of connected components.
df_pred2_csv = pd.DataFrame.from_dict(ConnectedPred2) 
# We ignore all clus
df_pred2_csv.to_csv("../data/connected-pred2.csv")
alldf=df # [(df.Good)]
k=0
edges={}
for index, row in df_pred2_csv.iterrows():
    #print (row.dropna().to_list())
    edges[k]=[]
    for i in row.dropna().to_list():
        for j in row.dropna().to_list():
            if i==j: continue
            name=alldf[((alldf.id1==i )& (alldf.id2==j))|((alldf.id1==j) & (alldf.id2==i))]["SortedName"].to_list()
            if (len(name)):
                if (not name[0] in edges[k]): 
                    #print (i,j,name)
                    edges[k]+=[name[0]]
    #print (k,edges[k])            
    k+=1

#edges
    
f = open("../data/edges-pred2.csv", "w+")
f.write("Cluster,Edge\n")
for i in edges:
    for j in edges[i]:
        f.write(str(i)+","+j)
        f.write("\n")
f.close()


# In[32]:



# In[ ]:
print ("INFO> Starting analyzing Gall")


# In[ ]:

DegreeAll=sorted(d for n, d in Gall.degree())
ClustersAll=nx.clustering(Gall)
ConnectedALL=list(nx.connected_components(Gall))


# In[ ]:


# Find all edges of connected components.
df_all_csv = pd.DataFrame.from_dict(ConnectedALL) 
# We ignore all clus
df_all_csv.to_csv("../data/connected-all.csv")
alldf=df # [(df.Good)]
k=0
edges={}
for index, row in df_all_csv.iterrows():
    #print (row.dropna().to_list())
    edges[k]=[]
    for i in row.dropna().to_list():
        for j in row.dropna().to_list():
            if i==j: continue
            name=alldf[((alldf.id1==i )& (alldf.id2==j))|((alldf.id1==j) & (alldf.id2==i))]["SortedName"].to_list()
            if (len(name)):
                if (not name[0] in edges[k]): 
                    #print (i,j,name)
                    edges[k]+=[name[0]]
    #print (k,edges[k])            
    k+=1

#edges
    
f = open("../data/edges-all.csv", "w+")
f.write("Cluster,Edge\n")
for i in edges:
    for j in edges[i]:
        f.write(str(i)+","+j)
        f.write("\n")
f.close()



print ("INFO> Starting analyzing GHuRI")


# In[ ]:

DegreeHuRI=sorted(d for n, d in GHuRI.degree())
ClustersHuRI=nx.clustering(GHuRI)
ConnectedHURI=list(nx.connected_components(GHuRI))


# In[ ]:


# Find HuRI edges of connected components.
df_HuRI_csv = pd.DataFrame.from_dict(ConnectedHURI) 
# We ignore HuRI clus
df_HuRI_csv.to_csv("../data/connected-HuRI.csv")
alldf=df_HuRI # [(df.Good)]
k=0
edges={}
for index, row in df_HuRI_csv.iterrows():
    #print (row.dropna().to_list())
    edges[k]=[]
    for i in row.dropna().to_list():
        for j in row.dropna().to_list():
            if i==j: continue
            name=alldf[((alldf.id1==i )& (alldf.id2==j))|((alldf.id1==j) & (alldf.id2==i))]["SortedName"].to_list()
            if (len(name)):
                if (not name[0] in edges[k]): 
                    #print (i,j,name)
                    edges[k]+=[name[0]]
    #print (k,edges[k])            
    k+=1

#edges
    
f = open("../data/edges-HuRI.csv", "w+")
f.write("Cluster,Edge\n")
for i in edges:
    for j in edges[i]:
        f.write(str(i)+","+j)
        f.write("\n")
f.close()

print ("INFO> Starting analyzing combo")


#Degreecombo=sorted(d for n, d in Gcombo.degree())
#Clusterscombo=nx.clustering(Gcombo)
#ConnectedCOMBO=list(nx.connected_components(Gcombo))
#
#
## In[ ]:
#
#
## Find combo edges of connected components.
#df_combo_csv = pd.DataFrame.from_dict(ConnectedCOMBO) 
## We ignore combo clus
#df_combo_csv.to_csv("../data/connected-combo.csv")
#alldf=df_combo # [(df.Good)]
#k=0
#edges={}
#for index, row in df_combo_csv.iterrows():
#    #print (row.dropna().to_list())
#    edges[k]=[]
#    for i in row.dropna().to_list():
#        for j in row.dropna().to_list():
#            if i==j: continue
#            name=alldf[((alldf.id1==i )& (alldf.id2==j))|((alldf.id1==j) & (alldf.id2==i))]["SortedName"].to_list()
#            if (len(name)):
#                if (not name[0] in edges[k]): 
#                    #print (i,j,name)
#                    edges[k]+=[name[0]]
#    #print (k,edges[k])            
#    k+=1
#
##edges
#    
#f = open("../data/edges-combo.csv", "w+")
#f.write("Cluster,Edge\n")
#for i in edges:
#    for j in edges[i]:
#        f.write(str(i)+","+j)
#        f.write("\n")
#f.close()





# In[ ]:




df_HuRI_csv["Count"]=df_HuRI_csv[df_HuRI_csv.notna()].count(axis=1)
#df_combo_csv["Count"]=df_combo_csv[df_combo_csv.notna()].count(axis=1)
df_all_csv["Count"]=df_all_csv[df_all_csv.notna()].count(axis=1)
df_Struct_csv["Count"]=df_Struct_csv[df_Struct_csv.notna()].count(axis=1)
df_pred_csv["Count"]=df_pred_csv[df_pred_csv.notna()].count(axis=1)
df_pred2_csv["Count"]=df_pred2_csv[df_pred2_csv.notna()].count(axis=1)



f, ax = plt.subplots(figsize=(6.5, 6.5))
#sns.histplot(data=df_combo_csv,x="Count",log_scale=(True,True),kde=True,label="combo",common_bins=True)
sns.histplot(data=df_HuRI_csv,x="Count",log_scale=(True,True),kde=True,label="Huri",common_bins=True)
sns.histplot(data=df_all_csv,x="Count",log_scale=(True,True),kde=True,label="All",common_bins=True)
sns.histplot(data=df_pred_csv,x="Count",log_scale=(True,True),kde=True,label="Pred",color="r",common_bins=True)
sns.histplot(data=df_Struct_csv,x="Count",log_scale=(True,True),kde=True,label="Struct",color="g",common_bins=True)
plt.legend(loc = 'upper right')
plt.xlim([1.9, 50])
plt.ylim([0.5, 500])
ax.set_title("Distribution of clustersizes")
#plt.ylabel('True Positive Rate')
plt.xlabel('Size of Cluster')
plt.savefig("../plots/network-distribution.png",dpi=1200)


f, ax = plt.subplots(figsize=(6.5, 6.5))
#sns.kdeplot(data=df_combo_csv,x="Count",log_scale=(True,True),label="combo")
sns.kdeplot(data=df_HuRI_csv,x="Count",log_scale=(True,True),label="HuRI")
sns.kdeplot(data=df_all_csv,x="Count",log_scale=(True,True),label="All")
sns.kdeplot(data=df_Struct_csv,x="Count",log_scale=(True,True),label="Struct")
sns.kdeplot(data=df_pred_csv,x="Count",log_scale=(True,True),label="Pred")
plt.legend(loc = 'upper right')
#plt.xlim([1.9, 50])
plt.ylim([0.01,40])
ax.set_title("Distribution of clustersizes")
#plt.ylabel('True Positive Rate')
plt.xlabel('Size of Cluster')
plt.savefig("../plots/network-dist-kde.png",dpi=1200)
