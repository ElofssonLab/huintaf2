#!/bin/bash 


A=`basename $1 | gawk -F "-"  '{print $1}'`
B=`basename $1 | gawk -F "-"  '{print $2}'`


a=`ls -l  $1/msas/ | gawk '{print $11}' | head -2 | tail -1  | sed  "s/.*multimer\///g" | sed "s/.*\.\.\///g" |sed  "s/\/msas.*//g" `
b=`ls -l  $1/msas/ | gawk '{print $11}' | head -3 | tail -1  | sed  "s/.*multimer\///g" | sed "s/.*\.\.\///g" |sed  "s/\/msas.*//g" `

aa=`grep desc $1/msas/chain_id_map.json | head -1 | gawk -F "|" '{print $2}'`
bb=`grep desc $1/msas/chain_id_map.json | head -2 | tail -1| gawk -F "|" '{print $2}'`


AA=`grep \> seq/${A}-${B}.fasta | head -1 | gawk -F "|" '{print $2}'`
BB=`grep \> seq/${A}-${B}.fasta | head -2 | tail -1| gawk -F "|" '{print $2}'`


#echo $A $a $aa $AA
#echo $B $b $bb $BB


if [ $A = $a ] &&  [ $B = $b ] &&  [ $A = $aa ] &&  [ $B = $bb ]  &&  [ $A = $AA ] &&  [ $B = $BB ] 
then
    echo  $1 "correct" $A $a $aa $AA $B $b $bb $BB
else
    if [ $A = $a ] &&  [ $B = $b ]
    then
	if [ $A = $aa ] &&  [ $B = $bb ]
	then
	    if [ $A = $AA ] &&  [ $B = $BB ]
	    then
		echo  $1 "error-multi" $A $a $aa $AA $B $b $bb $BB
	    else
		echo  $1 "error-4" $A $a $aa $AA $B $b $bb $BB
	    fi
	else
	    if [[ -s $1/msas/chain_id_map.json ]]
	    then
		echo  $1 "error-3" $A $a $aa $AA $B $b $bb $BB
	    else
		echo  $1 "missing" $A $a $aa $AA $B $b $bb $BB
	    fi
	fi
    else
	echo  $1 "error-2" $A $a $aa $AA $B $b $bb $BB
    fi
fi
