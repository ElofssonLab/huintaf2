#!/bin/bash -x
#SBATCH -A SNIC2021-5-297
#SBATCH --output=/proj/nobackup/snic2019-35-62/arnee/hhblits-runs/out/%j.out
#SBATCH --error=/proj/nobackup/snic2019-35-62/arnee/hhblits-runs/err/%j.err
#SBATCH --array=1-500
#SBATCH -c 8
#SBATCH -t 04:00:00


list=$1
offset=$2

pos=$(($SLURM_ARRAY_TASK_ID + $offset))
id=`tail -n+$pos $list | head -n 1`

echo 'Processing ' $id '...'
if [ ! -f sto//${id}.a3m ]
then
bin/jackhmmer --cpu 8 -N 1 -E 0.0001 --F1 0.0005 --F2 0.00005 --F3 0.0000005 -A JH-STO/${id}.sto -o out/${id}.out seq/${id}.fasta  /proj/nobackup/snic2019-35-62/Database/uniprot/uniprot.fasta
#    /proj/nobackup/snic2019-35-62/arnee/bin/hhblits  -i seq/${id}.fasta -d /proj/nobackup/snic2019-35-62/Database/UniRef30_2020_03/UniRef30_2020_03 -E 0.001 -all name=${id} -oa3m a3m//${id}.a3m > out/${id}.log
fi


