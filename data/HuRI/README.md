The file HI-union.csv contains all interactions from HuRI
The file seqs.csv contains all unique sequences shorter than 2000 residues that could be mapped to uniprot of these.
The file pos_interactions.csv contains the interactions in HI-union.csv that have an available sequence in seqs.csv
The file unique_ids.csv contains all unique ids from HI-union.
The files clustered_seqs* are the results from clustering all sequences in seqs.csv on 40% identity using CD-HIT.
