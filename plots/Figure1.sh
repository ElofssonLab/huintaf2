#!/bin/bash


convert  plDDT-evidence.png -pointsize 300 -gravity NorthWest -draw "text 10,300 A)" Fig1a.png
#convert  pLDDT.png -pointsize 300 -gravity NorthWest -draw "text 10,300 A)" Fig1a.png
convert  NumRes.png -pointsize 300 -gravity NorthWest -draw "text 10,300 B)" Fig1b.png
convert  Y2H.png  -pointsize 200 -gravity NorthWest -draw "text 10,300 C)" Fig1c.png
convert venn-humap.png -pointsize 300 -gravity NorthWest -draw "text 10,300 B)" Fig1g.png
convert venn-huri.png -pointsize 300 -gravity NorthWest -draw "text 10,300 B)" Fig1h.png
convert  P52906-P47756.png -pointsize 100 -gravity NorthWest -draw "text 10,300 C)" Fig1i.png

convert  P31150-P20338.png -pointsize 100 -gravity NorthWest -draw "text 10,300 F)" Fig1j.png

#pLDDT-Homodimers.png

composite -geometry 2500x2500+800+1000 -gravity SouthEast venn-structure.png ROC-error.png ROC-error.tmp.png
convert  ROC-error.tmp.png -pointsize 300 -gravity NorthWest -draw "text 10,300 D)" Fig1d.png

composite -geometry 2500x2500+800+1000 -gravity SouthEast venn-humap.png ROC-negatome.png ROC-negatome.tmp.png
convert  ROC-negatome.tmp.png -pointsize 300 -gravity NorthWest -draw "text 10,300 E)" Fig1e.png
convert  P28161-P46439.png -pointsize 100 -gravity NorthWest -draw "text 10,300 F)" Fig1f.png

montage -tile 3x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 3072x2048+0+0 Fig1[abcdef].png  figure1.tiff

montage -tile 3x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 3072x2048+0+0 Fig1a.png Fig1h.png Fig1i.png Fig1d.png Fig1f.png Fig1i.png figure1_new.tiff

convert figure1.tiff figure1.png
convert figure1_new.tiff figure1_new.png




convert  plDDT-evidence.png -pointsize 600 -gravity NorthWest -draw "text 10,600 A)" Figure-1a.png
convert venn-humap.png -pointsize 300 -gravity NorthWest -draw "text 10,300 B)" Figure-1b.png
convert  P52906-P47756.png -pointsize 200 -gravity NorthWest -draw "text 10,200 C)" Figure-1c.png
convert  cluster_representatives/huMAP/P49754-Q9H269.png -pointsize 100 -gravity NorthWest -draw "text 10,100 D)" Figure-1d.png 
convert  cluster_representatives/huMAP/Q96QK1-O75436_template.png -pointsize 50 -gravity NorthWest -draw "text 10,50 E)" Figure-1e.png 
convert  cluster_representatives/huMAP/O00499-Q9UBW5.png -pointsize 100 -gravity NorthWest -draw "text 10,100 F)" Figure-1f.png 



# List of nice figures

#P52906-P47756.png
P32969-Q02543.png
#Q96QK1-O75436.png
P35250-P40937.png
P31946-P61981.png
#P49754-Q9H269.png
#O00499-Q9UBW5.png
P32119-P30048.png
P35998-O00231.png
# P33402-Q02153.png Very nice but not included in Petras correct models, aligns with 6JT2

montage -tile 3x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 3072x2048+0+0 Figure-1[abcdef].png figure1_final.tiff

convert figure1_final.tiff figure1_final.png
