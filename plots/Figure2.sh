#!/bin/bash


convert ./network.png -pointsize 100 -gravity NorthWest -draw "text 10,100 A)" Fig2a.png
convert ./network-full-P31150.png -pointsize 100 -gravity NorthWest -draw "text 10,100 B)" Fig2b.png
convert ./P31150.png -pointsize 100 -gravity NorthWest -draw "text 10,100 C)" Fig2c.png



montage -tile 1x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+0+0 Fig2b.png Fig2c.png figure2.temp.tiff

montage -tile 2x1 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 2048x2048+0+0 ./Fig2a.png  figure2.temp.tiff figure2.tiff
convert figure2.tiff figure2.png

rm figure2.temp.tiff

