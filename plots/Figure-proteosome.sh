
convert  proteasome/proteasome-network.png -pointsize 50 -gravity NorthWest -draw "text 10,50 B)" Fig-Pb.png
convert  proteasome/proteasome-4mer.png -pointsize 150 -gravity NorthWest -draw "text 10,150 C)" Fig-Pc.png
convert  proteasome/proteasome-4mer-superposed.png -pointsize 150 -gravity NorthWest -draw "text 10,150 E)" Fig-Pe.png
convert  proteasome/proteasome-allchains.png -pointsize 150 -gravity NorthWest -draw "text 10,150 F)" Fig-Pf.png

convert  P31150.png -pointsize 150 -gravity NorthWest -draw "text 10,150 D)" Fig-Pd.png
convert  network-P31150.png -pointsize 50 -gravity NorthWest -draw "text 10,50 A)" Fig-Pa.png

montage -tile 3x2 -mode concatenate -density 600  -compress lzw -pointsize 120 -geometry 3072x2048+0+0 Fig-P[abcdef].png  figure-P.tiff
convert figure-P.tiff  figure-P.png

