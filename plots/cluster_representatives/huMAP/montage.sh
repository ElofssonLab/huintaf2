

for i in {1..20}
do
  ID=$(sed -n $i'p' sel_ids.txt)
  DOCKQ=$(sed -n $i'p' sel_ids_dockq_template.txt)
  convert $ID.png  -pointsize 30 -gravity North -annotate +0+0 $ID'|DockQ='$DOCKQ $ID'_test.png'
done


# #Good ex
# montage C9JRZ8-F5H284_test.png \
# O43292-Q969N2_test.png \
# O75431-Q9Y512_test.png \
# O75477-O94905_test.png \
# O75521-Q13011_test.png \
# P49754-Q9H269_test.png \
# P61981-P63104_test.png \
# P62714-P30153_test.png \
# Q9H6D7-Q96CS2_test.png \
# Q99623-P35232_test.png \
# Q9H6D7-Q96CS2_test.png \
# Q96QK1-O75436_test.png \
# -tile 3x4 -geometry +2+2 good_ex.png
#
# #Bad ex
# montage P05412-Q02930_test.png \
# P07197-Q16352_test.png \
# O00499-Q9UBW5_test.png \
# P15408-P17275_test.png \
# -tile 2x2 -geometry +2+2 bad_ex.png
#
# #loop ints
# montage P09467-O00757_test.png \
# P17858-P08237_test.png \
# P18754-P62826_test.png \
# Q9HAW8-Q9HAW7_test.png \
# Q676U5-Q9H1Y0_test.png \
# -tile 3x2 -geometry +2+2 loop_int_ex.png


#Selection for paper
montage P49754-Q9H269_test.png \
Q96QK1-O75436_template_test.png \
Q676U5-Q9H1Y0_template_test.png \
O00499-Q9UBW5_test.png -tile 2x2 -geometry +2+2 Figx.png
